<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 04/08/2019
 * Time: 14:17
 */

namespace app\classes;

use app\models\PushToken;
use app\models\User;
use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;
use Kreait\Firebase\ServiceAccount;

class PushMessageSender
{
    private $users = [];
    private $firebaseMessaging;
    private $deviceIDs = [];

    public function __construct()
    {
        $serviceAccount = ServiceAccount::fromJsonFile(\Yii::$app->params['google_services_json_path']);
        $this->firebaseMessaging = (new Factory)
            ->withServiceAccount($serviceAccount)->create()->getMessaging();
    }

    public function setUsers(User $user)
    {
        $this->users[] = $user;
        return $this;
    }

    private function composeIDs()
    {
        $uids = [];
        foreach ($this->users as $user) {
            $uids[] = $user->getId();
        }
        $this->deviceIDs = array_column(PushToken::find()->where(['user_id' => $uids])->asArray()->all(), 'token');
    }


    public function sendNotification(Notification $notification, array $data = [])
    {
        $this->composeIDs();
        $message = CloudMessage::new()->withNotification($notification)->withData($data);
        return $this->firebaseMessaging->sendMulticast($message, $this->deviceIDs);
    }
}