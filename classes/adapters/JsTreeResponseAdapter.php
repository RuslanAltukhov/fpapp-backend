<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 29/09/2019
 * Time: 14:53
 */

namespace app\classes\adapters;


use app\models\data\InvitationTreeItem;

class JsTreeResponseAdapter
{
    private $treeData;

    public function __construct(array $treeData)
    {
        $this->treeData = $treeData;
    }

    private function getIdForSibling(InvitationTreeItem $item)
    {
        return "#node_" . $item->user->id;
    }

    private function getChilds(InvitationTreeItem $parentItem)
    {
        $data = [];
        foreach ($parentItem->childs as $childTreeItem) {
            if ($childTreeItem->hasChilds()) {
                $data = array_merge($data, $this->getChilds($childTreeItem));
            }
            $data[] = $this->formatNode($parentItem, $childTreeItem);
        }
        return $data;
    }

    private function formatNode(?InvitationTreeItem $parentNode, InvitationTreeItem $node)
    {
        return ['parent' => $parentNode ? $this->getIdForSibling($parentNode) : '#',
            'id' => $this->getIdForSibling($node),
            'text' => $node->user->getNameFormatted() . "({$node->user->id})"
        ];
    }

    public function getJSON()
    {
        $results = [];
        foreach ($this->treeData as $treeItem) {
            if ($treeItem->hasChilds()) {
                $results = array_merge($results, $this->getChilds($treeItem));
            }
            $results[] = $this->formatNode(null, $treeItem);
        }
        return json_encode($results);
    }
}