<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 12/10/2019
 * Time: 13:21
 */

namespace app\classes\adapters;


use app\models\data\UserStructureItem;
use yii\data\ArrayDataProvider;

class UserStructureResponseAdapter
{
    private $structureItems;
    private $incomeFeeTransactions;

    public function __construct(array $structureItems, array $incomeFeeTransactions)
    {
        $this->structureItems = $structureItems;
        $this->incomeFeeTransactions = $incomeFeeTransactions;
    }

    /**
     * Сумма дохода с пользователя структуры
     * @param UserStructureItem $structureItem
     * @return int
     */
    private function getFeeSumForItem(UserStructureItem $structureItem)
    {
        $sum = 0;
        foreach ($this->incomeFeeTransactions as $transaction) {
            if ($transaction->source_user_id == $structureItem->getUser()->getId()) {
                $sum += $transaction->sum;
            }
        }
        return $sum;
    }

    public function getData()
    {
        foreach ($this->structureItems as $structureItem) {
            $structureItem->setFeeSum($this->getFeeSumForItem($structureItem));
        }
        return $this->structureItems;
    }

    private function convertDataToArray(array $data)
    {
        foreach ($data as $key => $item) {
            $data[$key] = (array)$item;
        }
        return $data;
    }

    public function getDataAsProvider()
    {
        $arrayData = $this->convertDataToArray($this->getData());

        return new ArrayDataProvider([
            'allModels' => $arrayData,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'attributes' => ['level', 'referalsCount','feeSum'],
            ]
        ]);

    }
}