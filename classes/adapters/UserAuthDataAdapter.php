<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 28/05/2019
 * Time: 19:56
 */

namespace app\classes\adapters;


use app\models\params\SessionData;
use app\models\User;

class UserAuthDataAdapter
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function setSessionData(SessionData $sessionData)
    {
        $transaction = \Yii::$app->getDb()->beginTransaction();
        foreach ($this->user->authTokens as $token) {
            $token->setData($sessionData);
        }
        $transaction->commit();
    }

    public function getSessionData()
    {
        return $this->user->authTokens[0] ?? new SessionData([]);
    }
}