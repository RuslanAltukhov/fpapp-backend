<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 18/11/2019
 * Time: 22:26
 */

namespace app\classes\adapters;


use app\models\PartnerLimitation;
use app\models\User;

class PartnerLimitationsAdapter
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getUsedAgentPackets(): int
    {
        if (!$this->user->isPartner()) {
            return 0;
        }
        $limitation = PartnerLimitation::getByUserId($this->user->getId());
        return $limitation->used_agent_packets;
    }

    public function getAgentPacketsLimitation(): int
    {
        if (!$this->user->isPartner()) {
            return 0;
        }
        $limitation = PartnerLimitation::getByUserId($this->user->getId());
        return $limitation->agent_packets;
    }

    public function isLimitExcided(): bool
    {
        return $this->getUsedAgentPackets() >= $this->getAgentPacketsLimitation();
    }

    public function increaseUsedPackets()
    {
        $limitation = PartnerLimitation::getByUserId($this->user->getId());
        $limitation->used_agent_packets++;
        $limitation->save();
    }
}