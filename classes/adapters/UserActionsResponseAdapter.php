<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 27/05/2019
 * Time: 17:45
 */

namespace app\classes\adapters;

use app\classes\UserActions;
use yii\helpers\Url;

class UserActionsResponseAdapter
{
    private $userActions;

    public function __construct(UserActions $userActions)
    {
        $this->userActions = $userActions;
    }

    private function getLogoPathFormatted($path)
    {
        return Url::base(true) . "/" . $path;
    }

    public function getActionsWithPurchasedItems()
    {
        $results = $this->userActions->getActionsList()->orderBy("action_sales.created_at DESC")->all();
        foreach ($results as &$action) {

            $currentLapPurchases = $this->userActions->getCurrentLapPurchasedItems($action)->all();
            $org = $action->organization;
            $style = $action->style;
            $style->logo_url = $this->getLogoPathFormatted($style->logo_url);
            $salesHistory = $this->userActions->getSalesLogForAction($action)->with("salePoint")->asArray()->all();
            $action = $action->toArray();
            $action['organization'] = $org->toArray();
            $action['style'] = $style;
            $action['salesHistory'] = $salesHistory;

            $action['current_lap_purchases'] = $currentLapPurchases;

        }

        return $results;
    }
}