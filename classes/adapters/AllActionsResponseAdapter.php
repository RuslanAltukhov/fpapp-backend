<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 04/07/2019
 * Time: 10:56
 */

namespace app\classes\adapters;


use app\models\ActionsQuery;
use yii\helpers\Url;

class AllActionsResponseAdapter
{
    private $query;
    private $userTime;

    public function __construct(ActionsQuery $query, int $userTime)
    {
        $this->query = $query;
        $this->userTime = $userTime;
    }

    private function isOpened($openTime, $closeTime): bool
    {
        $todayOpen = strtotime(date("Y-m-d {$openTime}"));
        $todayClose = strtotime(date("Y-m-d {$closeTime}"));
        $currentTime = strtotime(date('Y-m-d H:i'));
        return ($currentTime < $todayClose) && ($todayOpen < $currentTime);
    }

    private function getWorkingState(array $salePoint)
    {
        $result = ['isOpen' => false, 'openTill' => null, 'willOpen' => null];
        $dayOfWeek = date('w', $this->userTime);
        foreach ($salePoint['workingHours'] as $workingHour) {

            if ($workingHour['every_time'] == 1) {
                $result['isOpen'] = true;
                $result['openTill'] = null;
                continue;
            }
            if ($this->isOpened($workingHour['open'], $workingHour['close'])) {
                $result['isOpen'] = true;
                $result['openTill'] = $workingHour['close'];
            }
        }

        return $result;
    }

    private function getLogoPathFormatted($path)
    {
        return Url::base(true) . "/" . $path;
    }

    public function getFormatted()
    {
        $actionItems = $this->query->asArray()->all();
        foreach ($actionItems as &$action) {
            $action['organization']['logo_path'] = $this->getLogoPathFormatted($action['organization']['logo_path']);
            foreach ($action['salePoints'] as &$salePoint) {
                $salePoint['workingState'] = $this->getWorkingState($salePoint);
            }
        }
        return $actionItems;
    }
}