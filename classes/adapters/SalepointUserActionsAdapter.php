<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 26/07/2019
 * Time: 18:44
 */

namespace app\classes\adapters;


use app\classes\SalepointActions;
use app\classes\UserActions;
use app\models\Action;
use yii\helpers\Url;

class SalepointUserActionsAdapter
{
    private $salepointActions;
    private $userActions;

    public function __construct(SalepointActions $salepointActions, UserActions $userActions)
    {
        $this->salepointActions = $salepointActions;
        $this->userActions = $userActions;
    }

    private function getAbsoluteImagePath(?string $path)
    {
        if(!$path) {
            return;
        }
        return Url::base(true) . DIRECTORY_SEPARATOR . $path;
    }

    public function getData()
    {
        $actions = $this->salepointActions->getActions()->asArray()->all();
        foreach ($actions as &$action) {
            $action['style']['logo_url'] = $this->getAbsoluteImagePath($action['style']['logo_url']);
            $action['currentLapPurchases'] = $this->userActions->getCurrentLapPurchasedItems(new Action([
                'id' => $action['id'],
                'need_to_buy' => $action['need_to_buy']
            ]))->with("salePoint")->asArray()->all();
        }
        return $actions;
    }
}