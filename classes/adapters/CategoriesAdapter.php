<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 05/07/2019
 * Time: 09:09
 */

namespace app\classes\adapters;


use app\models\CategoriesQuery;
use app\models\CategoryGroupsQuery;

class CategoriesAdapter
{
    private $query;

    public function __construct(CategoryGroupsQuery $query)
    {
        $this->query = $query;
    }

    public function get()
    {
        return $this->query->asArray()->all();
    }
}