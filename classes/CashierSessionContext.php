<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 25/07/2019
 * Time: 14:19
 */

namespace app\classes;


use app\models\AuthToken;
use app\models\Cashier;

class CashierSessionContext
{
    private $cashier;
    private $authToken;

    public function __construct(Cashier $cashier, AuthToken $authToken)
    {
        $this->cashier = $cashier;
        $this->authToken = $authToken;
    }

    public function geCurrentSalepoint()
    {
        $sessionData = $this->authToken->getSessionData();
        return $sessionData->getSalePoint();
    }
}