<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 24/07/2019
 * Time: 09:05
 */

namespace app\classes;


use app\models\ActionSale;
use app\models\Cashier;
use app\models\SalePoint;
use yii\base\Model;

class CashierSalesHistory extends Model
{
    private $cashier;
    private $salePoint;
    public $date;


    public function getErrorsFormatted()
    {
        return implode(",", array_merge($this->getErrors("date")));
    }

    public function rules()
    {
        return [
            ['date', 'required'],
            ['date', 'date', 'format' => 'php:Y-m-d']
        ];
    }

    public function __construct(Cashier $cashier, SalePoint $salePoint)
    {
        parent::__construct();
        $this->cashier = $cashier;
        $this->salePoint = $salePoint;

    }

    private function getSalesFordate($date)
    {
        $salesQuery = ActionSale::find()->with(["user" => function ($q) {
            $q->select(["users.*", "(IF(users.referer_organization = {$this->cashier->organization_id},1,0)) AS is_referal"]);
            return $q;
        }])
            ->with("action")
            ->where([
                'cashier_id' => $this->cashier->id,
                'salepoint_id' => $this->salePoint->id
            ])
            ->andWhere("created_at BETWEEN :start_date AND :end_date")->params([
                ':start_date' => $date . " 00:00",
                ':end_date' => $date . " 23:59:59",
            ])
            ->orderBy('id DESC')
            ->asArray();

        return $salesQuery->all();
    }

    public function getSalesFor5Days()
    {
        $dates = $this->getDates();
        $result = [];
        foreach ($dates as $date) {
            $sales = $this->getSalesFordate($date);
            $result[] = [
                'date' => $date,
                'salesCount' => $this->getSalesCount($sales),
                'bonusCount' => $this->getBonusCount($sales),
                'items' => $sales
            ];
        }
        return $result;
    }

    private function getBonusCount($sales)
    {
        $count = 0;
        foreach ($sales as $sale) {
            if ($sale['type'] == ActionSale::TYPE_GIFT) {
                $count++;
            }
        }
        return $count;
    }

    private function getSalesCount($sales)
    {
        $count = 0;
        foreach ($sales as $sale) {
            if ($sale['type'] == ActionSale::TYPE_SALE) {
                $count++;
            }
        }
        return $count;
    }

    private function getDates()
    {
        $dates = [];
        $date = \DateTime::createFromFormat("Y-m-d", $this->date);
        $dates[] = $date->format("Y-m-d");
        while (count($dates) < 5) {
            $date = $date->modify("- 1 day");
            $dates[] = $date->format("Y-m-d");
        }
        return $dates;
    }

}