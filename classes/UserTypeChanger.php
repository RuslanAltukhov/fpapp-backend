<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 11/10/2019
 * Time: 11:28
 */

namespace app\classes;


use app\classes\Exception\UserChangeStatusException;
use app\models\Transaction;
use app\models\User;

class UserTypeChanger
{
    /**
     * @var User $user
     */
    private $user;
    private $partnerStructure;
    private $transactionCreator;
    const AGENT_SUBSCRIPTION_PRICE = 30000;
    const PARTNER_SUBSCRIPTIONS_PRICE = 100000;

    public function __construct(User $user)
    {
        $this->user = $user;
        $this->partnerStructure = new PartnerStructure();
        $this->transactionCreator = new TransactionCreator($user);
    }

    public function changeStatusToPartner()
    {
        if (!$this->user->hasReferer()) {
            throw new UserChangeStatusException("Для участия в партнерской программе вам необходимо указать код наставника в профиле");
        }
        if ($this->user->user_type == User::USER_TYPE_PARTNER) {
            throw new UserChangeStatusException("Вы уже являетесь партнером");
        }
        $transaction = \Yii::$app->getDb()->beginTransaction();
        if (!$this->user->hasEnoughtMoney(self::PARTNER_SUBSCRIPTIONS_PRICE)) {
            throw new UserChangeStatusException(sprintf("Не достаточно средств для покупки партнерского пакета. На балансе должно быть %d", self::PARTNER_SUBSCRIPTIONS_PRICE));
        }
        $this->user->user_type = User::USER_TYPE_PARTNER;
        $this->user->balance -= self::PARTNER_SUBSCRIPTIONS_PRICE;
        $this->transactionCreator->createOutTransaction(self::PARTNER_SUBSCRIPTIONS_PRICE, Transaction::TYPE_PARTNER_SUBSCRIPTION);
        $partnerProgramm = new PartnerProgrammPayment($this->user->refererUser);
        $partnerProgramm->onPartnerSubscriptionBought($this->user);
        $this->user->save();
        if (!$this->partnerStructure->isUserExistInStructure($this->user)) {
            $this->partnerStructure->addNewUser($this->user->refererUser, $this->user);
        }
        $partnerProgramm->createLevelFeeTransaction(self::PARTNER_SUBSCRIPTIONS_PRICE, $this->user, $this->user);
        $transaction->commit();
    }

    public function changeStatusToAgent()
    {
        if (!$this->user->hasReferer()) {
            throw new UserChangeStatusException("Для того чтобы стать агентом, вам необходимо указать код наставника в профиле");
        }
        if ($this->user->user_type == User::USER_TYPE_AGENT) {
            throw new UserChangeStatusException("Вы уже являетесь агентом");
        }
        $transaction = \Yii::$app->getDb()->beginTransaction();
        if (!$this->user->hasEnoughtMoney(self::AGENT_SUBSCRIPTION_PRICE)) {
            throw new UserChangeStatusException(sprintf("Не достаточно средств для покупки агентского пакета. На балансе должно быть %d", self::AGENT_SUBSCRIPTION_PRICE));
        }
        $this->user->user_type = User::USER_TYPE_AGENT;
        $this->user->balance -= self::AGENT_SUBSCRIPTION_PRICE;
        $this->transactionCreator->createOutTransaction(self::AGENT_SUBSCRIPTION_PRICE, Transaction::TYPE_AGENT_SUBSCRIPTION);
        $partnerProgramm = new PartnerProgrammPayment($this->user->refererUser);
        $partnerProgramm->onAgentSubscriptionBought($this->user);
        $this->user->save();
        if (!$this->partnerStructure->isUserExistInStructure($this->user)) {
            $this->partnerStructure->addNewUser($this->user->refererUser, $this->user);
        }
        $partnerProgramm->createLevelFeeTransaction(self::AGENT_SUBSCRIPTION_PRICE, $this->user, $this->user );
        $transaction->commit();
    }
}