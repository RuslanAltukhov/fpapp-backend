<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 29/09/2019
 * Time: 22:50
 */

namespace app\classes;


use app\models\ReferalStatItem;
use app\models\User;

class ReferalStatReader
{
    const ROUND_PRECISION = 2;
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getRegistrationsCount()
    {
        return ReferalStatItem::find()
            ->where(['user_id' => $this->user->getId(), 'action_type' => ReferalStatItem::ACTION_TYPE_REGISTRATION])->count();
    }

    public function getVisitsCount()
    {
        return ReferalStatItem::find()
            ->where(['user_id' => $this->user->getId(), 'action_type' => ReferalStatItem::ACTION_TYPE_VISIT])->count();
    }

    public function getConversion()
    {
        if ($this->getVisitsCount() == 0) {
            return 0;
        }
        return round($this->getRegistrationsCount() / $this->getVisitsCount() * 100, self::ROUND_PRECISION);
    }
}