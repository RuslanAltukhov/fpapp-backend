<?php

namespace app\classes;

use app\models\Action;
use app\models\ActionSale;
use app\models\User;
use yii\db\Query;

class UserActions
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getActionsList()
    {
        $actions = \app\models\Action::find()
            ->with("style")
            ->onlyActive()
            ->innerJoin('action_sales', "`action_sales`.`action_id` = `actions`.`id` AND  `action_sales`.`user_id` = {$this->user->id}");
        return $actions;
    }


    public function isGiftAfterNextPurchase(Action $action)
    {
        $currentLapItems = $this->getCurrentLapBoughtItems($action);
        return (count($currentLapItems) == $action->need_to_buy - 1);
    }

    public function getAllBoughtItemsByAction(Action $action)
    {
        return ActionSale::find()->where(['user_id' => $this->user->id, 'action_id' => $action->id]);
    }

    public function getSalesLogForAction(Action $action)
    {
        $query = ActionSale::find()->where(['action_id' => $action->id, 'user_id' => $this->user->id])->orderBy("created_at DESC");
        return $query;
    }

    public function getActionLapRemainItems(Action $action)
    {
        $lapPurchases = $this->getCurrentLapPurchasedItems($action)->count();
        return $action->need_to_buy - $lapPurchases;
    }

    public function getActionNextLapNum(Action $action)
    {
        return $this->getCurrentLapNum($action) + 1;
    }

    public function gePurchasedItemsForLap(Action $action, int $lap)
    {
        return ActionSale::find()->where(['user_id' => $this->user->id, 'action_id' => $action->id, 'lap' => $lap]);
    }

    public function getCurrentLapNum(Action $action)
    {
        $query = new Query();
        $lastLapNum = $query->select("max(lap)")->from(ActionSale::tableName())->where(['user_id' => $this->user->id, 'action_id' => $action->id])->column();
        $lastLapNum = empty($lastLapNum[0]) ? 1 : $lastLapNum[0];

        $purchasedCount = $this->gePurchasedItemsForLap($action, $lastLapNum)->count();
        if ($action->need_to_buy == $purchasedCount - 1) {
            $lastLapNum++;

        }
        return $lastLapNum;
    }

    public function getCurrentLapPurchasedItems(Action $action)
    {
        $lapNum = $this->getCurrentLapNum($action);
        return $this->gePurchasedItemsForLap($action, $lapNum);
    }

}