<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 05/08/2019
 * Time: 18:32
 */

namespace app\classes;


class StreamTelecomSmsSender
{
    private $login;
    private $password;
    private $source;
    const URL = "https://gateway.api.sc/get/";
    private static $instance;

    private function __construct()
    {
        $credentials = \Yii::$app->params['streamtelecom'];
        $this->login = $credentials['login'];
        $this->password = $credentials['password'];
        $this->source = $credentials['source'];
    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new static();
        }
        return self::$instance;
    }

    private function getUrlFormatted(string $message, string $phone)
    {
        return self::URL . "?sadr={$this->source}&user={$this->login}&pwd={$this->password}&dadr={$phone}&text=" . urlencode($message);
    }

    public function sendMessage(string $message, string $phone)
    {
        if (defined("YII_ENV") && YII_ENV == "dev") {
            return;
        }

        $urlFormatted = $this->getUrlFormatted($message, $phone);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $urlFormatted);
        $result = curl_exec($ch);
    }
}