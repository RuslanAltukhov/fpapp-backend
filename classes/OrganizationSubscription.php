<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 14/10/2019
 * Time: 22:13
 */

namespace app\classes;


use app\classes\Exception\SubscriptionException;
use app\models\data\SubscriptionsLimitations;
use app\models\Organization;
use app\models\Subscription;
use app\models\SubscriptionParams;

class OrganizationSubscription
{
    private $organization;
    private $partnerProgrammPayment;

    const SUBSCRIPTION_MONTH_PRICE = 2000; //Цена подписки за месяц

    const SUBSCRIPTION_YEAR_PRICE = 18000; //Скидка при оплате за код
    const SECONDS_IN_MONTH = 2592000;
    const SECONDS_IN_YEAR = 31104000;
    const SUBSCRIPTION_TYPE_MONTH = 1;
    const SUBSCRIPTION_TYPE_YEAR = 2;

    public function __construct(Organization $organization)
    {
        $this->organization = $organization;
        $this->partnerProgrammPayment = new PartnerProgrammPayment($organization->owner);
    }

    public function calculateSubscriptionPlan(SubscriptionParams $params, int $subscriptionType)
    {
        return $params->salepoints_count * ($subscriptionType == self::SUBSCRIPTION_TYPE_YEAR ? self::SUBSCRIPTION_YEAR_PRICE : self::SUBSCRIPTION_MONTH_PRICE);
    }

    /**
     * Получение типа подписки организации
     * @return int
     */
    public function getSubscriptionType()
    {
        return SubscriptionParams::find()->forOrganization($this->organization->id)->onlyActive()->exists() ? Subscription::SUBSCRIPTION_TYPE_BUSINESS : Subscription::SUBSCRIPTION_TYPE_DEFAULT;
    }

    /**
     * Получение суммарное кол-во ограничений для для организаций
     */
    public function getSubscriptionsLimitationsSummary(): SubscriptionsLimitations
    {
        $limitations = new SubscriptionsLimitations(0,0,0);
        $activeSubscriptions = SubscriptionParams::find()->forOrganization($this->organization->id)->onlyActive()->all();
        if (empty($activeSubscriptions)) {
            return SubscriptionsLimitations::getDefault();
        }
        foreach ($activeSubscriptions as $subscription) {
            $limitations->actions += $subscription->actions_count;
            $limitations->salePoints += $subscription->salepoints_count;
            $limitations->cashiersCount += $subscription->cashiers_count;
        }
        return $limitations;
    }

    /**
     * Покупка подписки
     * @param SubscriptionParams $params
     * @param int $periodType
     * @throws SubscriptionException
     * @throws \yii\db\Exception
     */
    public function buySubscription(SubscriptionParams $params, int $periodType)
    {
        $price = $this->calculateSubscriptionPlan($params, $periodType);
        $dbTransaction = \Yii::$app->getDb()->beginTransaction();
        if (!$this->organization->owner->hasEnoughtMoney($price)) {
            throw new SubscriptionException(sprintf("На счету не достаточно средств для оформление подписки, стоимость подписки сотавляет %dCb", $price));
        }
        $transactionCreator = new TransactionCreator($this->organization->owner);
        $transactionCreator->createSubscriptionTransaction($price);
        $this->organization->owner->decreaseBalance($price);
        $params->expire_at = time() + ($periodType == self::SUBSCRIPTION_TYPE_MONTH ? self::SECONDS_IN_MONTH : self::SECONDS_IN_YEAR);
        if (!$params->save()) {
            throw new SubscriptionException("Не удалось купить подписку");
        }
        $this->partnerProgrammPayment->onOrganizationSubscriptionBought($this->organization->owner->refererUser, $this->organization, $price);
        $dbTransaction->commit();
    }
}