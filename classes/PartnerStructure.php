<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 12/10/2019
 * Time: 11:40
 */

namespace app\classes;


use app\models\data\UserStructureItem;
use app\models\Organization;
use app\models\User;

class PartnerStructure
{
    const DEAULT_PARTNER_ID = 5;

    public function getNearestPartner(User $referer)
    {
        while ($referer) {
            if (in_array($referer->user_type, [User::USER_TYPE_PARTNER])) {
                return $referer;
            }
            $referer = $referer->refererUser;
        }
    }

    public function getNearestPartnerOrAgent(User $referer)
    {
        while ($referer) {
            if (in_array($referer->user_type, [User::USER_TYPE_PARTNER, User::USER_TYPE_AGENT])) {
                return $referer;
            }
            $referer = $referer->refererUser;
        }
    }

    /**
     * Добавление торговой точки в структуру ближайшего агента или партнера
     * @param User $refererUser
     * @param Organization $organization
     */
    public function findAndSetMentorForOrg(User $refererUser, Organization $organization)
    {
        $refererUser = $this->getNearestPartnerOrAgent($refererUser);
        $organization->link("mentor", $refererUser);
    }

    public function addUserToStructure(?User $mentor, User $user)
    {
        \Yii::$app->db->createCommand()->insert("partner_structure", [
            'mentor_id' => $mentor->getId() ?? null,
            'user_id' => $user->getId(),
            'created_at' => time()
        ])->execute();
    }

    public function addNewUser(User $referer, User $user)
    {
        $nearestPartnerOrAgent = $this->getNearestPartner($referer);
        $this->addUserToStructure($nearestPartnerOrAgent, $user);
    }

    public function isUserExistInStructure(User $user)
    {
        return User::find()
            ->innerJoin("partner_structure ps", "ps.user_id=users.id")
            ->where(['users.id' => $user->getId()])
            ->exists();
    }

    public function userHasChilds(User $parent)
    {
        return User::find()
            ->leftJoin("partner_structure ps", "ps.user_id=users.id")
            ->where("ps.mentor_id={$parent->getId()}")->exists();
    }

    public function getReferalsCount(User $parent)
    {
        return User::find()->where(['referer' => $parent->getId()])->count();
    }

    /**
     * @param User $parentUser
     * @param int $level
     * @return array
     */
    public function getStructureUsers(User $parentUser, $level = 1)
    {
        $result = [];
        $childUsers = User::find()
            ->leftJoin("partner_structure ps", "ps.user_id=users.id")
            ->where("ps.mentor_id={$parentUser->getId()}")->all();
        foreach ($childUsers as $child) {
            if ($this->userHasChilds($child)) {
                $result = array_merge($result, $this->getStructureUsers($child, ++$level));
            }
            $result[] = new UserStructureItem($level, $this->getReferalsCount($child), $child);
        }
        return $result;
    }
}