<?php
/**
 * Класс для начисления вознаграждений по партнерской программе
 * Created by PhpStorm.
 * User: ruslan
 * Date: 09/10/2019
 * Time: 14:59
 */

namespace app\classes;


use app\models\Organization;
use app\models\User;

class PartnerProgrammPayment
{
    private $parentUser;
    const MAX_LEVEL_DEPTH = 4;
    /**
     * вознаграждение за регистрацию первой линии
     * @var array
     */
    public static $userRegistrationsPayment = [
        User::USER_TYPE_PARTNER => 1,
        User::USER_TYPE_AGENT => 1,
        User::USER_TYPE_DEFAULT => 1,
    ];
    /**
     * Вознаграждение за покупку партнерского пакета
     * @var array
     */
    public static $partnerStatusBuyPayment = [
        User::USER_TYPE_PARTNER => 30000,
        User::USER_TYPE_AGENT => 20000,
        User::USER_TYPE_DEFAULT => 1000,
    ];
    /**
     * Вознаграждение за покупку подписки торговой точкой
     * @var array
     */
    public static $orgSubscriptionBuyPayment = [
        User::USER_TYPE_DEFAULT => 0,
        User::USER_TYPE_AGENT => 400,
        User::USER_TYPE_PARTNER => 600
    ];
    public static $levelFeePercentage = [
        10, 5, 2, 1, 0.5
    ];

    /**
     * Вознаграждение за покупку агентского пакета
     * @var array
     */
    public static $agentStatusBuyPayment = [
        User::USER_TYPE_PARTNER => 9000,
        User::USER_TYPE_AGENT => 6000,
        User::USER_TYPE_DEFAULT => 300
    ];

    public function __construct(?User $user)
    {
        $this->parentUser = $user;
    }

    /**
     * Вознаграждение за регистрацию реферала
     * @param User $childUser
     */
    public function onChildUserRegistered(User $childUser)
    {
        $feeSum = self::$userRegistrationsPayment[$this->parentUser->user_type] ?? 0;
        $transactionCreator = new TransactionCreator($this->parentUser);
        $transactionCreator->createRegistrationFeeTransaction($childUser, $feeSum);
    }

    /**
     * Комиссия за покупку агенсткого пакета рефералом
     * @param User $childUser
     */
    public function onAgentSubscriptionBought(User $childUser)
    {
        $feeSum = self::$agentStatusBuyPayment[$this->parentUser->user_type] ?? 0;
        $transactionCreator = new TransactionCreator($this->parentUser);
        $transactionCreator->createAgentFeeTransaction($childUser, $feeSum);
    }

    /**
     * Комиссия за покупку партнерского пакета рефералом
     * @param User $childUser
     */
    public function onPartnerSubscriptionBought(User $childUser)
    {
        $feeSum = self::$partnerStatusBuyPayment[$this->parentUser->user_type] ?? 0;
        $transactionCreator = new TransactionCreator($this->parentUser);
        $transactionCreator->createPartnerFeeTransaction($childUser, $feeSum);
    }


    /**
     * Расчет вознаграждения по уровням
     * @param $sum - проплаченная сумма
     * @param User $sourceUser - Пользователь с транзакции которого идут начисления
     * @param User $startFrom - С какого пользователя начинать начисление
     * @throws \Exception
     */
    public function createLevelFeeTransaction($sum, User $sourceUser, User $startFrom)
    {
        $level = 0;
        $user = $startFrom->mentor;
        while ($user && $level <= self::MAX_LEVEL_DEPTH) {
            if ($user->user_type == User::USER_TYPE_PARTNER) {
                $transactionCreator = new TransactionCreator($user);
                $transactionCreator->createPartnerLevelFeeTransaction($sourceUser, $this->calculateFeeForLevel($level, $sum), $level);
            }
            $user = $user->mentor;
            $level++;
        }
    }


    /**
     * начисление вознаграждение за покупку подписки организацией
     * @param User|null $organizationReferer - пользователь пригласивший организацию
     * @param Organization $organization - организация, оплатившая подписку
     * @param $sum - сумма подписки
     * @throws \Exception
     */
    public function onOrganizationSubscriptionBought(?User $organizationReferer, Organization $organization, $sum)
    {
        if (!$organizationReferer) {
            return;
        }

        if (in_array($organizationReferer->user_type, [User::USER_TYPE_PARTNER, User::USER_TYPE_AGENT])) { // вознаграждения за покупку подписки получают только агенты или партнеры
            $feeSum = self::$orgSubscriptionBuyPayment[$organizationReferer->user_type];
            $transactionCreator = new TransactionCreator($organizationReferer);
            $transactionCreator->createSubscriptionPaymentFee($organization->owner, $feeSum);
        }
        if ($organization->hasMentor()) {
            $this->createLevelFeeTransaction($sum, $organization->owner, $organization->mentor);//начисление вознаграждения вверх по структуре
        }
    }

    /**
     * Расчет вознаграждения для определенного уровня
     * @param int $level
     * @param $sum
     * @return float|int
     * @throws \Exception
     */
    private function calculateFeeForLevel(int $level, $sum)
    {
        if (!isset(self::$levelFeePercentage[$level])) {
            throw new \Exception(sprintf("level %d is undefined", $level));
        }
        return $sum / 100 * self::$levelFeePercentage[$level];
    }


}