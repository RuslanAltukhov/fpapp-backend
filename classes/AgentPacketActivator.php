<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 18/11/2019
 * Time: 22:31
 */

namespace app\classes;


use app\classes\adapters\PartnerLimitationsAdapter;
use app\classes\Exception\UserChangeStatusException;
use app\models\User;

class AgentPacketActivator
{
    private $limitationFetcher;
    private $whichBeActivated;

    public function __construct(PartnerLimitationsAdapter $limitationFetcher, User $whichBeActivated)
    {
        $this->limitationFetcher = $limitationFetcher;
        $this->whichBeActivated = $whichBeActivated;
    }

    public function activate()
    {
        $transaction = \Yii::$app->getDb()->beginTransaction();
        if ($this->limitationFetcher->isLimitExcided()) {
            throw new UserChangeStatusException("Вы израсходовали свой лимит на активацию агентских пакетов");
        }

        if ($this->whichBeActivated->isAgent()) {
            throw new UserChangeStatusException(sprintf("Пользователь с id %d уже является агентом", $this->whichBeActivated->getId()));
        }

        if (!$this->whichBeActivated->hasReferer()) {
            throw new UserChangeStatusException(sprintf("Пользователь с id %d не является участником партнерской программы, для участия в партнерской программе необходимо указать id пригласившего в профиле", $this->whichBeActivated->getId()));
        }

        if ($this->whichBeActivated->referer != $this->limitationFetcher->getUser()->getId()) {
            throw new UserChangeStatusException("Для активации пакета по акции, пользователь должен быть вашим рефералам");
        }
        $this->whichBeActivated->user_type = User::USER_TYPE_AGENT;
        $partnerStructure = new PartnerStructure();
        if (!$partnerStructure->isUserExistInStructure($this->whichBeActivated)) {
            $partnerStructure->addNewUser($this->limitationFetcher->getUser(), $this->whichBeActivated);
        }
        $this->whichBeActivated->save();
        $this->limitationFetcher->increaseUsedPackets();
        $transaction->commit();
    }
}