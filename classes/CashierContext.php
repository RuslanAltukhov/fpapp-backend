<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 28/05/2019
 * Time: 19:01
 */

namespace app\classes;


use app\models\Cashier;
use app\models\SalePoint;

class CashierContext
{
    private $cashier;

    public function __construct(Cashier $cashier)
    {
        $this->cashier = $cashier;
    }

    public function isRelatedToSalePoint(SalePoint $salePoint): bool
    {
        return $salePoint->getCashiers()->where(['id' => $this->cashier->id])->exists();
    }


}