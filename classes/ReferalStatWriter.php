<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 29/09/2019
 * Time: 22:43
 */

namespace app\classes;


use app\models\ReferalStatItem;
use app\models\User;

class ReferalStatWriter
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function incrementVisitCounter()
    {
        $model = new ReferalStatItem([
            'user_id' => $this->user->id,
            'action_type' => ReferalStatItem::ACTION_TYPE_VISIT
        ]);
        $model->save();
    }

    public function incrementRegistrationsCounter()
    {
        $model = new ReferalStatItem([
            'user_id' => $this->user->id,
            'action_type' => ReferalStatItem::ACTION_TYPE_REGISTRATION
        ]);
        $model->save();
    }
}