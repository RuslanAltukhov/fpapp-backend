<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 07/11/2019
 * Time: 12:24
 */

namespace app\classes\paymentsystem;


use app\models\User;

interface PaymentSystemInterface
{
    public function getPaymentLink(User $user, $sum);
}