<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 07/11/2019
 * Time: 11:14
 */

namespace app\classes\paymentsystem;


use app\classes\TransactionCreator;
use app\models\Transaction;
use app\models\User;

class Tinkoff implements PaymentSystemInterface
{
    private $terminal;
    private $password;
    const PAYMENT_URL = 'https://securepay.tinkoff.ru/v2/Init';

    public function __construct()
    {
        $tinkoffParams = \Yii::$app->params['tinkoff'];
        $this->password = $tinkoffParams['password'];
        $this->terminal = $tinkoffParams['terminal'];
    }

    private function composeRequestParams($amount, $orderId)
    {
        $params = [
            'TerminalKey' => $this->terminal,
            'Amount' => $amount * 100,
            'OrderId' => $orderId,
        ];
        return $params;
    }

    private function makeJsonRequest($params, $url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen(json_encode($params)))
        );
        return curl_exec($ch);
    }

    private function initPaymentRequest(Transaction $transaction): string
    {
        $params = $this->composeRequestParams($transaction->sum, $transaction->id);
        $response = (array)json_decode($this->makeJsonRequest($params, self::PAYMENT_URL));
        if ($response['ErrorCode'] == 0) {
            return $response['PaymentURL'];
        }

        throw new \Exception("Невозможно инициировать оплату заказа, код ошибки {$response['ErrorCode']}");
    }

    public function getPaymentLink(User $user, $sum)
    {
        $transactionCreator = new TransactionCreator($user);
        $transaction = $transactionCreator->createIncomeTransaction($sum, Transaction::TYPE_BALANCE_CHARGE);
        return $this->initPaymentRequest($transaction);
    }
}