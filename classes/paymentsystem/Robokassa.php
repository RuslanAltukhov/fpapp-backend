<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 04/10/2019
 * Time: 14:37
 */

namespace app\classes\paymentsystem;


use app\classes\TransactionCreator;
use app\models\Transaction;
use app\models\User;

class Robokassa implements PaymentSystemInterface
{
    private $mrchLogin;
    private $pass1;
    private $pass2;
    private $merchantUrl;
    private $isTest;
    const INVOICE_DESCRIPTION = "Пополнение баланса в системе Comeback Plus";

    public function __construct()
    {
        $params = \Yii::$app->params['robokassa'];
        $this->mrchLogin = $params['merch_login'];
        $this->pass1 = $params['pass1'];
        $this->pass2 = $params['pass2'];
        $this->isTest = $params['isTest'];
        $this->merchantUrl = $params['merchant_address'];
    }

    public function getPaymentLink(User $user, $sum)
    {
        $transactionCreator = new TransactionCreator($user);
        $transaction = $transactionCreator->createIncomeTransaction($sum, Transaction::TYPE_BALANCE_CHARGE);
        $signature = $this->getSignatureWithPass1($sum, $transaction->id);
        return $this->merchantUrl . "?" . http_build_query([
                'MerchantLogin' => $this->mrchLogin,
                'OutSum' => $sum,
                'InvId' => $transaction->id,
                'Description' => self::INVOICE_DESCRIPTION,
                'SignatureValue' => $signature,
                'IsTest' => $this->isTest ? 1 : 0
            ]);
    }

    /**
     * Валидация сигнатуры ответа SuccessUrl
     * @param Transaction $transaction
     * @param string $signature
     * @return bool
     */
    public function validateSuccessSignature(Transaction $transaction, string $signature)
    {
        return md5("{$transaction->sum}:{$transaction->id}:{$this->pass1}") == $signature;
    }

    public function validateResultSignature(Transaction $transaction, string $signature)
    {
        return md5("{$transaction->sum}:{$transaction->id}:{$this->pass2}") == $signature;
    }

    public function getSignatureWithPass2($sum, $invoiceId)
    {
        return md5("{$this->mrchLogin}:{$sum}:{$invoiceId}:{$this->pass2}");
    }

    public function getSignatureWithPass1($sum, $invoiceId)
    {
        return md5("{$this->mrchLogin}:{$sum}:{$invoiceId}:{$this->pass1}");
    }
}