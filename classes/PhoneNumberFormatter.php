<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 13/08/2019
 * Time: 14:33
 */

namespace app\classes;


use libphonenumber\PhoneNumber;

class PhoneNumberFormatter
{
    private $phonenumberUtil;
    const DEFAULT_COUNTRY_CODE = "RU";

    public function __construct()
    {
        $this->phonenumberUtil = \libphonenumber\PhoneNumberUtil::getInstance();
    }

    public function getFormattedNumber(string $inputPhone)
    {
        $parsedNaumber = $this->phonenumberUtil->parse($inputPhone, self::DEFAULT_COUNTRY_CODE);
        try {
            return $parsedNaumber->getCountryCode() . $parsedNaumber->getNationalNumber();
        } catch (\libphonenumber\NumberParseException $e) {
            return $inputPhone;
        }
    }

    public function isPhoneNumber(string $text)
    {
        $parsedNumber = $this->phonenumberUtil->parse($text, self::DEFAULT_COUNTRY_CODE);
        return $this->phonenumberUtil->isValidNumberForRegion($parsedNumber, self::DEFAULT_COUNTRY_CODE);
    }
}