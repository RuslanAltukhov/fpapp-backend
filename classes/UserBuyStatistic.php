<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 16/06/2019
 * Time: 19:34
 */

namespace app\classes;


use app\models\Action;
use app\models\ActionSale;
use app\models\Organization;
use app\models\SalePoint;
use app\models\User;

class UserBuyStatistic
{
    private $user;
    private $organization;

    public function __construct(User $user, Organization $organization)
    {
        $this->user = $user;
        $this->organization = $organization;
    }

    /**
     * Акции, в которх участвовал пользователь в торговой точке
     * @param SalePoint $salePoint
     * @return mixed
     */
    public function getActionsInSalePoint(SalePoint $salePoint)
    {
        return Action::find()->innerJoin("action_sales", "action_sales.action_id=actions.id")
            ->where(['action_sales.user_id' => $this->user->id, 'action_sales.salepoint_id' => $salePoint->id])->all();
    }

    /**
     * Торговые точки, в
     * @return array
     */
    public function getSalePoints()
    {
        $salePoints = [];
        $actionSales = ActionSale::find()
            ->innerJoin("users", "users.id=action_sales.user_id")
            ->innerJoin("sale_points", "sale_points.id=action_sales.salepoint_id AND sale_points.organization_id={$this->organization->id}")
            ->with("salePoint")->where(['users.id' => $this->user->id])->groupBy("sale_points.id")->all();
        foreach ($actionSales as $sales) {
            $salePoints[] = $sales->salePoint;
        }
        return $salePoints;
    }


    public function getSalesStatistic()
    {
        $sales = ActionSale::find()->select("action_sales.*,COUNT(action_sales.created_at) as count")->innerJoin("sale_points", 'action_sales.salepoint_id=sale_points.id')->where(
            ['user_id' => $this->user->id,
                'sale_points.organization_id' => $this->organization->id
            ])->groupBy("action_sales.type,MINUTE(action_sales.created_at)")->all();
        return $sales;
    }

    /**
     * Статистика покупок по пользователю по конуретной акции
     * @param Action $action
     * @return array
     */
    public function getBuyStatisticForAction(Action $action)
    {
        return [
            'boughts' => $action->getActionSales()->andWhere(['user_id' => $this->user->id])->count(),
            'gifts' => $action->getActionGifts()->andWhere(['user_id' => $this->user->id])->count()
        ];
    }

}