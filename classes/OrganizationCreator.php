<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 16/10/2019
 * Time: 11:53
 */

namespace app\classes;


use app\models\Organization;
use app\models\User;

class OrganizationCreator
{
    private $owner;
    private $referer;
    private $partnerStructure;

    public function __construct(?User $referer, User $owner)
    {
        $this->owner = $owner;
        $this->referer = $referer;
        $this->partnerStructure = new PartnerStructure();
    }

    public function create()
    {
        $transaction = \Yii::$app->getDb()->beginTransaction();
        $model = Organization::createEmptyForUser($this->owner);
        if ($this->referer && ($mentor = $this->partnerStructure->getNearestPartnerOrAgent($this->referer))) {
            $model->link("mentor", $mentor);
        }
        $transaction->commit();
    }
}