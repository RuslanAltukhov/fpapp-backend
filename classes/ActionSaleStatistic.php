<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 29/06/2019
 * Time: 19:52
 */

namespace app\classes;


use app\models\Action;
use app\models\ActionSale;
use app\models\Cashier;
use app\models\SalePoint;

class ActionSaleStatistic
{
    private $action;

    public function __construct(Action $action)
    {
        $this->action = $action;
    }

    public function getSalePoints()
    {
        return SalePoint::find()->innerJoin("action_sales", "action_sales.salepoint_id=sale_points.id")->where([
            'action_sales.action_id' => $this->action->id
        ])->groupBy("sale_points.id")->all();
    }

    public function getSalesForCashierInSalePoint(Cashier $cashier, SalePoint $salePoint)
    {
        $salesCount = ActionSale::find()->where([
            'salepoint_id' => $salePoint->id,
            'cashier_id' => $cashier->id,
            'action_id' => $this->action->id,
            'type' => ActionSale::TYPE_SALE])->count();
        $giftsCount = ActionSale::find()->where([
            'salepoint_id' => $salePoint->id,
            'cashier_id' => $cashier->id,
            'action_id' => $this->action->id,
            'type' => ActionSale::TYPE_GIFT])->count();
        return ['gifts' => $giftsCount, 'sales' => $salesCount];
    }

    public function getActivity()
    {
        return $this->action->getActivity()->select("action_sales.*,COUNT(created_at) as count")->groupBy("type,MINUTE(created_at)")->all();
    }
}