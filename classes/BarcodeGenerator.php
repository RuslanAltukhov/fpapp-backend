<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 06/07/2019
 * Time: 20:03
 */

namespace app\classes;


use app\models\User;
use Picqer\Barcode\BarcodeGeneratorPNG;
use Picqer\Barcode\Exceptions\BarcodeException;

class BarcodeGenerator
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function generateBarcodeForUser()
    {
        $generator = new  BarcodeGeneratorPNG();
        if (!empty($this->user->barcode_base64)) {
            return;
        }
        try {

            $barcodeEncoded = "data:image/png;base64," . base64_encode($generator->getBarcode((string)$this->user->id, $generator::TYPE_CODE_128));
            $this->user->barcode_base64 = $barcodeEncoded;
            $this->user->save();
        } catch (BarcodeException $e) {
        }
    }
}