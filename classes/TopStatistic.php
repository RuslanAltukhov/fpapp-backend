<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 30/06/2019
 * Time: 16:57
 */

namespace app\classes;


use app\models\ActionSale;
use app\models\Organization;
use app\models\User;

class TopStatistic
{
    private $organization;

    public function __construct(Organization $organization)
    {
        $this->organization = $organization;
    }

    public function getTopActions()
    {
        $data = ActionSale::find()->select("action_sales.*, COUNT(action_sales.id) as count")->innerJoin("actions", 'actions.id=action_sales.action_id')->where([
            'actions.organization_id' => $this->organization->id,
            'action_sales.type' => ActionSale::TYPE_SALE
        ])->groupBy("actions.id")->orderBy("count DESC")->all();


        return $data;
    }

    public function getTopReferals()
    {
        return $this->getTopUsers()
            ->andWhere(['users.referer_organization' => $this->organization->id])
            ->groupBy("users.id")
            ->orderBy("bought_count DESC")->all();
    }

    public function getTopCashiers()
    {
        return $this->organization->getCashiers()
            ->select("cashiers.*,COUNT(action_sales.id) AS sales_count")
            ->innerJoin("action_sales", "action_sales.cashier_id=cashiers.id")
            ->where(['action_sales.type' => ActionSale::TYPE_SALE])
            ->groupBy("cashiers.id")
            ->orderBy("sales_count DESC")->all();
    }

    private function getTopUsers()
    {
        return User::find()
            ->select("users.*,COUNT(action_sales.id) AS bought_count")
            ->innerJoin("action_sales", 'action_sales.user_id=users.id')
            ->groupBy("users.id")
            ->innerJoin("sale_points", "sale_points.id=action_sales.salepoint_id AND sale_points.organization_id={$this->organization->id}")
            ->where(['action_sales.type' => ActionSale::TYPE_SALE]);
    }

    public function getTopClients()
    {
        return $this->getTopUsers()->where(['<>', 'users.referer_organization', $this->organization->id])->orderBy("bought_count DESC")->all();

    }

    public function getSalesHistory()
    {
        return ActionSale::find()
            ->innerJoin("sale_points", 'action_sales.salepoint_id=sale_points.id')
            ->where(['sale_points.organization_id' => $this->organization->id])->all();
    }
}