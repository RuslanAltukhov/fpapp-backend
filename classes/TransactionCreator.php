<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 04/10/2019
 * Time: 14:07
 */

namespace app\classes;


use app\models\Transaction;
use app\models\User;

class TransactionCreator
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function createIncomeTransaction($sum, $type = Transaction::TYPE_REGISTRATION_FEE): Transaction
    {
        $model = new Transaction();
        $model->user_id = $this->user->id;
        $model->status = Transaction::STATUS_NEW;
        $model->type = $type;
        $model->direction = Transaction::DIRECTION_INCOME;
        $model->sum = $sum;
        if (!$model->save()) {
            throw new \Exception(sprintf("Can't create new income transaction for user %d with sum  %f", $this->user->id, $sum));
        }
        return $model;
    }

    public function createOutTransaction($sum, $type): Transaction
    {
        $model = new Transaction();
        $model->user_id = $this->user->id;
        $model->status = Transaction::STATUS_NEW;
        $model->type = $type;
        $model->direction = Transaction::DIRECTION_OUTGONE;
        $model->sum = $sum;
        if (!$model->save()) {
            throw new \Exception(sprintf("Can't create new outgone transaction for user %d with sum  %f", $this->user->id, $sum));
        }
        return $model;
    }

    /**
     * Транзакция проплаты подписки
     * @param $sum
     * @throws \Exception
     */
    public function createSubscriptionTransaction($sum)
    {
        $this->createOutTransaction($sum, Transaction::TYPE_SUBSCRIPTION_BUY);
    }

    /**
     * Вознаграждение реферера за покупку подписки торговой точки рефералом
     * @param User $sourceUser
     * @param $sum
     * @throws \Exception
     */
    public function createSubscriptionPaymentFee(User $sourceUser, $sum)
    {
        $this->createFeeTransaction($sourceUser, $sum, Transaction::TYPE_ORG_SUBSCRIPTION_FEE);
    }

    private function createFeeTransaction(User $sourceUser, $sum, int $feeType, $data = [])
    {
        $model = new Transaction();
        $model->user_id = $this->user->id;
        $model->status = Transaction::STATUS_DONE;
        $model->type = $feeType;
        $model->data = serialize($data);
        $model->source_user_id = $sourceUser->getId() ?? 0;
        $model->direction = Transaction::DIRECTION_INCOME;
        $model->sum = $sum;
        $transaction = \Yii::$app->getDb()->beginTransaction();
        if (!$model->save()) {
            throw new \Exception(sprintf("Can't create  fee transaction for user %d with sum  %f", $this->user->id, $sum));
        }
        $this->user->increaseBalance($sum);
        $transaction->commit();
    }

    public function createRegistrationFeeTransaction(User $sourceUser, $sum)
    {
        $this->createFeeTransaction($sourceUser, $sum, Transaction::TYPE_REGISTRATION_FEE);
    }

    public function createAgentFeeTransaction(User $sourceUser, $sum)
    {
        $this->createFeeTransaction($sourceUser, $sum, Transaction::TYPE_AGENT_SUBSCRIPTION_FEE);
    }

    public function createPartnerFeeTransaction(User $sourceUser, $sum)
    {
        $this->createFeeTransaction($sourceUser, $sum, Transaction::TYPE_PARTNER_SUBSCRIPTION_FEE);
    }

    public function createPartnerLevelFeeTransaction(User $sourceUser, $sum, $level)
    {
        $this->createFeeTransaction($sourceUser, $sum, Transaction::TYPE_PARTNER_LEVEL_FEE, ['level' => $level]);
    }

    public function confirmIncomeTransaction(Transaction $transaction)
    {
        if ($transaction->direction != Transaction::DIRECTION_INCOME || $transaction->status == Transaction::STATUS_DONE) {
            return;
        }
        $dbTransaction = \Yii::$app->getDb()->beginTransaction();
        $transaction->dstUser->increaseBalance($transaction->sum);
        $transaction->markAsDone();
        $dbTransaction->commit();
    }
}
