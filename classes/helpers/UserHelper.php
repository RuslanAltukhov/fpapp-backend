<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 06/08/2019
 * Time: 16:44
 */

namespace app\classes\helpers;


class UserHelper
{
    public static function generateRandomPassword(int $length)
    {
        $result = "";
        for ($i = 1; $i <= $length; $i++) {
            $result .= rand(0, 9);
        }
        return $result;
    }
}