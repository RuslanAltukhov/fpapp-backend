<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 11/10/2019
 * Time: 16:11
 */

namespace app\classes;


use app\models\Transaction;
use app\models\User;
use yii\data\ActiveDataProvider;

class TransactionsStatReader
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Получение списка вознаграждения для пользователя
     * @param User $user
     * @return \app\models\TransactionsQuery
     */
    public function getIncomeFeeTransactions()
    {
        return Transaction::find()->where(['direction' => Transaction::DIRECTION_INCOME, 'type' => [Transaction::TYPE_PARTNER_SUBSCRIPTION_FEE,
            Transaction::TYPE_AGENT_SUBSCRIPTION_FEE,
            Transaction::TYPE_PARTNER_LEVEL_FEE,
        ], 'user_id' => $this->user->getId()])->all();
    }

    public function getTransactionsListForUser()
    {
        return Transaction::find()->where(['user_id' => $this->user->getId()])->orderBy("id DESC");
    }

    public function getTransactionsListForUserAsProvider()
    {
        return new ActiveDataProvider([
            'query' => $this->getTransactionsListForUser(),
            'pagination' => [
                'pageSize' => 10,
            ]
        ]);
    }
}