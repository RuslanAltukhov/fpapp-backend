<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 27/08/2019
 * Time: 21:11
 */

namespace app\classes;


class SmscSender
{
    private $login;
    private $password;
    const URL = "https://smsc.ru/sys/send.php";
    private static $instance;

    private function __construct()
    {
        $credentials = \Yii::$app->params['smsc'];
        $this->login = $credentials['login'];
        $this->password = $credentials['password'];

    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new static();
        }
        return self::$instance;
    }

    private function getUrlFormatted(string $message, string $phone)
    {
        return self::URL . "?login={$this->login}&psw={$this->password}&phones={$phone}&mes=" . urlencode($message);
    }

    public function sendMessage(string $message, string $phone)
    {
        if (defined("YII_ENV") && YII_ENV == "dev") {
            return;
        }

        $urlFormatted = $this->getUrlFormatted($message, $phone);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $urlFormatted);
        $result = curl_exec($ch);
    }
}