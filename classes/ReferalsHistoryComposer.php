<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 02/10/2019
 * Time: 19:32
 */

namespace app\classes;


use app\models\User;

class ReferalsHistoryComposer
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getAlReferals($user)
    {
        $data = [];
        $users = User::find()
            ->select("*,(SELECT COUNT(*) FROM users WHERE users.referer = u.id) AS childs")
            ->from("users u")
            ->where(['referer' => $user->id])->orderBy("id DESC")->all();
        foreach ($users as $item) {
            if ($item->childs > 0) {
                $data = array_merge($this->getAlReferals($item), $data);
            }
            $data[] = $item;
        }
        return $data;
    }


    public function composeActivityHistory()
    {
        $users = $this->getAlReferals($this->user);
        return $users;
    }
}