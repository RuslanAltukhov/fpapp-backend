<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 26/07/2019
 * Time: 14:53
 */

namespace app\classes;


use app\models\Action;
use app\models\SalePoint;
use app\models\User;

class SalepointActions
{

    private $salePoint;

    public function __construct(SalePoint $salePoint)
    {
        $this->salePoint = $salePoint;
    }

    public function getActions()
    {
        $actions = $this->salePoint->getActions()->with("style")->with("organization");
        return $actions;
    }
}