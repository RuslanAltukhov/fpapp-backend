<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 30/06/2019
 * Time: 13:23
 */

namespace app\classes;


use app\models\Action;
use app\models\ActionSale;
use app\models\Cashier;
use app\models\SalePoint;

class SalePointStatistic
{
    private $salePoint;

    public function __construct(SalePoint $salePoint)
    {
        $this->salePoint = $salePoint;
    }

    public function getSalesLog()
    {
        return $this->salePoint->getSales()->select("action_sales.*,COUNT(created_at) as count")->groupBy("type,MINUTE(created_at)")->all();
    }

    public function getActions()
    {
        return Action::find()->innerJoin("action_sales", "action_sales.action_id=actions.id")
            ->where(['action_sales.salepoint_id' => $this->salePoint->id])
            ->groupBy("actions.id")->all();
    }

    public function getCashierSaleStatistic(Cashier $cashier)
    {
        return [
            'sales' => $this->salePoint->getSales()->where(['cashier_id' => $cashier->id, 'type' => ActionSale::TYPE_SALE])->count(),
            'gifts' => $this->salePoint->getSales()->where(['cashier_id' => $cashier->id, 'type' => ActionSale::TYPE_GIFT])->count(),
        ];
    }
}