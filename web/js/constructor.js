var Constructor = {
    changeTextColor: function (el, newColor) {
        $(el).css({"color": newColor});
    },
    //TODO: чтобы менялся цвет обводки, нужно для .fshk-brdr менять цвет свойства  "stroke" все стили теперь меняются только в элементе <li>
    itemTemplates: {
        defaultItem: '<li class="col fshk fshk-default fshk-brdr fshk-default-brdr num num-default" style="color: #ffffff; fill: #540d6e; stroke: #ffffff;">\n' +
        '<svg version="1.1" viewBox="0 0 500 500" preserveAspectRatio="xMinYMin meet" class="svg-content">\n' +
        '<circle stroke-width="20"   stroke-miterlimit="20" cx="250" cy="250" r="240"/>\n' +
        '</svg>\n' +
        '</li>',
        activeItem: '<li class="col active checked fshk fshk-active fshk-brdr fshk-active-brdr num num-active" style="color: #540d6e; fill: #ee4264; stroke: #ee4264;">\n' +
        '<svg version="1.1" viewBox="0 0 500 500" preserveAspectRatio="xMinYMin meet" class="svg-content">\n' +
        '<circle stroke-width="20"   stroke-miterlimit="20" cx="250" cy="250" r="240"/>\n' +
        '</svg>\n' +
        '</li>',
        lastItem: '<li class="col fshk fshk-last fshk-brdr fshk-last-brdr num num-last" style="color: #540d6e; fill: #ffd23f; stroke: #ffd23f;">\n' +
        '<svg version="1.1" viewBox="0 0 500 500" preserveAspectRatio="xMinYMin meet" class="svg-content">\n' +
        '<circle stroke-width="20"   stroke-miterlimit="20" cx="250" cy="250" r="240"/>\n' +
        '</svg>\n' +
        '</li>'
    },

    getItemInitialized(type, num) {
        if (!this.itemTemplates[type]) {
            throw new Error("undefined type:" + type);
        }
        var newItem = $(this.itemTemplates[type]);
        newItem.find(".num").text(num);
        return newItem;
    },
    changeBackgroundColor: function (el, newColor) {
        $(el).css({"background-color": newColor});
    },
    changeFillColor: function (el, newColor) {
        $(el).css({"fill": newColor});
    },
    changeStrokeColor: function (el, newColor) {
        $(el).css({"stroke": newColor});
    },
    changeTextColor: function (el, newColor) {
        $(el).css({"color": newColor});
    },
    updateItemsCount(el, newCount) {
        var domEl = $(el);
        var classes = "card_promo-fishki row fishek-" + newCount;
        $(domEl).removeClass().addClass(classes);
    },
    initStyleChangeListeners: function () {
        $("#action_bg_color").change(function () {
            Constructor.changeBackgroundColor($("#promo"), this.value);
        });
        $("#action_text_color").change(function () {
            Constructor.changeTextColor($(".card_promo-header-text"), this.value);
        });
        $("#item_bg_color").change(function () {
            Constructor.changeFillColor('.fshk-default', this.value);
        });
        $("#item_text_color").change(function () {
            Constructor.changeTextColor('.num-default', this.value);
        });
        $("#item_border_color").change(function () {
            Constructor.changeStrokeColor('.fshk-default-brdr', this.value);
            // Constructor.changeTextColor('.num-default', this.value);
        });
        $("#active_item_bg_color").change(function () {
            Constructor.changeFillColor('.fshk-active', this.value);
        });
        $("#active_item_border_color").change(function () {
            Constructor.changeStrokeColor('.fshk-active-brdr', this.value);
            //Constructor.changeTextColor('.num-active', this.value);
        });
        $("#active_item_text_color").change(function () {
            Constructor.changeTextColor('.fshk-active', this.value);
        });
        $("#last_item_bg_color").change(function () {
            Constructor.changeFillColor('.fshk-last', this.value);
        });
        $("#last_item_text_color").change(function () {
            Constructor.changeTextColor('.fshk-last', this.value);
        });
        $("#last_item_border_color").change(function () {
            Constructor.changeStrokeColor('.fshk-last-brdr', this.value);
            // Constructor.changeTextColor('.num-last', this.value);
        });
        $("#footer_bg_color").change(function () {
            Constructor.changeBackgroundColor('.card_promo-footer', this.value);
        });
        $("#footer_text_color").change(function () {
            Constructor.changeTextColor('.card_promo-footer', this.value);
        });
        $("#need_to_buy").change(function () {
            Constructor.loadItems(parseInt(this.value));
            $("input[type='color']").change();
        });
    },
    loadItems: function (count) {
        var itemsContainer = $("#items-list").empty();
        var firstActive = this.getItemInitialized('activeItem', 1);
        itemsContainer.append(firstActive);
        for (var i = 2; i <= count; i++) {
            itemsContainer.append(this.getItemInitialized('defaultItem', i));
        }
        itemsContainer.append(this.getItemInitialized('lastItem', count + 1));
        this.updateItemsCount(itemsContainer, count);
    },
    readURL: function (input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#card-logo').attr("src", e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    },
    setExparation: function (current, endAt) {

        var diffTime = endAt.getTime() - current.getTime();
        console.log(diffTime);
        if (isNaN(diffTime)) {
            $("#action-exparation").text("Бессрочно");
            return;
        }
        if (diffTime < 0) {
            $("#action-exparation").text("Бессрочно");
            return;
        }
        var days = Math.floor(diffTime / (1000 * 60 * 60 * 24));
        var hours = Math.floor((diffTime - (86400000 * days)) / (1000 * 60 * 60));
        var minutes = Math.floor((diffTime - (86400000 * days) - (3600000 * hours)) / 60000);
        $("#action-exparation").text("Истекает через: " + days + "дн. " + hours + "ч. ")
    },
    getNow: function () {
        var date = new Date();
        return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " + date.getHours() + ":" + date.getMinutes();
    },
    initUI: function () {

        var startAt = $("#start_at");
        if (startAt.val().trim().length == 0) {
             startAt.val(this.getNow());
        }

        $("#logo-file-input").change(function () {
            Constructor.readURL(this);
        });
        $("#end_at").change(function () {
            Constructor.setExparation(new Date(), new Date($(this).val()));
        });
        $("#action-description-input").keyup(function () {
            $("#action-description").text($(this).val());
        });
    },
    loadStyles: function (styles) {
        $("input[type='color'], #need_to_buy").change();
    }

}

$(document).ready(function () {
    Constructor.initStyleChangeListeners();
    Constructor.initUI();
    //  Constructor.loadItems(itemsCount);
    Constructor.loadStyles(styles);
});