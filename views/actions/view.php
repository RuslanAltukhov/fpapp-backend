<?

use app\models\params\ActionParams;

?>
<script>
    function getExpiration(current, endAt) {

        var diffTime = endAt.getTime() - current.getTime();
        console.log(diffTime);
        if (isNaN(diffTime)) {
            return "Бессрочно";
        }
        if (diffTime < 0) {
            return "Бессрочно";
        }
        var days = Math.floor(diffTime / (1000 * 60 * 60 * 24));
        var hours = Math.floor((diffTime - (86400000 * days)) / (1000 * 60 * 60));
        var minutes = Math.floor((diffTime - (86400000 * days) - (3600000 * hours)) / 60000);
        return "Истекает через: " + days + "дн. " + hours + "ч. ";
    }
</script>
<div class="row">
    <div class="col-md-4 margin-bottom-20">
        <div class="box-content card">
            <h4 class="box-title">Карта акции</h4>
            <div class="card-content">
                <div class="card_promo card-offer" id="promo"
                     style="background: <?= $model->style->action_bg_color ?>;position: static; transform: none; margin: 0 auto;">
                    <div class="card_promo-header card-header">
                        <!--Здесь логотип-->
                        <div class="card_promo-header-logo company-logo"
                             style="background-image: url(/<?= $model->style->logo_url ?>);"></div>
                        <!--Здесь цвет текста-->
                        <div class="card_promo-header-text company-offer"
                             style="color: <?= $model->style->action_text_color ?>;">
                            <!--Здесь текст из textarea -->
                            <?= $model->description ?>
                        </div>
                    </div>
                    <div class="card_promo-container card-content card-content-padding fishki">
                        <!--Здесь меняем класс, в зависимости от выбранного кол-ва фишек: fishek-3, fishek-4... -->
                        <ol class="card_promo-fishki row">
                            <li class="col active"
                                style="fill: <?= $model->style->active_item_bg_color ?>; stroke: <?= $model->style->active_item_border_color ?>; color: <?= $model->style->active_item_text_color ?>">
                                <svg version="1.1" viewBox="0 0 500 500" preserveAspectRatio="xMinYMin meet"
                                     class="svg-content">
                                    <circle stroke-width="20" stroke-miterlimit="20" cx="250" cy="250" r="240"></circle>
                                </svg>
                            </li>
                            <?
                            $pos = 0;
                            while ($pos++ < $model->need_to_buy - 1) { ?>
                                <!-- здесь лупим фишки-->
                                <li class="col"
                                    style="fill: <?= $model->style->item_bg_color ?>; stroke: <?= $model->style->item_border_color ?>; color: <?= $model->style->item_text_color ?>">
                                    <svg version="1.1" viewBox="0 0 500 500" preserveAspectRatio="xMinYMin meet"
                                         class="svg-content">
                                        <circle stroke-width="20" stroke-miterlimit="20" cx="250" cy="250"
                                                r="240"></circle>
                                    </svg>
                                </li>
                            <? } ?>
                            <li class="col"
                                style="fill: <?= $model->style->last_item_bg_color ?>; stroke: <?= $model->style->last_item_border_color ?>; color: <?= $model->style->last_item_text_color ?>;">
                                <svg version="1.1" viewBox="0 0 500 500" preserveAspectRatio="xMinYMin meet"
                                     class="svg-content">
                                    <circle stroke-width="20" stroke-miterlimit="20" cx="250" cy="250" r="240"></circle>
                                </svg>
                                <!--Здесь цвет такой же, как у обводки плюшки...
                                    <span class="num" style="color:<?= $model->style->last_item_border_color ?>;">5</span>-->
                                <!--Здесь border и color такой же, как у обводки плюшки и фон, как у плюшки...

                                <div class="plshk-count" style="border-color: #000; color: #000; background: #caa170;">
                                    0
                                </div>-->
                            </li>
                        </ol>
                    </div>
                    <!--Здесь фон и цвет текста подвала -->
                    <div class="card_promo-footer card-footer"
                         style="color: <?= $model->style->footer_text_color ?>; background: <?= $model->style->footer_bg_color ?>;">
                        <div class="row">
                            <div class="column card-company-name">
                                <?= $model->organization->name; ?>
                            </div>
                            <div class="column text-right">
                                <script>
                                    document.write(getExpiration(new Date(), new Date(" <?= $model->end_at; ?>")));
                                </script>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="card-content">
                <table class="table table-hover no-margin">
                    <tbody>
                    <tr>
                        <td>ID</td>
                        <td>
                            <?= $model->id ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Название</td>
                        <td>
                            <?= $model->name ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Статус</td>
                        <td>
                            <?
                            $statusClass = [ActionParams::STATUS_NEW => "new", ActionParams::STATUS_ACTIVE => 'active', ActionParams::STATUS_CLOSED => 'archived', ActionParams::STATUS_REJECTED => "rejected"];
                            echo sprintf('<span class="bage %s">%s</span>',
                                $statusClass[$model->status_id] ?? '',
                                ActionParams::getStatusNameById($model->status_id));
                            ?>
                        </td>
                    </tr>
                    <?
                    if ($model->firstSale) {
                        ?>
                        <tr>
                            <td>Старт</td>
                            <td>
                                <?= date("d.m.Y H:i", strtotime($model->firstSale->created_at)) ?> ТТ <a
                                        href="/sale-points/update/<?= $model->firstSale->salePoint->id; ?>"><?= $model->firstSale->salePoint->name; ?></a>.
                                Кассир:
                                <a href="/cashiers/update/<?= $model->firstSale->cashier->id; ?>"><?= $model->firstSale->cashier->name; ?></a>
                            </td>
                        </tr>
                    <? } ?>
                    <tr>
                        <td>Торговые Точки</td>
                        <td>
                            <? foreach ($model->salePoints as $salePoint): ?>
                                <h5><a href="/sale-points/update/<?= $salePoint->id ?>"><?= $salePoint->name ?></a></h5>
                            <? endforeach; ?>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="box-footer text-center">
                <a href="/actions/<?= $model->id ?>/update" class="btn btn-default btn-sm">Редактировать</a>
                <? if ($model->status_id == ActionParams::STATUS_ACTIVE) { ?>
                    <a href="/actions/<?= $model->id ?>/move-to-archive" class="btn btn-danger btn-sm">Завершить
                        акцию</a>
                <? } ?>
                <? if ($model->status_id == ActionParams::STATUS_CLOSED) { ?>
                    <a href="/actions/<?= $model->id ?>/activate" class="btn btn-success btn-sm">Активировать
                        акцию</a>
                <? } ?>
            </div>
        </div>
    </div>
    <div class="col-md-4 margin-bottom-20">
        <div class="box-content card">
            <h4 class="box-title">Статистика акции</h4>
            <div class="card-content">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Кассир</th>
                        <th class="text-center">Плюсы</th>
                        <th class="text-center">Бонусы</th>
                    </tr>
                    </thead>
                    <tbody>
                    <? foreach ($saleStatistic->getSalePoints() as $salePoint) { ?>
                        <tr>
                            <th colspan="3" class="text-center"><a
                                        href="/sale-points/update/<?= $salePoint->id ?>"><?= $salePoint->name ?></a>
                            </th>
                        </tr>
                        <?

                        foreach ($salePoint->cashiers as $cashier) {
                            $stat = $saleStatistic->getSalesForCashierInSalePoint($cashier, $salePoint);
                            ?>
                            <tr>
                                <td><a href="/cashiers/update/<?= $cashier->id ?>"><?= $cashier->name ?></a></td>
                                <td class="text-center">
                                    <?= $stat['sales'] ?>
                                </td>
                                <td class="text-center">
                                    <?= $stat['gifts'] ?>
                                </td>
                            </tr>
                        <? } ?>
                    <? } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-4 margin-bottom-20">
        <div class="box-content card">
            <h4 class="box-title">Лог активностей</h4>
            <div class="card-content" style="max-height: 80vh; overflow-y: auto">
                <ul class="notice-list">
                    <? foreach ($saleStatistic->getActivity() as $activityItem) { ?>
                        <li>
                                <span class="icon bg-<?= $activityItem->type == \app\models\ActionSale::TYPE_SALE ? "success " : "violet " ?>"><i
                                            class="fa fa-<?= $activityItem->type == \app\models\ActionSale::TYPE_SALE ? "plus" : "check" ?>-circle"></i></span>
                            <span class="title">Выдана <?= $activityItem->type == \app\models\ActionSale::TYPE_SALE ? "фишка" : "плюшка" ?>
                                : <?= $activityItem->count ?>шт.</span>
                            <span class="desc">Кассир <a
                                        href="/cashiers/update/<?= $activityItem->cashier->id ?>"><?= $activityItem->cashier->name ?></a> выдал 1 фишку по акции <a
                                        href="/actions/view/<?= $activityItem->action->id ?>">"<?= $activityItem->action->name ?>
                                    "</a> клиенту <a
                                        href="/users/view/<?= $activityItem->user->id ?>"><?= $activityItem->user->name ?></a></span>
                            <span class="time"><?= date("d.m.Y в H:i", strtotime($activityItem->created_at)) ?></span>
                        </li>
                    <? } ?>
                </ul>
            </div>
        </div>
    </div>
</div>