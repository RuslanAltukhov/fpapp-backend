<?

use kartik\datetime\DateTimePicker;
use yii\widgets\ActiveForm;

$this->registerJsFile("/js/constructor.js", ['depends' => [\yii\web\JqueryAsset::class]]);
$form = ActiveForm::begin([
]);
?>
    <script>
        var itemsCount = <?=$model->need_to_buy?>;
        var styles = {
            actionBgColor: '<?=$model->action_bg_color?>',
            actionTextColor: '<?=$model->action_text_color?>',
            itemBgColor: '<?=$model->item_bg_color?>',
            itemBorderColor: '<?=$model->item_border_color?>',
            itemTextColor: '<?=$model->item_text_color?>',
            activeItemBgColor: '<?=$model->active_item_bg_color?>',
            activeItemTextColor: '<?=$model->active_item_text_color?>',
            activeItemBorderColor: '<?=$model->active_item_border_color?>',
            footerBgColor: '<?=$model->footer_bg_color?>',
            footerTextColor: '<?=$model->footer_text_color?>'
        }



    </script>

    <!-- Костылиище -->
    <style>
        .box-content {
            background: transparent !important;
            box-shadow: none !important;
        }
    </style>

    <h1>Конструктор акции</h1>
    <div class="row" id="constructor">


        <div class="col-md-3 left-options">
            <h3 class="title">Параметры акции</h3>
            <?= $form->field($model, 'name')->textInput(['placeholder' => 'Введите название']) ?>
            <?= $form->field($model, 'start_at')->widget(DateTimePicker::class, [
                'options' => ['placeholder' => '', 'autocomplete' => 'Off', 'id' => 'start_at'],
                'type' => DateTimePicker::TYPE_INPUT,
                'pluginOptions' => [
                    'autoclose' => true
                ]
            ]) ?>

            <?= $form->field($model, 'end_at')->widget(DateTimePicker::class, [
                'options' => ['placeholder' => '', 'autocomplete' => 'Off', 'id' => "end_at"],
                'type' => DateTimePicker::TYPE_INPUT,
                'pluginOptions' => [
                    'autoclose' => true
                ]
            ]) ?>

            <?= $form->field($model, 'need_to_buy')->dropDownList(\app\models\params\ActionParams::getNeedToBuyRange(), ['id' => 'need_to_buy']) ?>
            <?= $form->field($model, 'salePointIds')->dropDownList(\yii\helpers\ArrayHelper::map($salePoints, 'id', 'name'), ['size' => 1, 'class' => 'select2_1', 'multiple' => 'multiple']); ?>

            <?= $form->field($model, 'description')->textarea(['id' => 'action-description-input', 'placeholder' => 'Введите краткое и ёмкое описание акции. Объясните суть несколькими словами...']) ?>
            <?= $form->field($model, 'category_id')->dropDownList(['' => 'Выберите категорию'] + \yii\helpers\ArrayHelper::map($categories, 'id', 'title')) ?>
        </div>
        <div class="col-md-6 bg-darker">
            <div class="construcor-container">
                <!--Здесь фон карточки-->
                <div class="card_promo card-offer" id="promo" style="background-color: #540d6e;">
                    <div class="card_promo-header card-header">
                        <!--Здесь логотип-->
                        <div class="card_promo-header-logo company-logo">
                            <img src="<?= $model->getLogoPath() ?>" id="card-logo"/>
                        </div>
                        <!--Здесь цвет текста-->
                        <div id="action-description" class="card_promo-header-text company-offer" style="color: #fff;">
                            <?= empty($model->description) ? " Введите краткое и ёмкое описание акции. Объясните суть несколькими словами..." : $model->description ?>
                        </div>
                    </div>
                    <div class="card_promo-container card-content card-content-padding fishki">
                        <ol id="items-list" class="row no-gap card_promo-fishki card_promo-fishki">

                        </ol>
                    </div>
                    <!--Здесь фон и цвет текста подвала -->
                    <div class="card_promo-footer card-footer" style="background-color: #ee4265; color: #540d6e;">
                        <div class="row">
                            <div class="column card-company-name">
                                <!--TODO: здесь подставляем назване компанииы -->
                                <?= $organization->name; ?>
                            </div>
                            <div id="action-exparation" class="column text-right card-expires">
                                Бессрочно
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="save-card">
                <!-- <div>
                     <input type="checkbox" id="save-tmpl" checked="checked">
                     <label class="label-inline" for="save-tmpl">Сохранить, как шаблон</label>
                 </div>-->
                <input class="btn btn-primary" type="submit" value="Сохранить">
            </div>

        </div>
        <div class="col-md-3">
            <h3 class="title">Внешний вид</h3>
            <div class="constructor-options">

                <?= $form->field($model, 'logo')->fileInput(['id' => 'logo-file-input']) ?>

                <fieldset>
                    <h5>Карта</h5>
                    <div class="row">
                        <div class="column">
                            <?= $form->field($model, 'action_bg_color',
                                ['options' => ['tag' => false],
                                    'template' => "{input}{label}"])->input("color", ['id' => 'action_bg_color', 'class' => '']) ?>
                        </div>
                        <div class="column">
                            <?= $form->field($model, 'action_text_color',
                                ['options' => ['tag' => false],
                                    'template' => "{input}{label}"])->input("color", ['class' => '', 'id' => 'action_text_color']) ?>
                        </div>
                    </div>
                    <h5>Фишки</h5>
                    <div class="row">
                        <div class="column">
                            <?= $form->field($model, 'item_bg_color',
                                ['options' => ['tag' => false],
                                    'template' => "{input}{label}"])->input("color", ['class' => '', 'id' => 'item_bg_color']) ?>

                        </div>
                        <div class="column">
                            <?= $form->field($model, 'item_border_color',
                                ['options' => ['tag' => false],
                                    'template' => "{input}{label}"])->input("color", ['class' => '', 'id' => 'item_border_color']) ?>
                        </div>
                        <!-- Номер-->
                        <div class="column">
                            <?= $form->field($model, 'item_text_color',
                                ['options' => ['tag' => false],
                                    'template' => "{input}{label}"])->input("color", ['class' => '', 'id' => 'item_text_color']) ?>
                        </div>
                    </div>
                    <h5>Плюс</h5>
                    <div class="row">
                        <div class="column">
                            <?= $form->field($model, 'active_item_bg_color',
                                ['options' => ['tag' => false],
                                    'template' => "{input}{label}"])->input("color", ['class' => '',
                                'id' => 'active_item_bg_color']) ?>

                        </div>
                        <div class="column">
                            <?= $form->field($model, 'active_item_border_color',
                                ['options' => ['tag' => false],
                                    'template' => "{input}{label}"])->input("color", ['class' => '', 'id' => 'active_item_border_color']) ?>

                        </div>
                        <!-- Номер-->
                        <div class="column">
                            <?= $form->field($model, 'active_item_text_color',
                                ['options' => ['tag' => false],
                                    'template' => "{input}{label}"])->input("color", ['class' => '', 'id' => 'active_item_text_color']) ?>

                        </div>
                    </div>
                    <h5>Бонус</h5>
                    <div class="row">
                        <div class="column">
                            <?= $form->field($model, 'last_item_bg_color',
                                ['options' => ['tag' => false],
                                    'template' => "{input}{label}"])->input("color", ['class' => '', 'id' => 'last_item_bg_color']) ?>
                        </div>
                        <div class="column">
                            <?= $form->field($model, 'last_item_border_color',
                                ['options' => ['tag' => false],
                                    'template' => "{input}{label}"])->input("color", ['class' => '', 'id' => 'last_item_border_color']) ?>

                        </div>
                        <div class="column">
                            <!-- Номера-->
                            <?= $form->field($model, 'last_item_text_color',
                                ['options' => ['tag' => false],
                                    'template' => "{input}{label}"])->input("color", ['class' => '', 'id' => 'last_item_text_color']) ?>

                        </div>
                    </div>

                    <!--<input type="checkbox" id="show-counter" checked="checked">
                    <label class="label-inline" for="show-counter">Показывать счетчик?</label>-->

                    <h5>Подвал</h5>
                    <div class="row">
                        <div class="column">
                            <?= $form->field($model, 'footer_bg_color',
                                ['options' => ['tag' => false],
                                    'template' => "{input}{label}"])->input("color", ['class' => '', 'id' => 'footer_bg_color']) ?>

                        </div>
                        <div class="column">
                            <?= $form->field($model, 'footer_text_color',
                                ['options' => ['tag' => false],
                                    'template' => "{input}{label}"])->input("color", ['class' => '', 'id' => 'footer_text_color']) ?>

                        </div>
                    </div>
                </fieldset>

            </div>
        </div>

    </div>
<? Activeform::end() ?>