<?

use app\models\params\ActionParams;
use yii\grid\GridView;
use yii\helpers\Html;
use app\models\Action;

?>
<h1>Все акции </h1>
<? $this->beginBlock('action_btn'); ?>
<a href="/actions/create" class="btn btn-success margin-bottom-10 "><i
            class="typcn typcn-plus-outline"></i>Добавить акцию</a>
<? $this->endBlock(); ?>

<div class="row">

    <?= $this->render("_filter", ['searchModel' => $searchModel]); ?>
</div>

<? if (Yii::$app->session->hasFlash("success")) { ?>
    <div class="alert alert-success"><?= Yii::$app->session->getFlash("success") ?></div>
<? } ?>
<div class="row">

    <?= GridView::widget([
        'tableOptions' => [
            'tag' => false,
            'class' => 'table table-striped table-bordered display customers dataTable all-offers',
            'style' => 'width:100%'
        ],
        'dataProvider' => $actionsProvider,
        'columns' => [
            'id',
            [
                'attribute' => 'name',
                'format' => 'raw',
                'value' => function (Action $model) {
                    return "<a href='/actions/view/{$model->id}'>{$model->name}</a>";
                }
            ],
            'description',
            'start_at:datetime',
            'end_at:datetime',
            'created_at:datetime',
            [
                'attribute' => 'status_id',
                'format' => 'raw',
                'value' => function ($model) {
                    $statusClass = [ActionParams::STATUS_NEW => "new", ActionParams::STATUS_ACTIVE => 'active', ActionParams::STATUS_CLOSED => 'archived', ActionParams::STATUS_REJECTED => "rejected"];
                    return sprintf('<span class="bage %s">%s</span>',
                        $statusClass[$model->status_id] ?? '',
                        ActionParams::getStatusNameById($model->status_id));
                }
            ],
            [
                'header' => 'Плюсы',
                'value' => function (Action $model) {
                    return $model->getActionSales()->count();
                }
            ],
            [
                'header' => 'Бонусы',
                'value' => function (Action $model) {
                    return $model->getActionGifts()->count();
                }
            ],
            [
                'header' => 'Действия',
                'class' => \yii\grid\ActionColumn::class,
                'buttons' => [
                    'update' => function ($url, $model) {
                        return sprintf('<a class="bage edit" href="/actions/%d/update" ><i class="fa fa-pencil"></i></a>', $model->id);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fa fa-trash"></i>', "/actions/{$model->id}/move-to-archive", [
                            'data-method' => 'post',
                            'class' => 'bage archive'
                        ]);

                    }
                ],
                'template' => '{update} {delete}',
            ]
            // ...
        ],
    ]) ?>
</div>
