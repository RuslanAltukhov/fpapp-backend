<?

use yii\widgets\ActiveForm;
use app\models\params\ActionParams;

?>
<div class="column">
    <? $form = ActiveForm::begin([
        'method' => 'get',
        'id' => 'actions-filter-form',
        'options' => [
            'style' => 'margin-bottom:0 !important'
        ]
    ]); ?>
    <?= $form->field($searchModel, 'status_id')->dropDownList(ActionParams::getAllStatusNames() + ['' => 'Все'],
        ['class' => '', 'onchange' => '$("#actions-filter-form").submit()']); ?>

    <? ActiveForm::end(); ?>
</div>
<!--<div class="column">
    <label for="ageRangeField">Показывать: </label>
    <select class="bg-light-2" id="ageRangeField">
        <option value="all">Все</option>
        <option value="10">10</option>
        <option value="20">20</option>
        <option value="50">50</option>
        <option value="100">100</option>
    </select>
</div>-->