<?php

use app\assets\ClientRegistrartionAssets;

ClientRegistrartionAssets::register($this);
$this->beginPage() ?>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ru-RU">

<head>
    <?php $this->head() ?>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Регистрация в comeback plus</title>

    <meta name="viewport" content="width=device-width" />
    <style>
        @font-face {font-family: 'TTLakesCondensed-DemiBold';src: url('<?=\yii\helpers\Url::base(true)?>/fonts/3501B7_1B_0.eot');src: url('<?=\yii\helpers\Url::base(true)?>/fonts/3501B7_1B_0.eot?#iefix') format('embedded-opentype'),url('<?=\yii\helpers\Url::base(true)?>/fonts/3501B7_1B_0.woff2') format('woff2'),url('<?=\yii\helpers\Url::base(true)?>/fonts/3501B7_1B_0.woff') format('woff'),url('<?=\yii\helpers\Url::base(true)?>/fonts/3501B7_1B_0.ttf') format('truetype');}

        @font-face {font-family: 'TTLakesCondensed-Medium';src: url('<?=\yii\helpers\Url::base(true)?>/fonts/fonts/3501B7_25_0.eot');src: url('<?=\yii\helpers\Url::base(true)?>/fonts/3501B7_25_0.eot?#iefix') format('embedded-opentype'),url('<?=\yii\helpers\Url::base(true)?>/fonts/3501B7_25_0.woff2') format('woff2'),url('<?=\yii\helpers\Url::base(true)?>/fonts/3501B7_25_0.woff') format('woff'),url('<?=\yii\helpers\Url::base(true)?>/fonts/3501B7_25_0.ttf') format('truetype');}

        * {
            box-sizing: border-box;
        }

        body {
            background: #510c76;
            background: radial-gradient(ellipse at center, #510c76 0%, #1c0329 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="#0c1019", endColorstr="#0b0b0e", GradientType=1);
            font-family: 'TTLakesCondensed-Medium';
            text-align: center;
            margin: 0;
            padding: 0;
            overflow: hidden;
            max-width: 100%;
            height: 100vh;
        }

        body .container_inner {
            width: 300px;
            margin: 0 auto;
        }

        body .container_inner__login {
            height: 100vh;
        }

        body .container_inner__login .tip {
            color: #81aece;
            opacity: 0;
            transition: all 0.4s;
            font-size: 10px;
            position: relative;
            font-weight: 100;
            height: 0px;
            overflow: hidden;
            top: -27px;
            line-height: 14px;
        }

        body .container_inner__login .tick {
            -webkit-transform: scale(0) translateY(-50%);
            transform: scale(0) translateY(-50%);
            transition: all 0.35s cubic-bezier(0.65, 1.88, 0.51, 0.69);
            position: absolute;
            left: 0;
            top: 50%;
            right: 0;
            margin: auto;
        }

        body .container_inner__login .tick img {
            width: 50px;
        }

        body .container_inner__login .hide {
            opacity: 0 !important;
        }

        body .container_inner__login .bulge {
            -webkit-transform: scale(1) !important;
            transform: scale(1) !important;
            top: 50px !important;
            transition: all 0.4s;
            -webkit-animation: bulge 1s 0.25s forwards !important;
            animation: bulge 1s 0.25s forwards !important;
        }

        body .container_inner__login .login_check {
            font-size: 11px;
            text-align: center;
            line-height: 20px;
            color: white;
            color: #bfbfce;
            position: absolute;
            display: none;
            left: -26px;
            top: 160px;
            height: 280px;
            width: 278px;
            margin: auto;
            right: 0;
        }

        body .container_inner__login .login_check img {
            opacity: 0.9;
            width: 140px;
            margin-bottom: 30px;
        }

        body .container_inner__login .login_check span {
            color: #ff133d;
            line-height: 20px;
        }

        body .container_inner__login .login {
            position: absolute;
            left: 0;
            top: 50%;
            transition: all 0.2s;
            width: 220px;
            -webkit-transform-origin: 110px -30px;
            transform-origin: 110px -30px;
            margin: auto;
            -webkit-transform: scale(1) translateY(-50%) rotatex(360deg) rotatey(360deg);
            transform: scale(1) translateY(-50%) rotatex(360deg) rotatey(360deg);
            right: 0;
        }

        body .container_inner__login .login .center {
            top: 100px !important;
        }

        body .container_inner__login .login_profile {
            margin-bottom: 40px;
            margin: 0 auto;
            position: relative;
            top: 0px;
            -webkit-transform: scale(0);
            transform: scale(0);
            -webkit-animation: logo_in 1s 0.9s forwards;
            animation: logo_in 1s 0.9s forwards;
        }

        body .container_inner__login .login_profile img {
            position: relative;
            top: 18px;
        }

        body .container_inner__login .login_profile .logo {
            z-index: 2;
        }

        body .container_inner__login .login_profile .pulse {
            width: 160px;
            position: relative;
            top: -85px;
            display: none;
            left: -42px;
            z-index: 1;
        }

        body .container_inner__login .login_desc {
            color: #bfbfce;
            font-size: 10px;
            margin-top: 20px;
            -webkit-animation: pop 0.5s 1.3s forwards;
            animation: pop 0.5s 1.3s forwards;
            position: relative;
            opacity: 0;
        }

        body .container_inner__login .login_desc h3 {
            font-family: 'TTLakesCondensed-DemiBold';
            text-transform: uppercase;
            font-size: 13px;
        }

        body .container_inner__login .login_desc span {
            display: inline-block;
            color: #eb0045;
            background: #fff;
            border-radius: 2px;
            font-size: 13px;
            margin-left: 5px;
            padding: 3px 9px;
        }

        body .container_inner__login .login_inputs {
        }

        body .container_inner__login .login_inputs form {
            margin: 0;
            padding: 0;
        }

        body .container_inner__login .login_inputs form input {
            width: 100%;
            padding: 10px;
            color: #141416;
            border: none;
            border-radius: 3px;
            text-align: center;
            -webkit-animation: pop 0.5s 1.6s forwards;
            animation: pop 0.5s 1.6s forwards;
            position: relative;
            opacity: 0;
            font-size: 13px;
            outline: none;
        }

        body .container_inner__login .login_inputs form input[type="submit"] {
            margin-top: 12px;
            cursor: pointer;
            border: 1px solid #ff133d;
            text-transform: uppercase;
            letter-spacing: 1px;
            padding: 10px 10px;
            -webkit-animation: pop 0.5s 1.9s forwards;
            animation: pop 0.5s 1.9s forwards;
            position: relative;
            opacity: 0;
            position: relative;
            font-weight: 100;
            color: white;
            font-family: 'TTLakesCondensed-DemiBold';
            background: #eb0045;
            box-shadow: 0px 2px #69091a, 0px 0px 3px rgba(2, 2, 2, 0.17), 0px 0px rgba(255, 255, 255, 0.13) inset;
            font-size: 11px;
            transition: all 0.24s;
        }

        body .container_inner__login .login_inputs{
            color: rgba(255,255,255,0.75);
            text-decoration: none;
            font-weight: 100;
            -webkit-animation: pop 0.5s 2.2s forwards;
            animation: pop 0.5s 2.2s forwards;
            position: relative;
            opacity: 0;
            font-size: 14px;
            display: inline-block;
            margin-top: 20px;
            transition: all 0.24s;
        }

        @-webkit-keyframes logo_in {
            0% {
                -webkit-transform: scale(0);
                transform: scale(0);
            }
            20% {
                -webkit-transform: scale(1.1);
                transform: scale(1.1);
            }
            40% {
                -webkit-transform: scale(0.98);
                transform: scale(0.98);
            }
            60% {
                -webkit-transform: scale(1.012);
                transform: scale(1.012);
            }
            80% {
                -webkit-transform: scale(0.995);
                transform: scale(0.995);
            }
            100% {
                -webkit-transform: scale(1);
                transform: scale(1);
            }
        }

        @keyframes logo_in {
            0% {
                -webkit-transform: scale(0);
                transform: scale(0);
            }
            20% {
                -webkit-transform: scale(1.1);
                transform: scale(1.1);
            }
            40% {
                -webkit-transform: scale(0.98);
                transform: scale(0.98);
            }
            60% {
                -webkit-transform: scale(1.012);
                transform: scale(1.012);
            }
            80% {
                -webkit-transform: scale(0.995);
                transform: scale(0.995);
            }
            100% {
                -webkit-transform: scale(1);
                transform: scale(1);
            }
        }

        @-webkit-keyframes pop {
            0% {
                opacity: 0;
                top: 4px;
            }
            100% {
                opacity: 1;
                top: 0px;
            }
        }

        @keyframes pop {
            0% {
                opacity: 0;
                top: 4px;
            }
            100% {
                opacity: 1;
                top: 0px;
            }
        }

        @-webkit-keyframes fade {
            0% {
                opacity: 0;
            }
            20% {
                opacity: 1;
            }
            50% {
                opacity: 0;
            }
            100% {
                opacity: 0;
            }
        }

    </style>
    <script>
        $('input[type="tel"]').focus(function() {
            $('.tip').css('height', '30px')
            setTimeout(function() {
                $('.tip').css('opacity', '1')
            }, 350)
        });

        $('input[type="tel"]').blur(function() {
            $('.tip').css('opacity', '0')
            setTimeout(function() {
                $('.tip').css('height', '0px')
            }, 350)
        });
    </script>
</head>

<body>
<?php $this->beginBody() ?>
<div class='container'>
    <div class='container_inner'>
       <?=$content;?>
    </div>
</div>
<?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>