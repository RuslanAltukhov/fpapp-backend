<?

use app\assets\AppAsset;
use yii\bootstrap\Nav;
use yii\helpers\Html;
use yii\helpers\Url;

$organization = $this->params['organization'];
$subscriptionName = $this->params['subscriptionName'];
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="ru" class="menu-active">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>


</head>
<body>
<?php $this->beginBody() ?>
<div class="main-menu">
    <header class="header">
        <a href="/" class="logo">comeback+</a>
        <button type="button" class="button-close fa fa-times js__menu_close"></button>
        <div class="user">

            <a href="#" class="avatar"><img src="<?= ('/' . $organization->logo_path ?? "http://placehold.it/80x80") ?>"
                                            alt=""></a>
            <h5 class="name"><a href="/organization/profile"><?= $organization->name ?></a></h5>
            <h5 class="position"><a href="/organization/finances/plan">план - <strong><?= $subscriptionName ?></strong></a>
            </h5>

            <!-- /.name -->
            <div class="control-wrap js__drop_down">
                <i class="fa fa-caret-down js__drop_down_button"></i>
                <div class="control-list">
                    <div class="control-item"><a href="/organization/profile"><i class="fa fa-user"></i> Профиль</a>
                    </div>
                    <div class="control-item"><a href="/organization/start"><i class="fa fa-magic"></i> Быстрый
                            старт</a>
                    </div>
                    <div class="control-item"><a href="/site/logout"><i class="fa fa-sign-out"></i> Выйти</a></div>
                </div>
                <!-- /.control-list -->
            </div>
            <!-- /.control-wrap -->
        </div>
        <!-- /.user -->
    </header>
    <!-- /.header -->
    <div class="content">
        <div class="navigation">
            <!--<h5 class="title">Информация</h5>-->
            <?
            echo Nav::widget([
                'options' => ['class' => 'menu js__accordion'],

                'encodeLabels' => false,
                'items' => [
                    [
                        'label' => '<i class="menu-icon fa fa-dashboard"></i><span>Моя компания</span>',
                        'url' => ['/organization/organization/index'],
                        'linkOptions' => [],
                    ],
                    [
                        'label' => '<i class="menu-icon fa fa-shopping-basket"></i><span>Торговые точки</span>',
                        'url' => ['organization/salepoints/index'],
                        'linkOptions' => [],
                    ],
                    [
                        'label' => '<i class="menu-icon fa fa-street-view"></i><span>Кассиры</span>',
                        'url' => ['users/cashiers/index'],
                        'linkOptions' => [],
                    ],
                    [
                        'label' => '<i class="menu-icon fa fa-users"></i><span>Пользователи</span>',
                        'url' => ['users/users/index'],
                        'linkOptions' => [],
                    ],
                    [
                        'label' => '<i class="menu-icon fa fa-gift"></i><span>Акции</span>',
                        'url' => ['actions/actions/index'],
                        'linkOptions' => [],
                    ],
                    [
                        'label' => '<i class="menu-icon fa fa-history"></i><span>Историия</span>',
                        'url' => ['/organization/organization/history'],
                        'linkOptions' => [],
                    ]
                ],
            ]);
            ?>
        </div>
        <!-- /.navigation -->
    </div>
    <!-- /.content -->
</div>
<!-- /.main-menu -->


<div class="fixed-navbar">
    <div class="pull-left">
        <button type="button" class="menu-mobile-button glyphicon glyphicon-menu-hamburger js__menu_mobile"></button>
        <h1 class="page-title"><?= Html::encode($this->title) ?></h1>
        <!-- /.page-title -->
    </div>
    <!-- /.pull-left -->
    <div class="pull-right">
        <?php if (isset($this->blocks['action_btn'])): ?>
            <?= $this->blocks['action_btn'] ?>
        <?php endif; ?>

        <div class="ico-item toggle-hover js__drop_down ">
            <span class="fa fa-th js__drop_down_button"></span>
            <div class="toggle-content">
                <ul>
                    <li><a href="https://t.me/comebackplus" target="_blank"><i class="fa fa-paper-plane"></i><span
                                    class="txt">Telegram</span></a></li>
                    <li><a href="https://vk.com/comeback.plus" target="_blank"><i class="fa fa-vk"></i><span
                                    class="txt">Вконтакте</span></a></li>
                    <li><a href="https://www.instagram.com/comeback.plus/" target="_blank"><i
                                    class="fa fa-instagram"></i><span class="txt">Instagram</span></a></li>
                </ul>
                <span class="read-more">Подпишитесь на наши каналы</span>
            </div>
            <!-- /.toggle-content -->
        </div>
        <!-- /.ico-item -->
        <a href="/organization/history" class="ico-item pulse"><span
                    class="ico-item fa fa-bell notice-alarm js__toggle_open"
                    data-target="#notification-popup"></span></a>
    </div>
    <!-- /.pull-right -->
</div>
<!-- /.fixed-navbar -->

<!-- /#notification-popup -->

<div id="wrapper">
    <div class="main-content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box-content">
                    <?= $content ?>
                </div>
                <!-- /.box-content -->
            </div>
        </div>
    </div>
    <!-- /.main-content -->
</div><!--/#wrapper -->

<footer class="footer">
    <ul class="list-inline">
        <li>2019 © comeback+</li>
        <li><a href="#">Соглашение о Конфиденциальности</a></li>
        <li><a href="#">Правила пользования сервисом</a></li>
        <li><a href="#">Помощь</a></li>
    </ul>
</footer>
<?php if (isset($this->blocks['modal'])): ?>
    <?= $this->blocks['modal'] ?>
<?php endif; ?>
<?php $this->endBody() ?>
</body>

</html>
<? $this->endPage(); ?>
