<?

use yii\bootstrap\ActiveForm;

$form = ActiveForm::begin([
    'options' => [
        'tag' => false,
        'class' => 'frm-single'
    ],
    'fieldConfig' => [
        'template' => '{input}{error}'
    ]
]);
?>

    <div class="inside auth-form">
        <div class="title"><img src="/img/logo.svg"></div>
        <!-- /.title -->
        <div class="frm-title">Восстановление пароля</div>
        <!-- /.frm-title -->
        <div class="frm-input">
            <!-- <input type="text" placeholder="Логин" >-->
            <?= $form->field($model, 'username')->textInput(['class'=>'frm-inp','placeholder'=>'Логин']); ?>
            <i class="fa fa-user frm-ico"></i>
        </div>

        <!-- /.clearfix -->
        <button type="submit" class="frm-submit">Восстановить <i class="fa fa-arrow-circle-right"></i></button>

        <!-- /.row -->
        <div class="text-center">
        <a href="/site/login" class="a-link t"><i class="fa fa-key"></i>
            Войти.</a>
        </div>
        <!-- /.footer -->
    </div>
    <!-- .inside -->
<?
ActiveForm::end();
?>