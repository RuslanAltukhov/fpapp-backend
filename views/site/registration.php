<?

use yii\bootstrap\ActiveForm;

$form = ActiveForm::begin([
    'options' => [
        'tag' => false,
        'class' => 'frm-single'
    ],
    'fieldConfig' => [
        'template' => '{input}{error}'
    ]
]);
?>

    <div class="inside auth-form">
        <div class="title"><img src="/img/logo.svg"></div>
        <!-- /.title -->
        <div class="frm-title">Регистрация кампании</div>
        <? if (Yii::$app->session->hasFlash("success")) { ?>
            <div class="alert alert-success"><?= Yii::$app->session->getFlash("success") ?></div>
        <? } ?>
        <? if (Yii::$app->session->hasFlash("error")) { ?>
            <div class="alert alert-danger"><?= Yii::$app->session->getFlash("error") ?></div>
        <? } ?>
        <!-- /.frm-title -->
        <div class="frm-input">
            <!-- <input type="text" placeholder="Логин" >-->
            <?= $form->field($model, 'companyName')->textInput(['class' => 'frm-inp', 'placeholder' => 'Название кампании']); ?>
            <?= $form->field($model, 'phone')->textInput(['class' => 'frm-inp', 'placeholder' => 'Ваш телефон']); ?>

        </div>

        <!-- /.clearfix -->
        <button type="submit" class="frm-submit">Зарегстрироваться <i class="fa fa-arrow-circle-right"></i></button>

        <!-- /.row -->
        <div class="text-center">
            <a href="/site/login" class="a-link t"><i class="fa fa-key"></i>
                Войти.</a>
        </div>
        <!-- /.footer -->
    </div>
    <!-- .inside -->
<?
ActiveForm::end();
?>