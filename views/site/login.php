<?

use yii\bootstrap\ActiveForm;

$form = ActiveForm::begin([
    'options' => [
        'tag' => false,
        'class' => 'frm-single'
    ],
    'fieldConfig' => [
        'template' => '{input}{error}'
    ]
]);
?>

    <div class="inside auth-form">
        <div class="title"><img src="/img/logo.svg"></div>
        <!-- /.title -->
        <div class="frm-title">авторизация</div>
        <?if(Yii::$app->session->hasFlash("success")) {?>
            <div class="alert alert-success" ><?=Yii::$app->session->getFlash("success")?></div>
        <?}?>
        <!-- /.frm-title -->
        <div class="frm-input">
            <!-- <input type="text" placeholder="Логин" >-->
            <?= $form->field($model, 'username')->textInput(['class'=>'frm-inp','placeholder'=>'Логин']); ?>
            <i class="fa fa-user frm-ico"></i>
        </div>
        <!-- /.frm-input -->
        <div class="frm-input"><?= $form->field($model, 'password')->passwordInput(['class'=>'frm-inp','placeholder'=>'Пароль']); ?><i
                    class="fa fa-lock frm-ico"></i></div>
        <!-- /.frm-input -->
        <div class="clearfix margin-bottom-20">
            <div class="pull-left">
                <div class="checkbox warning"><input type="checkbox" id="rememberme"><label for="rememberme">Запомнить
                        меня</label></div>
                <!-- /.checkbox -->
            </div>
            <!-- /.pull-left -->
            <div class="pull-right"><a href="/site/restore" class="a-link"><i class="fa fa-unlock-alt"></i>Забыли
                    пароль?</a></div>
            <!-- /.pull-right -->
        </div>
        <!-- /.clearfix -->
        <button type="submit" class="frm-submit">Войти <i class="fa fa-arrow-circle-right"></i></button>

        <!-- /.row -->
        <div class="text-center">
        <a href="https://partner.cbp.su/site/registration" target="_blank" class="a-link t"><i class="fa fa-key"></i>
            Регистрация.</a>
        </div>
        <!--<div class="text-center">
        <a href="/site/registration" class="a-link t"><i class="fa fa-key"></i>
            Регистрация.</a>
        </div>-->
        <!-- /.footer -->
    </div>
    <!-- .inside -->
<?
ActiveForm::end();
?>