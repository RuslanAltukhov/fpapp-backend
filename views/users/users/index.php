<?

use app\models\params\ActionParams;
use yii\grid\GridView;
use yii\helpers\Html;

?>
<h1>Участники акций</h1>
<? $this->beginBlock('action_btn'); ?>
<button type="button" class="btn btn-success margin-bottom-10" data-toggle="modal" data-target="#create-client">Добавить
    покупателя
</button>
<? $this->endBlock(); ?>
<? $this->beginBlock('modal'); ?>
<div class="modal fade" id="create-client" tabindex="-1" role="dialog" aria-labelledby="create-kassir">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel-1"><i class="fa fa-user-plus"></i> Добавить нового покупателя
                </h4>
            </div>
            <div class="modal-body">
                <h3 class="text-info">Новый покупатель добавляется автоматически!</h3>
                <p class="lead">Как только кассир отсканирует QR-код покупателя, он автоматически попадает в вашу
                    базу.</p>
                <p class="lead">Если после установки приложения, покупатель совершает первую покупку* у вас, то он
                    становится вашим рефералом.</p>
                <p class="lead">Статус <span class="label label-warning">РЕФЕРАЛ</span> у клиента дает вам право
                    включать этого пользователя в свои периодические рассылки абсолютно бесплатно!</p>
                <p class="lead">Пользователи, привлеченные другими компаниями, попадают в вашу базу со статусом <span
                            class="label label-success">КЛИЕНТ</span> сразу после совершениия покупки* в вашей торговой
                    точке.</p>
                <hr/>
                <blockquote>
                    <p>Под покупкой мы понимаем сканирование кассиром штрих-кода клиента.</p>
                </blockquote>
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-info btn-sm" data-dismiss="modal">Понятно</button>
            </div>
        </div>
    </div>
</div>
<? $this->endBlock(); ?>
<div class="row">
    <!--<div class="column column-75">
        С выбранными:<br> <a href="">Отправить в архив</a>
    </div>-->
    <?= $this->render("_filter", ['searchModel' => $searchModel]); ?>
</div>
<div class="row">
    <?= GridView::widget([
        'tableOptions' => [
            //     'tag' => false,
            'class' => 'table table-striped table-bordered display customers dataTable',
            'style' => 'width:100%'
        ],
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            [
                'header' => 'Имя',
                'format' => 'raw',
                'value' => function ($model) use ($organization) {

                    return HTML::a($model->name ?? "Не указано", '/users/view/' . $model->id);
                }
            ],
            [
                'header' => 'Статус',
                'format' => 'raw',
                'value' => function ($model) use ($organization) {
                    if ($model->referer_organization == $organization->id) {
                        return '<span class="label label-warning">РЕФЕРАЛ</span>';
                    }
                    return '<span class="label label-success">Клиент</span>';
                }
            ],
            [
                'header' => 'Акции',
                'format' => 'raw',
                'value' => function ($model) use ($organization) {
                    $actions = $model->getActions()->where(['organization_id' => $organization->id])->asArray()->all();

                    return implode(", &nbsp;&nbsp", array_map(function ($item) {
                        return Html::a($item['name'], '/actions/view/' . $item['id']);
                    }, $actions));
                }
            ],
            [
                'header' => 'Плюсы',
                'format' => 'raw',
                'value' => function ($model) use ($organization) {
                    return $model->getBoughtsCount($organization);
                }
            ],
            [
                'header' => 'Бонусы',
                'format' => 'raw',
                'value' => function ($model) use ($organization) {
                    return $model->getGiftsCount($organization);
                }
            ],
            [
                'header' => 'Последняя покупка',
                'format' => 'raw',
                'value' => function ($model) use ($organization) {
                    return $model->lastBoughtDate;
                }
            ],
            /*'item_description',
            [
                'attribute' => 'status_id',
                'format' => 'raw',
                'value' => function ($model) {
                    $statusClass = [ActionParams::STATUS_NEW => "new", ActionParams::STATUS_ACTIVE => 'active', ActionParams::STATUS_CLOSED => 'archived', ActionParams::STATUS_REJECTED => "rejected"];
                    return sprintf('<span class="bage %s">%s</span>',
                        $statusClass[$model->status_id] ?? '',
                        ActionParams::getStatusNameById($model->status_id));
                }
            ],*/
            /* [
                 'header' => 'Действия',
                 'class' => \yii\grid\ActionColumn::class,
                 'buttons' => [
                     'update' => function ($url, $model) {
                         return sprintf('<a class="bage active" href="/actions/%d/update" ><i class="typcn icon-default typcn-pencil"></i></a>', $model->id);
                     },
                     'delete' => function ($url, $model) {
                         return Html::a('<i class="typcn icon-default typcn-trash"></i>', "/actions/{$model->id}/move-to-archive", [
                             'data-method' => 'post',
                             'class' => 'bage archive'
                         ]);

                     }
                 ],
                 'template' => '{update} {delete}',
             ]*/
            // ...
        ],
    ]) ?>
</div>
