<div class="row">
    <div class="col-md-4 margin-bottom-20">
        <div class="box-content card">
            <h4 class="box-title">Профиль пользователя</h4>
            <div class="card-content">
                <table class="table table-hover no-margin">
                    <tbody>
                    <tr>
                        <td>ID</td>
                        <td>#<?= $user->id ?></td>
                    </tr>
                    <tr>
                        <td>Имя</td>
                        <td><?= $user->name ?></td>
                    </tr>
                    <!--<tr>
                        <td>Город</td>
                        <td>Казань</td>
                    </tr>-->
                    <tr>
                        <td>Статус</td>
                        <td><?
                            if ($user->referer_organization == $organization->id) {
                                echo '<span class="label label-warning">РЕФЕРАЛ</span>';
                            } else {
                                echo '<span class="label label-success">Клиент</span>';
                            }
                            ?>
                        </td>
                    </tr>
                    <!-- <tr>
                         <td>Уведомления</td>
                         <td><h5 class="text-danger">выкл.</h5></td>
                     </tr>-->
                    <tr>
                        <td>Первая покупка</td>

                        <td>
                            <? if ($user->firstBought) { ?>
                                <?= $user->firstBought->created_at ?> ТТ <a
                                        href="/sale-points/update/<?= $user->firstBought->salePoint->id ?>"><?= $user->firstBought->salePoint->name ?></a>. Кассир: <a
                                        href="/cashiers/update/<?= $user->firstBought->cashier->id ?>"><?= $user->firstBought->cashier->name ?></a>
                            <? } else {
                                echo "Покупок еще не было";
                            }
                            ?>

                        </td>
                    </tr>
                    <!-- <tr>
                         <td>Торговые Точки</td>
                         <td><h5><a href="tochka.html">Лакомка</a></h5> <h5><a href="tochka.html">Ларёк вокзал</a></h5>
                         </td>
                     </tr>-->
                    <tr>
                        <td>Акции</td>
                        <td>
                            <? foreach ($user->actions as $action) { ?>
                                <h5><a href="/actions/view/<?=$action->id?>"><?= $action->name ?></a></h5>
                            <? } ?>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-4 margin-bottom-20">
        <div class="box-content card">
            <h4 class="box-title">Статистика покупок</h4>
            <div class="card-content">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Акция</th>
                        <th class="text-center">Плюсы</th>
                        <th class="text-center">Бонусы</th>
                    </tr>
                    </thead>
                    <tbody>
                    <? foreach ($userStatistic->getSalePoints() as $salePoint) { ?>
                        <tr>
                            <th colspan="3" class="text-center"><a
                                        href="/sale-points/update/<?= $salePoint->id ?>"><?= $salePoint->name ?></a>
                            </th>
                        </tr>
                        <? foreach ($userStatistic->getActionsInSalePoint($salePoint) as $action) {
                            $stat = $userStatistic->getBuyStatisticForAction($action);
                            ?>
                            <tr>
                                <td><a href="/actions/view/<?= $action->id ?>"><?= $action->name ?></a></td>
                                <td class="text-center"><?= $stat['boughts'] ?></td>
                                <td class="text-center"><?= $stat['gifts'] ?></td>
                            </tr>
                        <? } ?>
                    <? } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-4 margin-bottom-20">
        <div class="box-content card">
            <h4 class="box-title">Лог активностей</h4>
            <div class="card-content" style="max-height: 80vh; overflow-y: auto">
                <ul class="notice-list">
                    <? foreach ($userStatistic->getSalesStatistic() as $item) { ?>
                        <li>
                            <span class="icon bg-success"><i class="fa fa-plus-circle"></i></span>
                            <span class="title">Выдан <?= $item->type == \app\models\ActionSale::TYPE_SALE ? "плюс" : "бонус" ?>
                                : <?= $item->count ?>шт.</span>
                            <span class="desc">Кассир <a

                                        href="/cashiers/update/<?=$item->cashier->id?>"><?= $item->cashier->name; ?></a> выдал <?= $item->type == \app\models\ActionSale::TYPE_SALE ? "фишки" : "плюшки" ?>

                                по акции <a
                                        href="/actions/view/<?= $item->action->id ?>">"<?= $item->action->name ?>"</a> клиенту <a
                                        href="/users/view/<?= $item->user->id ?>"><?= $item->user->name ?></a></span>
                            <span class="time"><?= date("d.m.Y в H:i", strtotime($item->created_at)); ?></span>
                        </li>
                    <? } ?>
                </ul>
            </div>
        </div>
    </div>
</div>