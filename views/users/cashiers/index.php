<?

use app\models\Cashier;
use app\models\params\ActionParams;
use app\models\params\CashierParams;
use yii\grid\GridView;
use yii\helpers\Html;

?>
<h1>Мои кассиры</h1>
<? $this->beginBlock('action_btn'); ?>
<a href="/cashiers/create" class="btn btn-success margin-bottom-10 "><i
            class="typcn typcn-plus-outline"></i>Добавить кассира</a>
<? $this->endBlock(); ?>
<div class="row">


    <!--<div class="column column-75">
        С выбранными:<br> <a href="">Отправить в архив</a>
    </div>-->
    <!--  --><? /*= $this->render("_filter", ['searchModel' => $searchModel]); */ ?>
</div>
<div class="row">

    <?= GridView::widget([
        'tableOptions' => [
            //     'tag' => false,
            'class' => 'table table-striped table-bordered display customers dataTable',
            'style' => 'width:100%'
        ],
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'created_at',
            [
                'attribute' => 'Имя',
                'format' => 'raw',
                'value' => function (Cashier $cashier) {
                    return HTML::a($cashier->name, "/cashiers/update/{$cashier->id}");
                }
            ],
            [
                'header' => 'Телефон',
                'attribute' => 'user.phone',

            ],
            [
                'header' => 'Торговая точка',
                'format' => 'raw',
                'value' => function (Cashier $model) {
                    $salePoints = $model->getSalePoints()->asArray()->all();

                    return implode(", &nbsp;&nbsp;", array_map(function ($item) {
                        return Html::a($item['name'], '/sale-points/update/'.$item['id']);
                    }, $salePoints));
                }
            ],
            [
                'header' => 'Статус',

                'format' => 'raw',
                'value' => function ($model) {
                    $statusClass = [CashierParams::STATUS_ACTIVE => "active", CashierParams::STATUS_ARCHIVED => 'archived'];
                    return sprintf('<span class="bage %s">%s</span>',
                        $statusClass[$model->is_archived] ?? '',
                        CashierParams::getStatusNameById($model->is_archived));
                }
            ],
            [
                'header' => 'Выдано плюсов',
                'format' => 'raw',
                'value' => function (Cashier $cashier) {
                    return $cashier->soldItemsCount;

                }
            ],
            [
                'header' => 'Выдано бонусов',
                'format' => 'raw',
                'value' => function (Cashier $cashier) {
                    return $cashier->giftsCount;

                }
            ],
            [
                'header' => 'Последняя продажа',
                'format' => 'raw',
                'value' => function (Cashier $cashier) {
                    return $cashier->lastSaleDate;
                }
            ]
            /*'item_description',
            [
                'attribute' => 'status_id',
                'format' => 'raw',
                'value' => function ($model) {
                    $statusClass = [ActionParams::STATUS_NEW => "new", ActionParams::STATUS_ACTIVE => 'active', ActionParams::STATUS_CLOSED => 'archived', ActionParams::STATUS_REJECTED => "rejected"];
                    return sprintf('<span class="bage %s">%s</span>',
                        $statusClass[$model->status_id] ?? '',
                        ActionParams::getStatusNameById($model->status_id));
                }
            ],*/
            /* [
                 'header' => 'Действия',
                 'class' => \yii\grid\ActionColumn::class,
                 'buttons' => [
                     'update' => function ($url, $model) {
                         return sprintf('<a class="bage active" href="/actions/%d/update" ><i class="typcn icon-default typcn-pencil"></i></a>', $model->id);
                     },
                     'delete' => function ($url, $model) {
                         return Html::a('<i class="typcn icon-default typcn-trash"></i>', "/actions/{$model->id}/move-to-archive", [
                             'data-method' => 'post',
                             'class' => 'bage archive'
                         ]);

                     }
                 ],
                 'template' => '{update} {delete}',
             ]*/
            // ...
        ],
    ]) ?>
</div>
