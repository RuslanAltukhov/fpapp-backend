<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 16/06/2019
 * Time: 15:01
 */

use yii\widgets\ActiveForm;

?>

<?
$form = ActiveForm::begin();
echo $form->field($model, 'name')->textInput();
echo $form->field($model, 'phone')->textInput();
echo $form->field($model, 'salePointIds')->dropDownList(\yii\helpers\ArrayHelper::map($salePoints, 'id', 'name'),['size' => 1, 'class' => 'select2_1', 'multiple' => 'multiple']);
?>
<div class="modal-footer text-center">
    <button type="submit" class="btn btn-success btn-sm">Сохранить</button>
</div>
<? ActiveForm::end(); ?>
