<? use yii\bootstrap\ActiveForm; ?>

<div class="pricing-plan">
    <? if ($error) { ?>
        <div class="alert alert-danger"><?= $error; ?></div>
    <? } ?>
    <div class="pricing-table">
        <div class="col col-first">
            <div class="thead">
                <div class="center-v">Выберите свой план</div>
            </div>
            <div class="td">Персональный дизайн</div>
            <div class="td">Автооповещения</div>
            <div class="td">Автоприглашения</div>
            <div class="td">Кассиров</div>
            <div class="td">Акций</div>
            <div class="td">Торговых точек</div>
            <div class="td"></div>
        </div>
        <div class="col">
            <div class="thead bg-blue-1">
                <h4>План "Старт"</h4>
                <div class="price">
                    <span class="number">0</span>
                    <span class="small_number">руб</span>
                </div>
            </div>
            <div class="td"><i class="fa fa-times"></i></div>
            <div class="td"><i class="fa fa-times"></i></div>
            <div class="td"><i class="fa fa-times"></i></div>
            <div class="td">1</div>
            <div class="td">1</div>
            <div class="td">1</div>
            <div class="td">по умолчанию</div>
        </div>

        <div class="col col-featured">
            <?
            $businessPlan = ActiveForm::begin([

                'fieldConfig' => [
                    'template' => '{input}',
                ]
            ]); ?>
            <?= $businessPlan->field($model, 'subscriptionType')->hiddenInput(['value' => \app\models\Subscription::SUBSCRIPTION_TYPE_BUSINESS]) ?>
            <?= $businessPlan->field($model, 'period')->hiddenInput(['value' => \app\classes\OrganizationSubscription::SUBSCRIPTION_TYPE_MONTH]) ?>
            <div class="thead bg-main-2">
                <h4>План "Бизнес"</h4>
                <div class="price">
                    <span class="number">2000</span>
                    <span class="small_number">руб в месяц</span>
                </div>
            </div>
            <div class="td"><i class="fa fa-check"></i></div>
            <div class="td"><i class="fa fa-check"></i></div>
            <div class="td"><i class="fa fa-check"></i></div>
            <div class="td">не ограничено</div>
            <div class="td">до 5</div>
            <div class="td">
                <div class="row">
                    <div class="col-xs-6">Количество</div>
                    <div class="col-xs-4">
                        <?= $businessPlan->field($model, 'salePointsCount')->textInput() ?>
                    </div>
                </div>
            </div>
            <div class="td"><input type="submit" class="btn-order js__popup_open" value="Активировать">
            </div>
            <? ActiveForm::end(); ?>
        </div>

        <div class="col">
            <?
            $businessPlanYear = ActiveForm::begin([

                'fieldConfig' => [
                    'template' => '{input}',
                ]
            ]); ?>
            <?= $businessPlanYear->field($model, 'subscriptionType')->hiddenInput(['value' => \app\models\Subscription::SUBSCRIPTION_TYPE_BUSINESS]) ?>
            <?= $businessPlanYear->field($model, 'period')->hiddenInput(['value' => \app\classes\OrganizationSubscription::SUBSCRIPTION_TYPE_YEAR]) ?>
            <div class="thead bg-blue-1">
                <h4>Годовой план "Бизнес"</h4>
                <div class="price">
                    <span class="number">18 000</span>
                    <span class="small_number">руб в год</span>
                </div>
            </div>
            <div class="td"><i class="fa fa-check"></i></div>
            <div class="td"><i class="fa fa-check"></i></div>
            <div class="td"><i class="fa fa-check"></i></div>
            <div class="td">не ограничено</div>
            <div class="td">до 5</div>
            <div class="td">
                <div class="row">
                    <div class="col-xs-6">Количество</div>
                    <div class="col-xs-4">
                        <?= $businessPlanYear->field($model, 'salePointsCount')->textInput() ?>
                    </div>
                </div>
            </div>
            <div class="td"><input type="submit" class="btn-order js__popup_open" value="Активировать">
            </div>
            <? ActiveForm::end(); ?>
        </div>
    </div>
</div>