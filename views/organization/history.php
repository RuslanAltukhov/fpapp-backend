<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 30/06/2019
 * Time: 17:53
 */
?>

    <h1>Оповещения</h1>
<div class="row">
<div class="col-lg-6">
    <ul class="notice-list">
    <?

    foreach ($history as $historyItem) {
        ?>
        <li>
                            <span class="icon bg-<?= $historyItem->type == \app\models\ActionSale::TYPE_SALE ? "success" : "violet" ?>"><i
                                        class="fa fa-<?= $historyItem->type == \app\models\ActionSale::TYPE_SALE ? "plus" : "check" ?>-circle"></i></span>
            <span class="title">Выдан <?= $historyItem->type == \app\models\ActionSale::TYPE_SALE ? "плюс" : "бонус" ?>
                : <? /*= $historyItem->count */ ?>шт.</span> 
            <span class="desc">Кассир <a href="kassir.html"><?= $historyItem->cashier->name ?></a> выдал 1 Плюс по акции <a
                        href="offer.html">"<?= $historyItem->action->name ?>"</a> клиенту <a
                        href="client.html"><?= $historyItem->user->name ?></a></span>
            <span class="time"><?= date("d.m.Y в H:i", strtotime($historyItem->created_at)) ?></span>
        </li>
    <? } ?>
</ul>
</div>
</div>
