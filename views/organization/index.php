<?

use yii\bootstrap\ActiveForm;

?>
<? if ($success) { ?>
    <div class="alert alert-success"><?= $success; ?></div>
<? } ?>
<div class="row">

    <div class="col-lg-3 margin-bottom-20">
        <ul class="nav nav-tabs nav-justified" id="myTabs-justified" role="tablist">
            <li role="presentation" class="active"><a href="#company-profile" id="company-tab-profile" role="tab"
                                                      data-toggle="tab" aria-controls="home" aria-expanded="true">Профиль
                    компании</a></li>
            <li role="presentation" class="dropdown">
                <a href="#" class="dropdown-toggle" id="myTabDrop1-justified" data-toggle="dropdown"
                   aria-controls="myTabDrop1-contents">Опции <span class="caret"></span></a>
                <ul class="dropdown-menu" aria-labelledby="myTabDrop1-justified" id="myTabDrop1-contents-justified">
                    <li><a href="#dropdown1-justified" role="tab" id="dropdown1-tab-justified" data-toggle="tab"
                           aria-controls="dropdown1">Редактировать</a></li>
                    <li><a href="#dropdown2-justified" role="tab" id="dropdown2-tab-justified" data-toggle="tab"
                           aria-controls="dropdown2">Удалить</a></li>
                </ul>
            </li>
        </ul>
        <!-- /.nav-tabs -->
        <div class="tab-content" id="myTabContent-justified">
            <div class="tab-pane fade active in" role="tabpanel" id="company-profile"
                 aria-labelledby="company-tab-profile">
                <div class="profile-avatar">
                    <img src="<?= $orgUpdateForm->currentLogo ?>" alt="" class="margin-bottom-20">
                </div>

                <div class="row">
                    <div class="col-xs-5"><label>Название</label></div>
                    <div class="col-xs-7">

                        <?= $orgUpdateForm->companyName ?></div>
                </div>
                <div class="row">
                    <div class="col-xs-5"><label>Баланс</label></div>
                    <div class="col-xs-7">

                        <?= $owner->balance ?> Cb</div>
                </div>
                <div class="row">
                    <div class="col-xs-5"><label>Контактное лицо</label></div>
                    <div class="col-xs-7"><?= $orgUpdateForm->contactPerson ?></div>
                </div>
                <div class="row">
                    <div class="col-xs-5"><label>Должность</label></div>
                    <div class="col-xs-7"><?= $orgUpdateForm->contactPersonEmployment ?></div>
                </div>
                <div class="row">
                    <div class="col-xs-5"><label>Телефон</label></div>
                    <div class="col-xs-7"><?= $orgUpdateForm->contactPersonPhone ?></div>
                </div>
                <div class="row">
                    <div class="col-xs-5"><label>Почта</label></div>
                    <div class="col-xs-7"><?= $orgUpdateForm->companyContactEmail ?></div>
                </div>
            </div>
            <div class="tab-pane fade" role="tabpanel" id="dropdown1-justified"
                 aria-labelledby="dropdown1-tab-justified">
                <? $form = ActiveForm::begin(); ?>

                <?= $form->field($orgUpdateForm, 'companyName')->textInput(); ?>
                <?= $form->field($orgUpdateForm, 'contactPerson')->textInput(['placeholder' => 'ФИО контактного лица']); ?>
                <?= $form->field($orgUpdateForm, 'contactPersonEmployment')->textInput(['placeholder' => 'Должность контактного лица']); ?>
                <?= $form->field($orgUpdateForm, 'contactPersonPhone')->textInput(['placeholder' => 'Телефон контактного лица']); ?>
                <?= $form->field($orgUpdateForm, 'companyContactEmail')->textInput(['placeholder' => 'Почта контактного лица']); ?>
                <?= $form->field($orgUpdateForm, 'logo')->fileInput(); ?>

                <button type="button" class="btn btn-bordered btn-sm">Отмена</button>
                <button type="submit" class="btn btn-primary btn-sm">Сохранить</button>
                <? Activeform::end(); ?>
            </div>
            <div class="tab-pane fade" role="tabpanel" id="dropdown2-justified"
                 aria-labelledby="dropdown2-tab-justified">
                <p class="lead">Вы можете отправть запрос на удаление вашей компании и всех, связанных с ней акционных
                    предложений</p>
                <form>
                    <div class="margin-bottom-20">
                        <h5><b>Причина удаления</b></h5>
                        <textarea id="textarea" class="form-control" maxlength="225" rows="2"
                                  placeholder="Пожалуйста, сообщите нам почему вы решилии отказаться от нашего сервиса"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary btn-sm">Отправить запрос</button>
                </form>
            </div>
        </div>
        <!-- /.tab-content -->
        <div class="table-responsive box-content">
            <h4 class="box-title">Пополнение баланса</h4>
            <div class="dropdown js__drop_down">
                <h4 class="box-title"><strong class="text-danger">1 Cb+ = 1 руб.</strong></h4>
            </div>
            <? $balanceForm = ActiveForm::begin([
                'fieldConfig' => [
                    'template' => '{error}{input}',
                    'labelOptions' => ['class' => 'col-sm-3 control-label']
                ]
            ]); ?>
            <div class="input-group margin-bottom-20">

                <!-- <input type="number" class="form-control" placeholder="Введиите сумму">-->
                <?= $balanceForm->field($balanceFormModel, 'sum')->textInput(['placeholder' => 'Введите сумму пополнения']) ?>
                <div class="input-group-btn">
                    <button type="submit" class="btn btn-success no-border"><i class="fa fa-paper-plane text-white"></i>
                    </button>
                </div>
            </div>
            <? ActiveForm::end(); ?>
        </div>
    </div>
    <div class="col-lg-9">
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="box-content card">
                    <h4 class="box-title">Топ акций</h4>
                    <div class="card-content">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Название</th>
                                <th>Фишки</th>
                            </tr>
                            </thead>
                            <tbody>

                            <? foreach ($topActions as $actionSale) {
                                if (!isset($actionSale->action)) {
                                    continue;
                                }
                                ?>
                                <tr>
                                    <td><a href="kassir.html"><?= $actionSale->action->name ?></a></td>
                                    <td>
                                        <?= $actionSale->count ?>
                                    </td>
                                </tr>
                            <? } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="box-content card">
                    <h4 class="box-title">Топ кассиров</h4>
                    <div class="card-content">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Имя</th>

                                <th>Фишки</th>
                            </tr>
                            </thead>
                            <tbody>
                            <? foreach ($topCashiers as $cashier) { ?>
                                <tr>
                                    <td><a href="kassir.html"><?= $cashier->name ?></a></td>
                                    <td>
                                        <?= $cashier->sales_count ?>
                                    </td>
                                </tr>
                            <? } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="box-content card">
                    <h4 class="box-title">Топ рефералов</h4>
                    <div class="card-content">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Имя</th>
                                <th>Фишки</th>
                            </tr>
                            </thead>
                            <tbody>
                            <? foreach ($topReferals as $referal) { ?>
                                <tr>
                                    <td><a href="/users/view/<?= $referal->id ?>"><?= $referal->name ?></a></td>
                                    <td>
                                        <?= $referal->bought_count ?>
                                    </td>
                                </tr>
                            <? } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="box-content card">
                    <h4 class="box-title">Топ клиентов</h4>
                    <div class="card-content">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Имя</th>

                                <th>Фишки</th>
                            </tr>
                            </thead>
                            <tbody>
                            <? foreach ($topClients as $client) { ?>
                                <tr>
                                    <td><a href="/users/view/<?= $client->id ?>"><?= $client->name ?></a></td>
                                    <td>
                                        <?= $client->bought_count ?>
                                    </td>
                                </tr>
                            <? } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>



         

