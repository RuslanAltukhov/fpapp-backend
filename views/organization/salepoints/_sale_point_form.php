<?

use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

$this->registerJsFile("/js/plugin/timepicker/bootstrap-timepicker.min.js", ['depends' => \yii\web\JqueryAsset::class, 'position' => \yii\web\View::POS_END]);
$this->registerCssFile("/js/plugin/timepicker/bootstrap-timepicker.min.css");
$this->registerJS("initUI()");

?>

    <script>

        function initUI() {
            initTimepicker();
            initWorkingHoursCheckBoxes();
            initWorkingHoursDefaultState();
        }

        function initTimepicker() {
            $(".timepicker").timepicker({
                showMeridian: false,
                defaultTime: '',
            });
        }

        function initWorkingHoursDefaultState() {
            $(".working-hour").each(function (index, el) {

                if ($(el).find("input[type=checkbox]").prop("checked")) {
                    $(el).find("input[type=text]").attr("disabled", "");
                }
            });
        }

        function initWorkingHoursCheckBoxes() {
            $(".working-hour input[type=checkbox]").change(function () {
                if ($(this).prop("checked")) {
                    $(this).closest('.form-group').find("input[type=text]").attr("disabled", "");
                    return;
                }
                $(this).closest('.form-group').find("input[type=text]").removeAttr("disabled");
            });
        }

        var map;
        var marker;
        var componentForm = {
            street_number: 'short_name',
            route: 'long_name',
            locality: 'long_name'
        };


        function fillAddress(place) {
            console.log(place);
            var address = [];
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {
                    address.push(place.address_components[i][componentForm[addressType]]);
                }
            }

            $("#address").val(address.reverse().join(","));
            $("#lat").val(place.geometry.location.lat);
            $("#lng").val(place.geometry.location.lng);
        }

        function initAutocomplete() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {
                    lat: -33.8688,
                    lng: 151.2195
                },
                zoom: 13,
                mapTypeId: 'roadmap',
                zoomControl: true,
                mapTypeControl: false,
                scaleControl: false,
                streetViewControl: false,
                rotateControl: false,
                fullscreenControl: false
            });

            // Create the search box and link it to the UI element.
            var input = document.getElementById('address');
            var searchBox = new google.maps.places.SearchBox(input);
            /*map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            // Bias the SearchBox results towards current map's viewport.
            map.addListener('bounds_changed', function () {
                searchBox.setBounds(map.getBounds());
            });*/

            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function () {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }
                place = places[0];
                setPlace(place);
            });
        }

        function setPlace(place) {
            if (marker) {
                marker.setMap(null);
            }

            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();

            if (!place.geometry) {
                console.log("Returned place contains no geometry");
                return;
            }

            fillAddress(place);
            // Create a marker for each place.
            marker = new google.maps.Marker({
                map: map,
                title: place.name,
                position: place.geometry.location
            });

            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }
            map.fitBounds(bounds);
        }
    </script>

<?

$form = ActiveForm::begin([
    'action' => $salePointCreateForm->isUpdateForm() ? '' : '/sale-points/create',
    'enableAjaxValidation' => true,
    'fieldConfig' => [
        'template' => '{label}{input}{error}',
        'labelOptions' => ['class' => 'control-label']
    ],
    'options' => [
        'class' => 'form-horizontal'
    ]
]); ?>
    <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel-1">
            <?= $salePointCreateForm->isUpdateForm() ? "Редактировать торговую точку" : "Добавить новую Торговую Точку" ?>
        </h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-7">
                <?= $form->field($salePointCreateForm, 'lat', ['template' => '{input}'])->hiddenInput(['id' => 'lat'])->label(false) ?>
                <?= $form->field($salePointCreateForm, 'lng', ['template' => '{input}'])->hiddenInput(['id' => 'lng'])->label(false) ?>
                <?= $form->field($salePointCreateForm, 'name')->textInput(); ?>

                <?= $form->field($salePointCreateForm, 'address')->textInput(['id' => 'address']) ?>
                <?= $form->field($salePointCreateForm, 'address_custom')->textInput(); ?>
                <?= $form->field($salePointCreateForm, 'cashiers')->dropDownList(ArrayHelper::map($salePointCreateForm->organization->getCashiers()->asArray()->all(), 'id', 'name'), ['size' => 1, 'class' => 'select2_1', 'multiple' => 'multiple']) ?>

                <?= $form->field($salePointCreateForm, 'actions')->dropDownList(ArrayHelper::map($salePointCreateForm->organization->getActions()->asArray()->all(), 'id', 'name'), ['size' => 1, 'class' => 'select2_1', 'multiple' => 'multiple']) ?>

                <h4>Адрес Торговой Точки</h4>
                <div class="form-group">
                    <div style="height:400px" id="map"></div>
                </div>
            </div>
            <div class="col-md-5" style="background: #eeeeee;">
                <h4>График работы</h4>
                <?
                $workingHoursFieldOptions = ['template' => '{input}{error}', 'options' => ['class' => 'bootstrap-timepicker']];
                ?>
                <div class="form-group working-hour">
                    <div class="row">
                        <div class="col-xs-6">
                            <label for="inp-type-1">Понедельник</label>
                        </div>
                        <div class="col-xs-6">


                            <?= $form->field($salePointCreateForm, 'working_time[0][every_time]', [
                                'template' => '{input}{label}',
                                'options' => [
                                    'tag' => false
                                ]

                            ])->checkbox([], false)->label("Круглосуточно"); ?>

                        </div>
                    </div>
                    <div class="input-group">
                        <?= $form->field($salePointCreateForm, 'working_time[0][open]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>
                        <span class="input-group-addon">-</span>
                        <?= $form->field($salePointCreateForm, 'working_time[0][close]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>
                    </div>
                </div>
                <div class="form-group working-hour">
                    <div class="row">
                        <div class="col-xs-6">
                            <label for="inp-type-1">Вторник</label>
                        </div>
                        <div class="col-xs-6">
                            <?= $form->field($salePointCreateForm, 'working_time[1][every_time]', [
                                'template' => '{input}{label}',
                                'options' => [
                                    'tag' => false
                                ]

                            ])->checkbox([], false)->label("Круглосуточно"); ?>
                        </div>
                    </div>

                    <div class="input-group">
                        <?= $form->field($salePointCreateForm, 'working_time[1][open]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>

                        <span class="input-group-addon">-</span>
                        <?= $form->field($salePointCreateForm, 'working_time[1][close]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>
                    </div>
                </div>
                <div class="form-group working-hour">
                    <div class="row">
                        <div class="col-xs-6">
                            <label for="inp-type-1">Среда</label>
                        </div>
                        <div class="col-xs-6">
                            <?= $form->field($salePointCreateForm, 'working_time[2][every_time]', [
                                'template' => '{input}{label}',
                                'options' => [
                                    'tag' => false
                                ]

                            ])->checkbox([], false)->label("Круглосуточно"); ?>
                        </div>
                    </div>
                    <div class="input-group">
                        <?= $form->field($salePointCreateForm, 'working_time[2][open]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>

                        <span class="input-group-addon">-</span>
                        <?= $form->field($salePointCreateForm, 'working_time[2][close]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>

                    </div>
                </div>
                <div class="form-group working-hour">
                    <div class="row">
                        <div class="col-xs-6">
                            <label for="inp-type-1">Четверг</label>
                        </div>
                        <div class="col-xs-6">
                            <?= $form->field($salePointCreateForm, 'working_time[3][every_time]', [
                                'template' => '{input}{label}',
                                'options' => [
                                    'tag' => false
                                ]

                            ])->checkbox([], false)->label("Круглосуточно"); ?>
                        </div>
                    </div>
                    <div class="input-group">
                        <?= $form->field($salePointCreateForm, 'working_time[3][open]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>
                        <span class="input-group-addon">-</span>
                        <?= $form->field($salePointCreateForm, 'working_time[3][close]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>
                    </div>
                </div>
                <div class="form-group working-hour">
                    <div class="row">
                        <div class="col-xs-6">
                            <label for="inp-type-1">Пятница</label>
                        </div>
                        <div class="col-xs-6">
                            <?= $form->field($salePointCreateForm, 'working_time[4][every_time]', [
                                'template' => '{input}{label}',
                                'options' => [
                                    'tag' => false
                                ]

                            ])->checkbox([], false)->label("Круглосуточно"); ?>
                        </div>
                    </div>
                    <div class="input-group">
                        <?= $form->field($salePointCreateForm, 'working_time[4][open]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>

                        <span class="input-group-addon">-</span>
                        <?= $form->field($salePointCreateForm, 'working_time[4][close]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>

                    </div>
                </div>
                <div class="form-group working-hour">
                    <div class="row">
                        <div class="col-xs-6">
                            <label for="inp-type-1">Суббота</label>
                        </div>
                        <div class="col-xs-6">
                            <?= $form->field($salePointCreateForm, 'working_time[5][every_time]', [
                                'template' => '{input}{label}',
                                'options' => [
                                    'tag' => false
                                ]

                            ])->checkbox([], false)->label("Круглосуточно"); ?>
                        </div>
                    </div>
                    <div class="input-group">
                        <?= $form->field($salePointCreateForm, 'working_time[5][open]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>

                        <span class="input-group-addon b-0">-</span>
                        <?= $form->field($salePointCreateForm, 'working_time[5][close]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>
                    </div>
                </div>
                <div class="form-group working-hour">
                    <div class="row">
                        <div class="col-xs-6">
                            <label for="inp-type-1">Воскресенье</label>
                        </div>
                        <div class="col-xs-6">
                            <?= $form->field($salePointCreateForm, 'working_time[6][every_time]', [
                                'template' => '{input}{label}',
                                'options' => [
                                    'tag' => false
                                ]

                            ])->checkbox([], false)->label("Круглосуточно"); ?>
                        </div>
                    </div>
                    <div class="input-group">
                        <?= $form->field($salePointCreateForm, 'working_time[6][open]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>

                        <span class="input-group-addon b-0">-</span>
                        <?= $form->field($salePointCreateForm, 'working_time[6][close]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>

                    </div>
                </div>
            </div>
        </div>


    </div>
    <div class="modal-footer text-center">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Отмена</button>
        <button type="submit" class="btn btn-success btn-sm">Сохранить</button>
    </div>
<? ActiveForm::end(); ?>