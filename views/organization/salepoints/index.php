<?

use app\models\Cashier;
use app\models\params\ActionParams;
use app\models\params\CashierParams;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = "Мои торговые точки";

$this->registerJsFile("https://maps.googleapis.com/maps/api/js?key=" . \Yii::$app->params['gmaps_key'] . "&language=ru-RU&libraries=places&callback=initAutocomplete", ['position' => \yii\web\View::POS_END]);
$this->beginBlock('modal');

?>
<div class="modal fade" id="create-tt" tabindex="-1" role="dialog" aria-labelledby="create-tt">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <?= $this->render("_sale_point_form", ['salePointCreateForm' => $salePointCreateForm]); ?>
        </div>
    </div>
</div>

<? $this->endBlock();
$this->beginBlock('action_btn');
?>
<a data-toggle="modal" data-target="#create-tt" class="btn btn-success margin-bottom-10 "><i
            class="typcn typcn-plus-outline"></i>Добавить торговую точку</a>
<? $this->endBlock(); ?>
<div class="row">

    <!--  --><? /*= $this->render("_filter", ['searchModel' => $searchModel]); */ ?>
</div>
<div class="row">
    <? if (Yii::$app->session->hasFlash("success")): ?>
        <div style="width:100%" class="alert alert-success" role="alert">
            <?= Yii::$app->session->getFlash("success") ?>
        </div>
    <? endif; ?>
</div>
<div class="row">

    <?= GridView::widget([
        'tableOptions' => [
            //     'tag' => false,
            'class' => 'table table-striped table-bordered display customers dataTable',
            'style' => 'width:100%'
        ],
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'created_at',
            [
                'attribute' => 'name',
                'format' => 'raw',
                'value' => function (\app\models\SalePoint $salePoint) {
                    return HTML::a($salePoint->name, "/sale-points/update/{$salePoint->id}");
                }
            ],
            'address',
            [
                'header' => 'Кассиры',
                'format' => 'raw',
                'value' => function (\app\models\SalePoint $salePoint) {
                    $cashiers = $salePoint->getCashiers()->asArray()->all();
                    return implode(',', array_column($cashiers, 'name'));
                }
            ],
            [
                'header' => 'Акции',
                'format' => 'raw',
                'value' => function (\app\models\SalePoint $salePoint) {
                    $cashiers = $salePoint->getActions()->asArray()->all();
                    return implode(',', array_column($cashiers, 'name'));
                }
            ],
            [
                'header' => 'Выдано плюсов',
                'format' => 'raw',
                'value' => function (\app\models\SalePoint $salePoint) {
                    return $salePoint->soldItemsCount;

                }
            ],
            [
                'header' => 'Выдано бонусов',
                'format' => 'raw',
                'value' => function (\app\models\SalePoint $salePoint) {
                    return $salePoint->giftsCount;

                }
            ],
            [
                'header' => 'Последняя продажа',
                'format' => 'raw',
                'value' => function (\app\models\SalePoint $salePoint) {
                    return $salePoint->lastSaleDate;
                }
            ],
            /*
            [

                'attribute' => 'is_archived',
                'format' => 'raw',
                'value' => function ($model) {
                    $statusClass = [CashierParams::STATUS_ACTIVE => "active", CashierParams::STATUS_REJECTED => 'archived'];
                    return sprintf('<span class="bage %s">%s</span>',
                        $statusClass[$model->is_archived] ?? '',
                        CashierParams::getStatusNameById($model->is_archived));
                }
            ]*/
            /*'item_description',
            [
                'attribute' => 'status_id',
                'format' => 'raw',
                'value' => function ($model) {
                    $statusClass = [ActionParams::STATUS_NEW => "new", ActionParams::STATUS_ACTIVE => 'active', ActionParams::STATUS_CLOSED => 'archived', ActionParams::STATUS_REJECTED => "rejected"];
                    return sprintf('<span class="bage %s">%s</span>',
                        $statusClass[$model->status_id] ?? '',
                        ActionParams::getStatusNameById($model->status_id));
                }
            ],*/
            /* [
                 'header' => 'Действия',
                 'class' => \yii\grid\ActionColumn::class,
                 'buttons' => [
                     'update' => function ($url, $model) {
                         return sprintf('<a class="bage active" href="/actions/%d/update" ><i class="typcn icon-default typcn-pencil"></i></a>', $model->id);
                     },
                     'delete' => function ($url, $model) {
                         return Html::a('<i class="typcn icon-default typcn-trash"></i>', "/actions/{$model->id}/move-to-archive", [
                             'data-method' => 'post',
                             'class' => 'bage archive'
                         ]);

                     }
                 ],
                 'template' => '{update} {delete}',
             ]*/
            // ...
        ],
    ]) ?>
</div>
