<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 16/06/2019
 * Time: 12:54
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

$salePoint = $salePointCreateForm->salePoint;
$this->registerJsFile("/js/plugin/timepicker/bootstrap-timepicker.min.js", ['depends' => \yii\web\JqueryAsset::class, 'position' => \yii\web\View::POS_END]);
$this->registerCssFile("/js/plugin/timepicker/bootstrap-timepicker.min.css");
$this->registerJS("initUI()");

$this->registerJsFile("https://maps.googleapis.com/maps/api/js?key=" . \Yii::$app->params['gmaps_key'] . "&language=ru-RU&libraries=places&callback=initMaps", ['position' => \yii\web\View::POS_END]);
$this->beginBlock("modal");
?>
<script>
    function initTimepicker() {
        $(".timepicker").timepicker({
            showMeridian: false,
            defaultTime: '',
        });
    }

    function initUI() {
        initTimepicker();
        initWorkingHoursCheckBoxes();
        initWorkingHoursDefaultState();
    }

    function initWorkingHoursDefaultState() {
        $(".working-hour").each(function (index, el) {

            if ($(el).find("input[type=checkbox]").prop("checked")) {
                $(el).find("input[type=text]").attr("disabled", "");
            }
        });
    }

    function initWorkingHoursCheckBoxes() {
        $(".working-hour input[type=checkbox]").change(function () {
            if ($(this).prop("checked")) {
                $(this).closest('.form-group').find("input[type=text]").attr("disabled", "");
                return;
            }
            $(this).closest('.form-group').find("input[type=text]").removeAttr("disabled");
        });
    }
</script>
<style>
    #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 200px;
    }

    #pac-input:focus {
        border-color: #4d90fe;
    }

    .pac-card {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
    }

    .pac-container {
        z-index: 100000 !important;
        padding-bottom: 12px;
        margin-right: 12px;
    }

    .pac-controls {
        display: inline-block;
        padding: 5px 11px;
    }

    .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }
</style>
<div class="modal" id="map_dialog" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Адрес на карте</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <input id="pac-input" class="controls" type="text" placeholder="Начните вводить адрес">
                    <div style="height:400px" id="update_form_map"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
<? $this->endBlock(); ?>
<? if (Yii::$app->session->hasFlash("success")): ?>
    <div class="alert alert-success" role="alert">
        <?= Yii::$app->session->getFlash("success") ?>
    </div>
<? endif; ?>
<script>
    var view_map;
    var update_form_map;
    var update_form_marker;

    function fillAddress(geometry) {
        $("#address").val($("#pac-input").val());
        $("#lat").val(geometry.location.lat);
        $("#lng").val(geometry.location.lng);
    }

    function initMaps() {
        view_map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: <?=$salePoint->lat?>, lng: <?=$salePoint->lng?>},
            zoom: 13,
            mapTypeId: 'roadmap',
            zoomControl: true,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            rotateControl: false,
            fullscreenControl: false
        });
        var salePointMarker = new google.maps.Marker({
            map: view_map,
            position: {lat: <?=$salePoint->lat?>, lng: <?=$salePoint->lng?>}
        });
        initUpdateFormMap();
    }

    function initUpdateFormMap() {
        update_form_map = new google.maps.Map(document.getElementById('update_form_map'), {
            center: {lat: <?=$salePoint->lat?>, lng: <?=$salePoint->lng?>},
            zoom: 13,
            mapTypeId: 'roadmap',
            zoomControl: true,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            rotateControl: false,
            fullscreenControl: false
        });
        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        update_form_map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        update_form_map.addListener('bounds_changed', function () {
            searchBox.setBounds(update_form_map.getBounds());
        });

        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function () {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }
            place = places[0];
            setPlace(place);
        });
    }

    function setPlace(place) {
        if (update_form_marker) {
            update_form_marker.setMap(null);
        }

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();

        if (!place.geometry) {
            console.log("Returned place contains no geometry");
            return;
        }

        fillAddress(place.geometry);
        // Create a marker for each place.
        update_form_marker = new google.maps.Marker({
            map: update_form_map,
            title: place.name,
            position: place.geometry.location
        });

        if (place.geometry.viewport) {
            // Only geocodes have viewport.
            bounds.union(place.geometry.viewport);
        } else {
            bounds.extend(place.geometry.location);
        }
        update_form_map.fitBounds(bounds);
    }
</script>
<div class="row">
    <div class="col-md-4 margin-bottom-20">
        <ul class="nav nav-tabs nav-justified" id="myTabs-justified" role="tablist">
            <li role="presentation" class="">
                <a href="#home-justified" id="home-tab-justified" role="tab" data-toggle="tab" aria-controls="home"
                   aria-expanded="false">Информация</a>
            </li>
            <li role="presentation" class="active">
                <a href="#profile-justified" role="tab" id="profile-tab-justified" data-toggle="tab"
                   aria-controls="profile" aria-expanded="true">Редактировать</a>
            </li>
            <li role="presentation">
                <a href="#delete-item" role="tab" id="dropdown1-tab-justified" data-toggle="tab"
                   aria-controls="dropdown1">Удалить</a>
            </li>
        </ul>

        <div class="tab-content box-content" id="item-tabs">
            <div class="tab-pane fade" role="tabpanel" id="home-justified" aria-labelledby="home-tab-justified">
                <h4>
                    <!--  <small>#<? /*= $salePoint->id */ ?></small> <? /*= $salePoint->name */ ?> <span
                            class="pull-right label label-success">ОТКРЫТО</span>-->
                </h4>
                <p><?= $salePoint->address ?></p>
                <div id="map" style="width:100%;height:300px">

                </div>
                <div class="row">
                    <div class="col-md-4">
                        <h4>Кассиры:</h4>
                        <ul>
                            <? foreach ($salePoint->cashiers as $cashier) { ?>
                                <li><a href="/cashiers/update/<?= $cashier->id ?>"><?= $cashier->name ?></a></li>
                            <? } ?>
                        </ul>
                    </div>
                    <div class="col-md-8">
                        <h4>Акции:</h4>
                        <ul>
                            <? foreach ($salePoint->actions as $action) { ?>
                                <li><a href="/actions/view/<?= $action->id ?>"><?= $action->name ?></a></li>
                            <? } ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade active in" role="tabpanel" id="profile-justified"
                 aria-labelledby="profile-tab-justified">
                <?
                $form = ActiveForm::begin([
                    'fieldConfig' => [
                        'template' => '{label}<div class="col-sm-8">{input}{error}</div>',
                        'labelOptions' => ['class' => 'col-sm-4 control-label']
                    ],
                    'options' => [
                        'class' => 'form-horizontal'
                    ]
                ]);
                ?>
                <?= $form->field($salePointCreateForm, 'lat')->hiddenInput(['id' => "lat"])->label(false); ?>
                <?= $form->field($salePointCreateForm, 'lng')->hiddenInput(['id' => "lng"])->label(false); ?>


                <?= $form->field($salePointCreateForm, 'name')->textInput(); ?>
                <?= $form->field($salePointCreateForm, 'address', ['template' => '{label}  <div class="col-sm-8">{input}</div> <div class="col-sm-4">
                      {error}
                      </div> 
                       <div class="col-sm-8 text-center">
                          <a data-toggle="modal" data-target="#map_dialog">Указать адрес</a>
                      </div>'])->textInput(['id' => "address", "readonly" => "readonly"]); ?>

                <?= $form->field($salePointCreateForm, 'address_custom')->textInput(); ?>
                <?= $form->field($salePointCreateForm, 'cashiers')->dropDownList(ArrayHelper::map($salePointCreateForm->organization->getCashiers()->asArray()->all(), 'id', 'name'), ['size' => 1, 'class' => 'select2_1', 'multiple' => 'multiple']) ?>
                <?= $form->field($salePointCreateForm, 'actions')->dropDownList(ArrayHelper::map($salePointCreateForm->organization->getActions()->asArray()->all(), 'id', 'name'), ['size' => 1, 'class' => 'select2_1', 'multiple' => 'multiple']) ?>
                <h4>Часы работы</h4>
                <?
                $workingHoursFieldOptions = ['template' => '{input}{error}', 'options' => ['class' => 'bootstrap-timepicker']];
                ?>
                <div class="form-group working-hour">
                    <label for="inp-type-1" class="col-xs-3 control-label">Понедельник</label>
                    <div class="col-xs-5">
                        <div class="input-group">
                            <?= $form->field($salePointCreateForm, 'working_time[0][open]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>

                            <span class="input-group-addon b-0">-</span>
                            <?= $form->field($salePointCreateForm, 'working_time[0][close]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>

                        </div>
                    </div>
                    <div class="col-xs-4 control-label">
                        <!--TODO: если чекбокс отмечен, то остальные поля в этот день "disabled" -->
                        <?= $form->field($salePointCreateForm, 'working_time[0][every_time]', [
                            'template' => '{input}{label}',
                            'options' => [
                                'tag' => false
                            ]

                        ])->checkbox([], false)->label("Круглосуточно"); ?>
                    </div>
                </div>
                <div class="form-group working-hour">
                    <label for="inp-type-1" class="col-xs-3 control-label">Вторник</label>
                    <div class="col-xs-5">
                        <div class="input-group">
                            <?= $form->field($salePointCreateForm, 'working_time[1][open]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>

                            <span class="input-group-addon b-0">-</span>
                            <?= $form->field($salePointCreateForm, 'working_time[1][close]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>

                        </div>
                    </div>
                    <div class="col-xs-4 control-label">
                        <?= $form->field($salePointCreateForm, 'working_time[1][every_time]', [
                            'template' => '{input}{label}',
                            'options' => [
                                'tag' => false
                            ]

                        ])->checkbox([], false)->label("Круглосуточно"); ?>
                    </div>
                </div>
                <div class="form-group working-hour">
                    <label for="inp-type-1" class="col-xs-3 control-label">Среда</label>
                    <div class="col-xs-5">
                        <div class="input-group">
                            <?= $form->field($salePointCreateForm, 'working_time[2][open]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>

                            <span class="input-group-addon b-0">-</span>
                            <?= $form->field($salePointCreateForm, 'working_time[2][close]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>

                        </div>
                    </div>
                    <div class="col-xs-4 control-label">
                        <?= $form->field($salePointCreateForm, 'working_time[2][every_time]', [
                            'template' => '{input}{label}',
                            'options' => [
                                'tag' => false
                            ]

                        ])->checkbox([], false)->label("Круглосуточно"); ?>
                    </div>
                </div>
                <div class="form-group working-hour">
                    <label for="inp-type-1" class="col-xs-3 control-label">Четверг</label>
                    <div class="col-xs-5">
                        <div class="input-group">
                            <?= $form->field($salePointCreateForm, 'working_time[3][open]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>

                            <span class="input-group-addon b-0">-</span>
                            <?= $form->field($salePointCreateForm, 'working_time[3][close]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>

                        </div>
                    </div>
                    <div class="col-xs-4 control-label">
                        <?= $form->field($salePointCreateForm, 'working_time[3][every_time]', [
                            'template' => '{input}{label}',
                            'options' => [
                                'tag' => false
                            ]

                        ])->checkbox([], false)->label("Круглосуточно"); ?>
                    </div>
                </div>
                <div class="form-group working-hour">
                    <label for="inp-type-1" class="col-xs-3 control-label">Пятница</label>
                    <div class="col-xs-5">
                        <div class="input-group">
                            <?= $form->field($salePointCreateForm, 'working_time[4][open]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>

                            <span class="input-group-addon b-0">-</span>
                            <?= $form->field($salePointCreateForm, 'working_time[4][close]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>

                        </div>
                    </div>
                    <div class="col-xs-4 control-label">
                        <?= $form->field($salePointCreateForm, 'working_time[4][every_time]', [
                            'template' => '{input}{label}',
                            'options' => [
                                'tag' => false
                            ]

                        ])->checkbox([], false)->label("Круглосуточно"); ?>
                    </div>
                </div>
                <div class="form-group working-hour">
                    <label for="inp-type-1" class="col-xs-3 control-label">Суббота</label>
                    <div class="col-xs-5">
                        <div class="input-group">
                            <?= $form->field($salePointCreateForm, 'working_time[5][open]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>

                            <span class="input-group-addon b-0">-</span>
                            <?= $form->field($salePointCreateForm, 'working_time[5][close]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>

                        </div>
                    </div>
                    <div class="col-xs-4 control-label">
                        <?= $form->field($salePointCreateForm, 'working_time[5][every_time]', [
                            'template' => '{input}{label}',
                            'options' => [
                                'tag' => false
                            ]

                        ])->checkbox([], false)->label("Круглосуточно"); ?>
                    </div>
                </div>
                <div class="form-group working-hour">
                    <label for="inp-type-1" class="col-xs-3 control-label">Воскресенье</label>
                    <div class="col-xs-5">
                        <div class="input-group">
                            <?= $form->field($salePointCreateForm, 'working_time[6][open]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>

                            <span class="input-group-addon b-0">-</span>
                            <?= $form->field($salePointCreateForm, 'working_time[6][close]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>

                        </div>
                    </div>
                    <div class="col-xs-4 control-label">
                        <?= $form->field($salePointCreateForm, 'working_time[6][every_time]', [
                            'template' => '{input}{label}',
                            'options' => [
                                'tag' => false
                            ]

                        ])->checkbox([], false)->label("Круглосуточно"); ?>
                    </div>
                </div>
                <div class="box-footer text-center">
                    <button type="button" class="btn btn-default btn-sm">Отмена</button>
                    <button type="submit" class="btn btn-success btn-sm">Сохранить</button>
                </div>
                <? ActiveForm::end(); ?>
            </div>
            <div class="tab-pane fade text-center" role="tabpanel" id="delete-item"
                 aria-labelledby="dropdown1-tab-justified">
                <h4 class=" margin-top-20 margin-bottom-50">Вы действительно хотите<br><br> <span class="text-danger">удалить ТТ "<?= $salePoint->name ?>
                        "</span><br><br>
                    из списка ваших Торговых Точек?</h4>
                <a href="/sale-points/delete/<?= $salePoint->id ?>" class="btn btn-danger">Удалить</a>
            </div>
        </div>
    </div>
    <div class="col-md-4 margin-bottom-20">
        <div class="box-content card">
            <h4 class="box-title">Статистика ТТ</h4>
            <div class="card-content">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Кассиры</th>
                        <th class="text-center">Плюсы</th>
                        <th class="text-center">Бонусы</th>
                    </tr>
                    </thead>
                    <tbody>
                    <? foreach ($statistic->getActions() as $action) { ?>
                        <tr>
                            <th colspan="3" class="text-center"><a
                                        href="/actions/view/<?= $action->id ?>"><?= $action->name ?></a></th>
                        </tr>
                        <? foreach ($salePoint->cashiers as $cashier) {
                            $sales = $statistic->getCashierSaleStatistic($cashier);
                            ?>
                            <tr>
                                <td><a href="/cashiers/update/<?= $cashier->id ?>"><?= $cashier->name ?></a></td>
                                <td class="text-center"><?= $sales['sales'] ?></td>
                                <td class="text-center"><?= $sales['gifts'] ?></td>
                            </tr>
                        <? } ?>
                    <? } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-4 margin-bottom-20">
        <div class="box-content card">
            <h4 class="box-title">Лог активностей</h4>
            <div class="card-content" style="max-height: 80vh; overflow-y: auto">
                <ul class="notice-list">
                    <?
                    $salesLog = $statistic->getSalesLog();
                    foreach ($salesLog as $activityItem) {
                        ?>
                        <li>
                            <span class="icon bg-<?= $activityItem->type == \app\models\ActionSale::TYPE_SALE ? "success" : "violet" ?>"><i
                                        class="fa fa-<?= $activityItem->type == \app\models\ActionSale::TYPE_SALE ? "plus" : "check" ?>-circle"></i></span>
                            <span class="title">Выдан <?= $activityItem->type == \app\models\ActionSale::TYPE_SALE ? "плюс" : "бонус" ?>
                                : <?= $activityItem->count ?>шт.</span>

                            <span class="desc">Кассир <a
                                        href="/cashiers/update/<?= $activityItem->cashier->id ?>"><?= $activityItem->cashier->name ?></a> выдал 1 фишку по акции <a
                                        href="/actions/view/<?= $activityItem->action->id ?>">"<?= $activityItem->action->name ?>
                                    "</a> клиенту <a
                                        href="/users/view/<?= $activityItem->user->id ?>"><?= $activityItem->user->name ?></a></span>

                            <span class="time"><?= date("d.m.Y в H:i", strtotime($activityItem->created_at)) ?></span>
                        </li>
                    <? } ?>
                </ul>
            </div>
        </div>
    </div>
</div>

