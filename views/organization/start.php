<?

use app\models\Cashier;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->registerJsFile("https://maps.googleapis.com/maps/api/js?key=" . \Yii::$app->params['gmaps_key'] . "&lang=ru&libraries=places&callback=initAutocomplete", ['position' => \yii\web\View::POS_END]);
$this->registerJsFile("/js/plugin/timepicker/bootstrap-timepicker.min.js", ['depends' => \yii\web\JqueryAsset::class, 'position' => \yii\web\View::POS_END]);
$this->registerCssFile("/js/plugin/timepicker/bootstrap-timepicker.min.css");
$this->registerJS("initUI()");
?>
<script>
    function initTimepicker() {
        $(".timepicker").timepicker({
            showMeridian: false,
            defaultTime: '',
        });
    }

    var map;
    var marker;

    function initUI() {
        initTimepicker();
        initWorkingHoursCheckBoxes();
    }

    function initWorkingHoursCheckBoxes() {
        $(".working-hour input[type=checkbox]").change(function () {
            if ($(this).prop("checked")) {
                $(this).closest('.form-group').find("input[type=text]").attr("disabled", "");
                return;
            }
            $(this).closest('.form-group').find("input[type=text]").removeAttr("disabled");
        });
    }

    function fillAddress(geometry) {
        $("#address").val($("#address").val());
        $("#lat").val(geometry.location.lat);
        $("#lng").val(geometry.location.lng);
    }

    function initAutocomplete() {
        map = new google.maps.Map(document.getElementById('map'), {
            center: {
                lat: -33.8688,
                lng: 151.2195
            },
            zoom: 13,
            mapTypeId: 'roadmap',
            zoomControl: true,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            rotateControl: false,
            fullscreenControl: false
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('address');
        var searchBox = new google.maps.places.SearchBox(input);


        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function () {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }
            place = places[0];
            setPlace(place);
        });
    }

    function setPlace(place) {
        if (marker) {
            marker.setMap(null);
        }

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();

        if (!place.geometry) {
            console.log("Returned place contains no geometry");
            return;
        }

        fillAddress(place.geometry);
        // Create a marker for each place.
        marker = new google.maps.Marker({
            map: map,
            title: place.name,
            position: place.geometry.location
        });

        if (place.geometry.viewport) {
            // Only geocodes have viewport.
            bounds.union(place.geometry.viewport);
        } else {
            bounds.extend(place.geometry.location);
        }
        map.fitBounds(bounds);
    }
</script>
<div class="row fast-start">
    <div class="col-xs-12 margin-bottom-20">
        <h4 class="box-title">Быстрый старт</h4>
        <ul class="nav nav-tabs nav-justified" id="myTabs-justified" role="tablist">
            <li role="presentation" class="<?= $step != 1 ?: " active " ?>"><a href="#tab-1" id="tabs" role="tab"
                                                                               data-toggle="tab" aria-controls="company"
                                                                               aria-expanded="true">1 - Компания</a>
            </li>
            <li role="presentation" class="<?= $step != 2 ?: " active " ?>"><a href="#tab-2" role="tab" id="tabs"
                                                                               data-toggle="tab" aria-controls="shops"
                                                                               aria-expanded="false">2 - Торговые
                    точки</a>
            </li>
            <li role="presentation" class="<?= $step != 3 ?: " active " ?>"><a href="#tab-3" role="tab" id="tabs"
                                                                               data-toggle="tab" aria-controls="cash"
                                                                               aria-expanded="false">3 - Кассиры</a>
            </li>
            <li role="presentation" class="<?= $step != 4 ?: " active " ?>"><a href="#tab-4" role="tab" id="tabs"
                                                                               data-toggle="tab" aria-controls="offers"
                                                                               aria-expanded="false">4 - Акции</a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent-justified">
            <div class="tab-pane fade <?= $step != 1 ?: " active in " ?> " role="tabpanel" id="tab-1"
                 aria-labelledby="tabs">
                <? $orgForm = ActiveForm::begin(); ?>
                <div class="row">

                    <div class="col-lg-8">
                        <div class="row">
                            <div class="col-sm-6">
                                <?= $orgForm->field($orgUpdateModel, 'companyName')->textInput(); ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $orgForm->field($orgUpdateModel, 'logo')->fileInput(); ?>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-sm-6">
                                <?= $orgForm->field($orgUpdateModel, 'contactPerson')->textInput(); ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $orgForm->field($orgUpdateModel, 'contactPersonPhone')->textInput(['placeholder' => 'Телефон контактного лица']); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <?= $orgForm->field($orgUpdateModel, 'contactPersonEmployment')->textInput(['placeholder' => 'Должность контактного лица']); ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $orgForm->field($orgUpdateModel, 'companyContactEmail')->textInput(['placeholder' => 'Почта контактного лица']); ?>
                            </div>
                        </div>
                        <div class="next-btn">
                            <button type="submit" class="btn btn-primary">Далее</button>
                        </div>
                    </div>
                    <div class="col-lg-4 about-section step-1">
                        <p><strong>Название компании</strong> отображается в приложении, к нему опривязываются все
                            добавленные Торговые точки и созданные акции. Советуем добавлять не юридическое название, а
                            название бренда, под которым вас знают ваши клиенты.</p>

                        <p>Загрузите <strong>Логотип</strong> - он будет использоваться везде, где упоминается ваша
                            компания. Для каждой отдельной акциии можно загрузить другое изображение.</p>

                        <p>В качестве <strong>Контактного Лица</strong> укажите того, с кем может связаться
                            администрация <strong class="color-pink">comeback+</strong> для решения любых вопросов,
                            касательно использования сервиса.</p>

                        <hr/>
                        <small><em>Все указанные здесь данные можно изменить в любое время в соответствующем разделе
                                админпанели.</em></small>
                    </div>

                </div>
                <? ActiveForm::end(); ?>
            </div>
            <div class="tab-pane fade <?= $step != 2 ?: " active in " ?>" role="tabpanel" id="tab-2"
                 aria-labelledby="tabs">
                <? $salePointForm = ActiveForm::begin([
                    'enableAjaxValidation' => true
                ]) ?>
                <div class="row">

                    <?= $salePointForm->field($salePointCreateModel, 'lat', ['template' => '{input}'])->hiddenInput(['id' => 'lat'])->label(false) ?>
                    <?= $salePointForm->field($salePointCreateModel, 'lng', ['template' => '{input}'])->hiddenInput(['id' => 'lng'])->label(false) ?>
                    <div class="col-lg-7">
                        <div class="row">
                            <div class="col-lg-8">
                                <?= $salePointForm->field($salePointCreateModel, 'name')->textInput(); ?>
                                <?= $salePointForm->field($salePointCreateModel, 'address')->textInput(['id' => 'address', 'placeholder' => 'Введите адрес']) ?>

                                <?= $salePointForm->field($salePointCreateModel, 'address_custom')->textInput(); ?>


                                <div style="height:400px" id="map"></div>

                            </div>
                            <div class="col-lg-4">

                                <div class="form-group working-hour">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <label for="inp-type-1" class="control-label">Понедельник</label>
                                        </div>
                                        <div class="col-xs-6">
                                            <?= $salePointForm->field($salePointCreateModel, 'working_time[0][every_time]', [
                                                'template' => '{input}{label}',
                                                'options' => [
                                                    'tag' => false
                                                ]

                                            ])->checkbox([], false)->label("Круглосуточно"); ?>
                                        </div>
                                    </div>
                                    <?
                                    $workingHoursFieldOptions = ['template' => '{input}{error}', 'options' => ['class' => 'bootstrap-timepicker']];
                                    ?>
                                    <div class="input-group">
                                        <?= $salePointForm->field($salePointCreateModel, 'working_time[0][open]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>
                                        <span class="input-group-addon">-</span>
                                        <?= $salePointForm->field($salePointCreateModel, 'working_time[0][close]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>
                                    </div>
                                </div>
                                <div class="form-group working-hour">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <label for="inp-type-1" class="control-label">Вторник</label>
                                        </div>
                                        <div class="col-xs-6">
                                            <?= $salePointForm->field($salePointCreateModel, 'working_time[1][every_time]', [
                                                'template' => '{input}{label}',
                                                'options' => [
                                                    'tag' => false
                                                ]

                                            ])->checkbox([], false)->label("Круглосуточно"); ?>
                                        </div>
                                    </div>
                                    <div class="input-group">
                                        <?= $salePointForm->field($salePointCreateModel, 'working_time[1][open]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>

                                        <span class="input-group-addon">-</span>
                                        <?= $salePointForm->field($salePointCreateModel, 'working_time[1][close]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>
                                    </div>
                                </div>
                                <div class="form-group working-hour">

                                    <div class="row">
                                        <div class="col-xs-6">
                                            <label for="inp-type-1" class="control-label">Среда</label>
                                        </div>
                                        <div class="col-xs-6">
                                            <?= $salePointForm->field($salePointCreateModel, 'working_time[2][every_time]', [
                                                'template' => '{input}{label}',
                                                'options' => [
                                                    'tag' => false
                                                ]

                                            ])->checkbox([], false)->label("Круглосуточно"); ?>
                                        </div>
                                    </div>
                                    <div class="input-group">
                                        <?= $salePointForm->field($salePointCreateModel, 'working_time[2][open]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>

                                        <span class="input-group-addon">-</span>
                                        <?= $salePointForm->field($salePointCreateModel, 'working_time[2][close]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>

                                    </div>
                                </div>
                                <div class="form-group working-hour">

                                    <div class="row">
                                        <div class="col-xs-6">
                                            <label for="inp-type-1" class="control-label">Четверг</label>
                                        </div>
                                        <div class="col-xs-6">
                                            <?= $salePointForm->field($salePointCreateModel, 'working_time[3][every_time]', [
                                                'template' => '{input}{label}',
                                                'options' => [
                                                    'tag' => false
                                                ]

                                            ])->checkbox([], false)->label("Круглосуточно"); ?>
                                        </div>
                                    </div>
                                    <div class="input-group">
                                        <?= $salePointForm->field($salePointCreateModel, 'working_time[3][open]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>
                                        <span class="input-group-addon">-</span>
                                        <?= $salePointForm->field($salePointCreateModel, 'working_time[3][close]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>
                                    </div>
                                </div>
                                <div class="form-group working-hour">

                                    <div class="row">
                                        <div class="col-xs-6">
                                            <label for="inp-type-1" class="control-label">Пятница</label>
                                        </div>
                                        <div class="col-xs-6">
                                            <?= $salePointForm->field($salePointCreateModel, 'working_time[4][every_time]', [
                                                'template' => '{input}{label}',
                                                'options' => [
                                                    'tag' => false
                                                ]

                                            ])->checkbox([], false)->label("Круглосуточно"); ?>
                                        </div>
                                    </div>
                                    <div class="input-group">
                                        <?= $salePointForm->field($salePointCreateModel, 'working_time[4][open]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>

                                        <span class="input-group-addon">-</span>
                                        <?= $salePointForm->field($salePointCreateModel, 'working_time[4][close]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>

                                    </div>
                                </div>
                                <div class="form-group working-hour">

                                    <div class="row">
                                        <div class="col-xs-6">
                                            <label for="inp-type-1" class="control-label">Суббота</label>
                                        </div>
                                        <div class="col-xs-6">
                                            <?= $salePointForm->field($salePointCreateModel, 'working_time[5][every_time]', [
                                                'template' => '{input}{label}',
                                                'options' => [
                                                    'tag' => false
                                                ]

                                            ])->checkbox([], false)->label("Круглосуточно"); ?>
                                        </div>
                                    </div>

                                    <div class="input-group">
                                        <?= $salePointForm->field($salePointCreateModel, 'working_time[5][open]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>

                                        <span class="input-group-addon b-0">-</span>
                                        <?= $salePointForm->field($salePointCreateModel, 'working_time[5][close]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>
                                    </div>
                                </div>
                                <div class="form-group working-hour">


                                    <div class="row">
                                        <div class="col-xs-6">
                                            <label for="inp-type-1" class="control-label">Воскресенье</label>
                                        </div>
                                        <div class="col-xs-6">
                                            <?= $salePointForm->field($salePointCreateModel, 'working_time[6][every_time]', [
                                                'template' => '{input}{label}',
                                                'options' => [
                                                    'tag' => false
                                                ]

                                            ])->checkbox([], false)->label("Круглосуточно"); ?>
                                        </div>
                                    </div>
                                    <div class="input-group">
                                        <?= $salePointForm->field($salePointCreateModel, 'working_time[6][open]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>

                                        <span class="input-group-addon b-0">-</span>
                                        <?= $salePointForm->field($salePointCreateModel, 'working_time[6][close]', $workingHoursFieldOptions)->textInput(['placeholder' => 'Закрыто', 'class' => 'timepicker form-control'])->label(false); ?>

                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="next-btn">
                            <button type="submit" class="btn btn-success btn-sm">Добавить</button>
                            <a class="btn btn-primary" href="/organization/start?currentStep=3">Далее</a>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <table class="table table-striped table-bordered display customers dataTable"
                               style="width:100%">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Название</th>
                                <th>Адрес</th>
                            </tr>
                            </thead>
                            <tbody>
                            <? foreach ($salePoints as $salePoint) { ?>
                                <tr>
                                    <td>
                                        <?= $salePoint->id ?>
                                    </td>
                                    <td>
                                        <?= $salePoint->name ?>
                                    </td>
                                    <td>
                                        <?= $salePoint->address ?>
                                    </td>
                                </tr>
                            <? } ?>
                            <? if (empty($salePoints)) { ?>
                                <tr>
                                    <td colspan="9">
                                        <div class="empty text-center">Вы ещё не добавили ни одной Торговой Точки
                                        </div>
                                    </td>
                                </tr>
                            <? } ?>
                            </tbody>
                        </table>

                        <div class="about-section step-2">
                            <p><strong>Название </strong> Торговой Точки отображается в системе для всех пользователей.
                                Если у Вашей компании несколько торговых точек, добавьте какое-нибудь примечание в
                                названии, чтобы пользователи не путались. <em>Например: "Кафе у причала", "Кафе ТЦ
                                    'Северный'"... </em></p>

                            <p><strong>Примечание к адресу</strong> Если для поиска вашей Торговой Точки одного адреса
                                мало, в примечании укажите ориентир. <em>Например: "ТЦ "Европа" 3-этаж", "возле входа на
                                    вокзал" ... </em></p>

                            <p>Обязательно укажите <strong>график работы</strong> для каждого из дней, если в какой-то
                                из дней ТТ не работает, оставьте поля пустыми</p>

                            <hr/>
                            <small><em>Все указанные здесь данные можно изменить в любое время в соответствующем
                                    разделе админпанели.</em></small>

                        </div>
                    </div>

                </div>
                <? ActiveForm::end(); ?>
            </div>
            <div class="tab-pane fade <?= $step != 3 ?: " active in " ?>" role="tabpanel" id="tab-3"
                 aria-labelledby="tabs">
                <div class="row">
                    <div class="col-lg-6">
                        <? $cashierForm = ActiveForm::begin([
                            'enableAjaxValidation' => true
                        ]); ?>

                        <?
                        echo $cashierForm->field($cashierCreateModel, 'name')->textInput();
                        echo $cashierForm->field($cashierCreateModel, 'phone')->textInput();
                        echo $cashierForm->field($cashierCreateModel, 'salePointIds')->dropDownList(\yii\helpers\ArrayHelper::map($salePoints, 'id', 'name'), ['size' => 1, 'class' => 'select2_1', 'multiple' => 'multiple']);

                        ?>

                        <div class="modal-footer text-center">
                            <button type="submit" class="btn btn-success btn-sm">Добавить</button>
                            <a class="btn btn-primary" href="/organization/start?currentStep=4">Далее</a>
                        </div>
                        <? ActiveForm::end(); ?>

                    </div>
                    <div class="col-lg-6">
                        <table class="table table-striped table-bordered display customers dataTable"
                               style="width:100%">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Имя</th>
                                <th>Телефон</th>
                                <th>Торговая точка</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?

                            function getCashierSalepoints(Cashier $model)
                            {
                                $salePoints = $model->getSalePoints()->asArray()->all();

                                return implode(", ", array_map(function ($item) {
                                    return Html::a($item['name'], '/sale-points');
                                }, $salePoints));
                            }


                            foreach ($cashiers as $cashier) { ?>
                                <tr>
                                    <td>
                                        <?= $cashier->id; ?>
                                    </td>
                                    <td>
                                        <?= $cashier->name; ?>
                                    </td>
                                    <td>
                                        <?= $cashier->user->phone; ?>
                                    </td>
                                    <td>
                                        <?= getCashierSalepoints($cashier); ?>
                                    </td>
                                </tr>
                            <? } ?>
                            <? if (empty($cashiers)) { ?>
                                <tr>
                                    <td colspan="9">
                                        <div class="empty">Вы ещё не добавили не одного Кассира.</div>
                                    </td>
                                </tr>
                            <? } ?>
                            </tbody>
                        </table>
                        <div class="about-section step-3">
                            <p class="color-pink"><strong>Внимание! </strong> Кассир должен быть пользователем
                                приложения comeback+, в противном случае вы не сможете его добавить к списку Ваших
                                кассиров.</p>

                            <p>Имя кассира видно только Вам и самому кассиру.</p>

                            <p>Прикерпить кассира можно сразу к нескольким Торговым точкам.</p>

                            <hr/>
                            <small><em>Все указанные здесь данные можно изменить в любое время в соответствующем
                                    разделе админпанели.</em></small>

                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade <?= $step != 4 ?: " active in " ?>" role="tabpanel" id="tab-4"
                 aria-labelledby="tabs">
                <div class="row">
                    <div class="col-lg-6">
                        <img src="/img/admin-panel.png">
                    </div>
                    <div class="col-lg-6">
                        <div class="about-section step-4">
                            <p>Для создания акции мы разработалии конструктор, который позволит Вам увидеть, как будет
                                смотреться карточка акции в приложении. Постарайтесь отнестись с вниманием к оформлению
                                акционного предложения.</p>

                            <p><strong>Ваша акция появится в приложении после того, как пройдет проверку
                                    модератором</strong>. Также акцию необходимо привязать к вашим Торговым Точкам.</p>

                            <p>Любые изменения в акционном предложении сначала проходят модерацию, и только после
                                утверждения администрацией сервиса появляются в приложении.</p>
                            <p class="text-center"><a href="/actions/create" class="btn btn-primary">Создать акцию</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>