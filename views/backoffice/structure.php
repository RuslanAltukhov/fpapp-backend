<? use app\models\User;
use yii\grid\GridView;
use yii\widgets\ActiveForm; ?>
<? if (Yii::$app->session->hasFlash("success")) { ?>
    <div class="alert alert-success"><?= Yii::$app->session->getFlash("success") ?></div>
<? } ?>
<? if (Yii::$app->session->hasFlash("error")) { ?>
    <div class="alert alert-danger"><?= Yii::$app->session->getFlash("error") ?></div>
<? } ?>
<div class="row">
    <div class="col-lg-4">
        <h1>Моя структура</h1>
    </div>
    <div class="col-lg-3">
        <div class="box-content card white">
            <h4 class="box-title">Активация агента</h4>
            <? $agentForm = ActiveForm::begin(
                [
                    'fieldConfig' => [
                        'template' => '{input}{error}'
                    ]
                ]
            ); ?>
            <!-- /.box-title -->
            <div class="card-content">

                <div class="input-group">
                    <?= $agentForm->field($agentPackageActivatorForm, 'userId')->textInput(['placeholder' => 'ID пользователя']); ?>

                    <div class="input-group-btn">
                        <button class="btn"><?=$limitationsAdapter->getUsedAgentPackets()?>/<?=$limitationsAdapter->getAgentPacketsLimitation()?></button>
                        <button type="submit" class="btn btn-violet no-border" style="color: #fff;">Активировать
                        </button>
                    </div>
                </div>
            </div>
            <? ActiveForm::end(); ?>
        </div>
    </div>

    <div class="col-lg-5">
        <div class="box-content card white">
            <h4 class="box-title">Новая компания</h4>
            <? $createUserForm = ActiveForm::begin(
                [
                    'fieldConfig' => [
                        'template' => '{input}{error}'
                    ]
                ]
            ); ?>
            <!-- /.box-title -->
            <div class="card-content">
                <div class="row small-spacing">
                    <div class="col-xs-5">
                        <!--  <input type="tel" placeholder="Телефон" class="form-control">-->
                        <?= $createUserForm->field($createUserFormModel, 'phone')->textInput(['placeholder' => 'Телефон']); ?>
                    </div>
                    <div class="col-xs-4">
                        <?= $createUserForm->field($createUserFormModel, 'password')->textInput(['placeholder' => 'Пароль']); ?>
                    </div>
                    <div class="col-xs-3">
                        <button type="submit" class="btn btn-primary no-border">Создать</button>
                    </div>
                </div>
            </div>
            <? ActiveForm::end(); ?>
        </div>
    </div>
</div>


<div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $provider,
        'columns' => [
            [
                'header' => 'ID',
                'value' => function ($data) {
                    return $data['user']->getId();
                },
            ],
            [
                'header' => 'Имя',
                'value' => function ($data) {
                    return $data['user']->name;
                },
            ],
            [
                'header' => 'Тип',
                'value' => function ($data) {
                    return User::$userTypeLabels[$data['user']->user_type];
                },
            ],
            [
                'header' => 'Уровень',
                'attribute' => 'level',
                'value' => 'level'
            ],
            [
                'header' => 'Доход с пользователя',
                'attribute' => 'feeSum',
                'value' => 'feeSum'
            ],
            [
                'header' => 'Контакты',
                'value' => function ($data) {
                    return $data['phone'];
                }
            ],
            [
                'header' => 'Кол-во рефералов',
                'attribute' => 'referalsCount',
                'value' => 'referalsCount'
            ],
            [
                'header' => 'Дата регистрации',
                'value' => function ($data) {
                    return $data['user']->created_at;
                }
            ]
        ]
    ]); ?>
</div>