<? use yii\bootstrap\ActiveForm;

$form = ActiveForm::begin([
    'options' => [
        'enctype' => 'multipart/form-data',
        'class' => 'form-horizontal'
    ],
    'fieldConfig' => [
        'template' => '{label}<div class="col-sm-9">{input}</div>{error}',
        'labelOptions' => ['class' => 'col-sm-3 control-label']
    ]
]); ?>
    <div class="row  small-spacing">
        <? if ($success) { ?>
            <div style="width:100%" class="alert alert-success"><?= $success ?></div>
        <? } ?>
    </div>
    <div class="row  small-spacing">

        <div class="col-lg-4">
            <div class="box-content " id="myTabContent-justified">
                <h4 class="box-title">Общая информация</h4>
                <div class="card-content">
                    <?= $form->field($model, "profilePhoto")->fileInput(); ?>
                    <?= $form->field($model, "name")->textInput(); ?>
                    <?= $form->field($model, "surname")->textInput(); ?>
                    <?= $form->field($model, "email")->textInput(); ?>
                    <?= $form->field($model, "referer")->textInput(); ?>
                    <?= $form->field($model, "show_contacts",['template'=>'{label}<div class="col-sm-9">{input}</div>{error}'])
                        ->checkbox(['labelOptions' => ['class' => 'col-sm-3 control-label']],false); ?>
                    <!-- <div class="form-group  row margin-top-20">
                         <label for="exampleInputFile" class="col-sm-3 control-label">Фото</label>
                         <div class="col-sm-9">
                             <input type="file" id="exampleInputFile">
                         </div>
                     </div>-->

                    <?= $form->field($model, "about")->textarea(); ?>


                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="box-content " id="myTabContent-justified">
                <h4 class="box-title">Ваши аккаунты в соцсетях:</h4>
                <div class="card-content">
                    <?= $form->field($model, "vk_link", ['template' => ' <div class="input-group-btn">
                        <label for="ig-1" class="btn btn-default"><i class="fa fa-vk"></i></label>
                    </div>{input}{error}', 'options' => ['class' => 'input-group margin-bottom-20']])->textInput(['placeholder' => 'Ссылка на профиль Вконтакте'])->label(false); ?>

                    <?= $form->field($model, "fb_link", ['template' => ' <div class="input-group-btn">
                        <label for="ig-1" class="btn btn-default"><i class="fa fa-facebook"></i></label>
                    </div>{input}{error}', 'options' => ['class' => 'input-group margin-bottom-20']])->textInput(['placeholder' => 'Ссылка на профиль Facebook'])->label(false); ?>

                    <?= $form->field($model, "ok_link", ['template' => ' <div class="input-group-btn">
                        <label for="ig-1" class="btn btn-default"><i class="fa fa-odnoklassniki"></i></label>
                    </div>{input}{error}', 'options' => ['class' => 'input-group margin-bottom-20']])->textInput(['placeholder' => 'Ссылка на профиль Одноклассники'])->label(false); ?>

                    <?= $form->field($model, "mm_link", ['template' => ' <div class="input-group-btn">
                        <label for="ig-1" class="btn btn-default"><i class="fa fa-at"></i></label>
                    </div>{input}{error}', 'options' => ['class' => 'input-group margin-bottom-20']])->textInput(['placeholder' => 'Ссылка на профиль Мой мир'])->label(false); ?>

                    <?= $form->field($model, "instagram_link", ['template' => ' <div class="input-group-btn">
                        <label for="ig-1" class="btn btn-default"><i class="fa fa-instagram"></i></label>
                    </div>{input}{error}', 'options' => ['class' => 'input-group margin-bottom-20']])->textInput(['placeholder' => 'Ссылка на профиль Инстаграм'])->label(false); ?>


                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="box-content " id="myTabContent-justified">
                <h4 class="box-title">Безопасность</h4>
                <div class="card-content" style="padding: 0 15px;">
                    <?= $form->field($model, "new_password", ['template' => '{label}{input}', 'labelOptions' => ['class' => 'control-label']])->textInput(); ?>
                    <?= $form->field($model, "repeat_password", ['template' => '{label}{input}', 'labelOptions' => ['class' => 'control-label']])->textInput(); ?>
                    <!-- <div class="form-group">
                         <label for="pass" class="control-label">Новый пароль</label>
                         <input type="password" class="form-control" id="pass" placeholder="Новый пароль">
                     </div>
                     <div class="form-group">
                         <label for="pass" class="control-label">Повтор нового пароля</label>
                         <input type="password" class="form-control" id="pass" placeholder="Новый пароль">
                     </div>-->

                    <blockquote>
                        <p>Пароль от Личного кабинета и приложения совпадает.</p>
                    </blockquote>
                </div>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <button type="submit" class="btn btn-primary btn-sm btn-block">Сохранить</button>
        </div>
        <div class="col-md-4"></div>
    </div>
<? ActiveForm::end() ?>