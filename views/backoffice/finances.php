<?

use yii\grid\GridView;
use yii\bootstrap\ActiveForm;

?>
    <div style="width:100%" class="row  small-spacing">
        <div class="col-md-4">
            <div class="table-responsive box-content">
                <h4 class="box-title">Личный баланс</h4>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Показатель</th>
                            <th>Сумма</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>Текущиий баланс</th>
                            <td><strong><?= $model->balance ?> <span class="text-danger">Cb+</span></strong>
                            </td>
                        </tr>
                        <tr>
                            <th>Ожиданиет вывода</th>
                            <td><strong>0000 руб.</strong></td>
                        </tr>
                        <tr>
                            <th>Всего выведено</th>
                            <td><strong>0000 руб.</strong></td>
                        </tr>
                    </tbody>
                </table>
            </div>


            <div class="table-responsive box-content">
                <h4 class="box-title">Пополнение баланса</h4>
                <div class="dropdown js__drop_down">
                    <h4 class="box-title"><strong class="text-danger">1 Cb+ = 1 руб.</strong></h4>
                </div>
                <? $balanceForm = ActiveForm::begin([
                'fieldConfig' => [
                    'template' => '{error}{input}',
                    'labelOptions' => ['class' => 'col-sm-3 control-label']
                ]
            ]); ?>
                    <div class="input-group margin-bottom-20">

                        <!-- <input type="number" class="form-control" placeholder="Введиите сумму">-->
                        <?= $balanceForm->field($balanceFormModel, 'sum')->textInput(['placeholder' => 'Введите сумму пополнения']) ?>
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-success no-border"><i class="fa fa-paper-plane text-white"></i>
                    </button>
                            </div>
                    </div>
                    <? ActiveForm::end(); ?>
            </div>
            
            
            <div class="table-responsive box-content">
                <h4 class="box-title">Вывод средств</h4>
                <div class="dropdown js__drop_down">
                    <h4 class="box-title"><strong class="text-danger">1 Cb+ = 1 руб.</strong></h4>
                </div>
                <? $outForm = ActiveForm::begin([
                'fieldConfig' => [
                    'template' => '{input}{error}',
                    'labelOptions' => ['class' => 'col-sm-3 control-label']
                ]
            ]); ?>
                    <div class="input-group margin-bottom-20">

                        <!-- <input type="number" class="form-control" placeholder="Введиите сумму">-->
                        <?= $outForm->field($moneyOutForm, 'sum')->textInput(['placeholder' => 'Введите сумму вывода']) ?>
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-success no-border"><i class="fa fa-paper-plane text-white"></i>
                    </button>
                            </div>
                    </div>
                    <? ActiveForm::end(); ?>
            </div>

            <div class="table-responsive box-content bg-muted">
                <h4 class="box-title text-muted">Перевод средств</h4>
                <div class="dropdown js__drop_down">
                    <h4 class="box-title"><strong class="text-muted">мин. 1000 Cb+</strong></h4>
                </div>
                <div class="input-group margin-bottom-20">
                    <!-- <input type="number" class="form-control" placeholder="Введиите сумму">-->
                    <div class="form-group field-moneyoutform-sum row" style="margin: 0 !important;">
                        <input disabled="disabled" type="text" id="moneyoutform-sum" class="form-control col-xs-6" name="MoneyOutForm[sum]" placeholder="ID или телефон пользователя" aria-invalid="false">
                        <input disabled="disabled" type="text" id="moneyoutform-sum" class="form-control  col-xs-6" name="MoneyOutForm[sum]" placeholder="Введите сумму перевода" aria-invalid="false">
                        <p class="help-block help-block-error"></p>
                    </div>
                    <div class="input-group-btn">
                        <button type="submit" class="btn no-border"><i class="fa fa-paper-plane text-white"></i>
                    </button>
                    </div>
                </div>
                <h5 class="text-muted">Перевод средств между пользователями временно заблокирован</h5>
            </div>

        </div>

        <div class="col-md-8">
            <div class="table-responsive box-content">
                <?= GridView::widget([
                'dataProvider' => $transactionsList,
                'columns' => [
                    'id',
                    'sum',
                    [
                        'attribute' => 'created_at',
                        'value' => function ($model) {
                            return date("d.m.Y в H:i", $model->created_at);
                        }
                    ],
                    [
                        'attribute' => 'type',
                        'value' => function ($model) {
                            return \app\models\Transaction::$transactionTypeNames[$model->type] ?? '';
                        }
                    ],
                    [
                        'attribute' => 'status',
                        'value' => function ($model) {
                            return \app\models\Transaction::$statusNames[$model->status] ?? '';
                        }
                    ]
                ]
            ]); ?>
            </div>
        </div>
    </div>
