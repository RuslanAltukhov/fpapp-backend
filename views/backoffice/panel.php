<?

use yii\bootstrap\ActiveForm;

?>
    <div class="row small-spacing">
        <div class="col-lg-4 col-md-12 col-xs-12">
            <div class="box-content">
                <h4 class="box-title"><i class="ico glyphicon glyphicon-link"></i>Мой партнёрский код: <span class="text-danger"><?= $model->getReferalCode(); ?></span></h4>

                <table class="table">
                    <tbody>
                        <tr>
                            <th>Ссылка на сайт</th>
                            <td>
                                <button type="button" data-clipboard-text="<?= $model->getSiteReferalLink() ?>" class="referal_link btn btn-icon btn-icon-right btn-sm btn-block"><i
                                    class="ico fa fa-flag"></i><?= $model->getSiteReferalLink() ?>
                        </button>
                            </td>
                        </tr>
                        <tr>
                            <th>Регистрация</th>
                            <td>
                                <button type="button" data-clipboard-text="<?= $model->getRegistrationReferalLink() ?>" class="referal_link btn btn-icon btn-icon-right btn-sm btn-block"><i
                                    class="ico fa fa-flag"></i><?= $model->getRegistrationReferalLink() ?>
                        </button>
                            </td>
                        </tr>
                        <tr>
                            <th>Ссылка на вебинар</th>
                            <td>
                                <button type="button" data-clipboard-text="<?= $model->getWebinarLink() ?>" class="referal_link btn btn-icon btn-icon-right btn-sm btn-block"><i
                                    class="ico fa fa-flag"></i><?= $model->getWebinarLink() ?>
                        </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-xs-12">
            <div class="box-content">
                <h4 class="box-title"><i class="ico fa fa-pie-chart"></i>Сводка</h4>
                <table class="table">
                    <tbody>
                        <tr>
                            <th>Всего переходов</th>
                            <td>
                                <?= $model->visits ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Всего регистраций</th>
                            <td>
                                <?= $model->registrations ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Конверсия</th>
                            <td>
                                <?= $model->conversion; ?>%</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-xs-12">
            <div class="box-content">
                <h4 class="box-title"><i class="ico fa fa-money"></i>Баланс</h4>
                <table class="table">
                    <tbody>
                        <tr>
                            <th>Текущиий баланс</th>
                            <td><strong><?= $model->getBalance(); ?> <span class="text-danger">Cb+</span></strong>
                            </td>
                        </tr>
                        <tr>
                            <th>Ожиданиет вывода</th>
                            <td><strong>0 000 <span class="text-danger">Cb+</span></strong></td>
                        </tr>
                        <tr>
                            <th>Всего выведено</th>
                            <td><strong>0 000 <span class="text-danger">Cb+</span></strong></td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row small-spacing">
        <div class="col-md-6">
            <div class="box-content">
                <h4 class="box-title"><i class="ico fa fa-calendar"></i>Последние действия</h4>
                <div class="">
                    <? if (empty($model->history)) { ?>
                        <h3>Пока нет активности:(</h3>
                        <? } ?>

                            <ul class="notice-list log-list">
                                <? foreach ($model->history as $historyItem) { ?>
                                    <li>
                                        <span class="icon bg-warning"><i class="fa fa-user-plus"></i></span>
                                        <span class="title">Новый Реферал</span>
                                        <span class="desc"><a href="profile.html"><?= $historyItem->name ?></a> Статус: <span
                                    class="agent-acc"><?= \app\models\User::$userTypeLabels[$historyItem->user_type] ?></span> </span>
                                        <span class="time"><?= $historyItem->getCreateDateFormatted() ?></span>
                                    </li>
                                    <? } ?>
                            </ul>
                            <!-- <ul class="notice-list">
                 <li>
                     <span class="icon bg-warning"><i class="fa fa-user-plus"></i></span>
                     <span class="title">Новый Реферал</span>
                     <span class="desc"><a href="profile.html">Виталий</a>, г. Калининград. Статус: <span
                                 class="partner-acc">Партнёр</span> </span>
                     <span class="time">19.09.2019 в 10:30</span>
                 </li>
                 <li>
                     <span class="icon bg-warning"><i class="fa fa-user-plus"></i></span>
                     <span class="title">Новый Реферал</span>
                     <span class="desc"><a href="profile.html">Виталий</a>, г. Калининград. Статус: <span
                                 class="agent-acc">Агент</span> </span>
                     <span class="time">19.09.2019 в 10:30</span>
                 </li>
                 <li>
                     <span class="icon bg-warning"><i class="fa fa-user-plus"></i></span>
                     <span class="title">Новый Реферал</span>
                     <span class="desc"><a href="profile.html">Виталий</a>, г. Калининград. Статус: <span
                                 class="user-acc">Пользователь</span> </span>
                     <span class="time">19.09.2019 в 10:30</span>
                 </li>
                 <li>
                     <span class="icon bg-success"><i class="fa fa-plus-circle"></i></span>
                     <span class="title">Новая Торговая Точка</span>
                     <span class="desc"><a href="tochka.html">"Ларёк вокзал"</a> г. Калининград. Статус: <span
                                 class="start-tt">Старт</span></span>
                     <span class="time">19.09.2019 в 10:30</span>
                 </li>
                 <li>
                     <span class="icon bg-success"><i class="fa fa-plus-circle"></i></span>
                     <span class="title">Новая Торговая Точка</span>
                     <span class="desc"><a href="tochka.html">"Ларёк вокзал"</a> г. Калининград. Статус: <span
                                 class="biz-tt">Бизнес</span></span>
                     <span class="time">19.09.2019 в 10:30</span>
                 </li>
                 <li>
                     <span class="icon bg-success"><i class="fa fa-plus-circle"></i></span>
                     <span class="title">Новая Торговая Точка</span>
                     <span class="desc"><a href="tochka.html">"Ларёк вокзал"</a> г. Калининград. Статус: <span
                                 class="serv-tt">Сервис</span></span>
                     <span class="time">19.09.2019 в 10:30</span>
                 </li>
             </ul>-->
                </div>
            </div>
            <!-- /.box-content -->
        </div>
        <div class="col-md-6">
            <div class="box-content">
                <h4 class="box-title"><i class="ico fa fa-pie-chart"></i>Инструменты</h4>
                <div class="box-body">
                    <ul class="nav nav-pills nav-stacked">
                        <!-- <li>
                        <a href="#"><i class="fa fa-file-text"></i> Скрипт переписки в соцсетях
                            <span class="label-right bg-primary pull-right">новое</span>
                        </a>
                    </li> -->
                        <li>
                            <a href="https://comeback.plus/files/self_vc.psd" target="_blank"><i class="fa fa-download"></i> Шаблон визиток
                        </a>
                        </li>
                        <!--<li>
                        <a href="#"><i class="fa fa-download" target="_blank"></i> Презентация партнерской программы
                        </a>
                    </li>-->
                        <li>
                            <a href="https://comeback.plus/files/compred-web.pdf" target="_blank"><i class="fa fa-download"></i> Презентация для владельцев бизнеса (веб)
                        </a>
                        </li>
                        <li>
                            <a href="https://comeback.plus/files/compred.pdf" target="_blank"><i class="fa fa-download"></i> Презентация для владельцев бизнеса (для печати)
                        </a>
                        </li>
                        <li>
                            <a href="https://comeback.plus/files/about.pdf" target="_blank"><i class="fa fa-download"></i> О сервисе для бизнеса
                        </a>
                        </li>
                        <li>
                            <a href="https://comeback.plus/files/buklet_4_tt.pdf" target="_blank"><i class="fa fa-download"></i> Буклет для владельцев бизнеса
                        </a>
                        </li>
                        <li>
                            <a href="https://comeback.plus/files/stickers.pdf" target="_blank"><i class="fa fa-download"></i> Макеты наклеек для торговых точек
                        </a>
                        </li>
                        <li>
                            <a href="https://comeback.plus/files/manual_admin.pdf" target="_blank"><i class="fa fa-file-text"></i> Руководство пользователя для администраторов
                        </a>
                        </li>
                        <li>
                            <a href="https://comeback.plus/files/manual_cashier.pdf" target="_blank"><i class="fa fa-file-text"></i> Руководство пользователя для кассиров
                        </a>
                        </li>
                        <li>
                            <a href="https://comeback.plus/files/manual_full.pdf" target="_blank"><i class="fa fa-file-text"></i> Полное руководство для владельцев бизнеса
                        </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- /.box-content -->
        </div>
    </div>
    <script>
        $(document).ready(function() {
            (new ClipboardJS('.referal_link')).on('success', function(e) {
                alert('Скопировано!');
            });
        })
    </script>