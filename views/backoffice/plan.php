<?php

use app\models\User;

?>
<? if ($success) { ?>
    <div class="alert alert-success"><?= $success ?></div>
<? } ?>
<? if ($error) { ?>
    <div class="alert alert-danger"><?= $error ?></div>
<? } ?>
<div class="pricing-plan">
    <div class="pricing-table">
        <div class="col col-first">
            <div class="thead">
                <div class="center-v">Выбирайте свой вариант</div>
            </div>
            <div class="td">Новый пользователь приложения</div>
            <div class="td">Новый агент</div>
            <div class="td">Новый партнёр</div>
            <div class="td">Новая Торговая точка (План Старт)</div>
            <div class="td">Новая Торговая точка (План Бизнес)</div>
            <div class="td">Доход с оборота 1-й линии</div>
            <div class="td">Доход с оборота 2-й линии</div>
            <div class="td">Доход с оборота 3-й линии</div>
            <div class="td">Доход с оборота 4-й линии</div>
            <div class="td">Доход с оборота 5-й линии</div>
            <div class="td"></div>
        </div>
        <div class="col">
            <div class="thead bg-gray">
                <h4>Пользователь</h4>
                <div class="price">
                    <span class="number">0</span>
                    <span class="small_number">00</span>
                    <span class="time">руб.</span>
                </div>
            </div>
            <div class="td">1</div>
            <div class="td">300</div>
            <div class="td">1000</div>
            <div class="td">500</div>
            <div class="td"><i class="fa fa-times"></i></div>
            <div class="td"><i class="fa fa-times"></i></div>
            <div class="td"><i class="fa fa-times"></i></div>
            <div class="td"><i class="fa fa-times"></i></div>
            <div class="td"><i class="fa fa-times"></i></div>
            <div class="td"><i class="fa fa-times"></i></div>

            <div class="td"><a style="<?= ($user->user_type != User::USER_TYPE_DEFAULT ? "visibility:hidden" : "") ?>"
                               href="" class="btn-order btn-gray">АКТИВИРОВАНО</a></div>
        </div>
        <div class="col">
            <div class="thead bg-purple">
                <h4>Агент</h4>
                <div class="price">
                    <span class="number">30</span>
                    <span class="small_number">000</span>
                    <span class="time">руб</span>
                </div>
            </div>
            <div class="td">1</div>
            <div class="td">6 000</div>
            <div class="td">20 000</div>
            <div class="td"><i class="fa fa-times"></i></div>
            <div class="td">400 руб/мес</div>
            <div class="td"><i class="fa fa-times"></i></div>
            <div class="td"><i class="fa fa-times"></i></div>
            <div class="td"><i class="fa fa-times"></i></div>
            <div class="td"><i class="fa fa-times"></i></div>
            <div class="td"><i class="fa fa-times"></i></div>
            <? if ($user->user_type == User::USER_TYPE_DEFAULT && $user->user_type != User::USER_TYPE_PARTNER) { ?>
                <div class="td"><a href="/backoffice/finances/buy-agent" class="btn-order">АКТИВИРОВАТЬ</a></div>
            <? } else if ($user->user_type == User::USER_TYPE_AGENT) { ?>
                <div class="td"><a href="#" class="btn-order">АКТИВИРОВАНО</a></div>
            <? } ?>
        </div>
        <div class="col col-featured">
            <div class="thead bg-main-2">
                <h4>Партнёр</h4>
                <div class="price">
                    <span class="number">100</span>
                    <span class="small_number">000</span>
                    <span class="time">руб</span>
                </div>
            </div>
            <div class="td">1</div>
            <div class="td">9 000</div>
            <div class="td">30 000</div>
            <div class="td"><i class="fa fa-times"></i></div>
            <div class="td">600 руб/мес</div>
            <div class="td">10%</div>
            <div class="td">5%</div>
            <div class="td">2%</div>
            <div class="td">1%</div>
            <div class="td">0.5%</div>
            <? if (in_array($user->user_type, [User::USER_TYPE_AGENT, User::USER_TYPE_DEFAULT])) { ?>
                <div class="td"><a href="/backoffice/finances/buy-partner" class="btn-order">АКТИВИРОВАТЬ</a></div>
            <? } else if ($user->user_type == User::USER_TYPE_PARTNER) { ?>
                <div class="td"><a href="/backoffice/finances/buy-partner" class="btn-order">АКТИВИРОВАНО</a></div>
            <? } ?>
        </div>
    </div>
</div>