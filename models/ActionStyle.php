<?php

namespace app\models;

use app\models\params\ActionDefaultStyleParams;
use Yii;

/**
 * This is the model class for table "actions_styles".
 *
 * @property int $id
 * @property int $action_id
 * @property int $is_template
 * @property string $created_at
 * @property string $action_bg_color
 * @property string $action_text_color
 * @property string $logo_url
 * @property string $item_bg_color Фон не активной фишки
 * @property string $item_border_color Цвет обводки не активной фишки
 * @property string $active_item_bg_color Фон активной фишки
 * @property string $active_item_border_color Цвет обводки активной фишки
 * @property string $last_item_bg_color Цвет фона плюшки
 * @property string $last_item_border_color Цвет обводки плюшки
 * @property string $footer_backround_color Цвет фона футера карточки
 * @property string $footer_text_color Цвет текста футера
 */
class ActionStyle extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'actions_styles';
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['action_bg_color', 'default', 'value' => ActionDefaultStyleParams::ACTION_BG_COLOR],
            ['action_text_color', 'default', 'value' => ActionDefaultStyleParams::ACTION_TEXT_COLOR],
            ['item_bg_color', 'default', 'value' => ActionDefaultStyleParams::ITEM_BG_COLOR],
            ['item_text_color', 'default', 'value' => ActionDefaultStyleParams::ITEM_TEXT_COLOR],
            ['active_item_text_color', 'default', 'value' => ActionDefaultStyleParams::ACTIVE_ITEM_TEXT_COLOR],
            ['last_item_text_color', 'default', 'value' => ActionDefaultStyleParams::LAST_ITEM_TEXT_COLOR],
            ['item_border_color', 'default', 'value' => ActionDefaultStyleParams::ITEM_BORDER_COLOR],
            ['active_item_bg_color', 'default', 'value' => ActionDefaultStyleParams::ACTIVE_ITEM_BG_COLOR],
            ['active_item_border_color', 'default', 'value' => ActionDefaultStyleParams::ACTIVE_ITEM_BORDER_COLOR],
            ['last_item_bg_color', 'default', 'value' => ActionDefaultStyleParams::LAST_ITEM_BG_COLOR],
            ['last_item_border_color', 'default', 'value' => ActionDefaultStyleParams::LAST_ITEM_BORDER_COLOR],
            [['action_id', 'is_template'], 'integer'],
            [['created_at'], 'safe'],
            [['action_bg_color', 'action_text_color', 'item_bg_color', 'item_border_color', 'active_item_bg_color', 'active_item_border_color', 'last_item_bg_color', 'last_item_border_color', 'footer_bg_color', 'footer_text_color'], 'string', 'max' => 7],
            [['logo_url'], 'string', 'max' => 255],
        ];
    }

    public function loadDefaultStyles()
    {
        $this->attributes = [
            'action_bg_color' => ActionDefaultStyleParams::ACTION_BG_COLOR,
            'action_text_color' => ActionDefaultStyleParams::ACTION_TEXT_COLOR,
            'item_text_color' => ActionDefaultStyleParams::ACTION_TEXT_COLOR,
            'active_item_text_color' => ActionDefaultStyleParams::ACTION_TEXT_COLOR,
            'last_item_text_color' => ActionDefaultStyleParams::LAST_ITEM_TEXT_COLOR,
            'item_bg_color' => ActionDefaultStyleParams::ITEM_BG_COLOR,
            'item_border_color' => ActionDefaultStyleParams::ITEM_BORDER_COLOR,
            'active_item_bg_color' => ActionDefaultStyleParams::ACTIVE_ITEM_BG_COLOR,
            'active_item_border_color' => ActionDefaultStyleParams::ACTIVE_ITEM_BORDER_COLOR,
            'last_item_bg_color' => ActionDefaultStyleParams::LAST_ITEM_BG_COLOR,
            'last_item_border_color' => ActionDefaultStyleParams::LAST_ITEM_BORDER_COLOR,
            'footer_bg_color' => ActionDefaultStyleParams::FOOTER_BG_COLOR,
            'is_template' => 0,
            'footer_text_color' => ActionDefaultStyleParams::FOOTER_TEXT_COLOR
        ];
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'action_id' => 'Action ID',
            'is_template' => 'Is Template',
            'created_at' => 'Created At',
            'action_bg_color' => 'Action Bg Color',
            'action_text_color' => 'Action Text Color',
            'logo_url' => 'Logo Url',
            'item_bg_color' => 'Item Bg Color',
            'item_border_color' => 'Item Border Color',
            'active_item_bg_color' => 'Active Item Bg Color',
            'active_item_border_color' => 'Active Item Border Color',
            'last_item_bg_color' => 'Last Item Bg Color',
            'last_item_border_color' => 'Last Item Border Color',
            'footer_backround_color' => 'Footer Backround Color',
            'footer_text_color' => 'Footer Text Color',
        ];
    }

    /**
     * {@inheritdoc}
     * @return ActionsStylesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ActionsStylesQuery(get_called_class());
    }
}
