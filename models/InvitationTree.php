<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 27/09/2019
 * Time: 21:53
 */

namespace app\models;


use app\models\data\InvitationTreeItem;

class InvitationTree
{
    private $rootUser;
    private $deepLevel;

    public function __construct(User $rootUser)
    {
        $this->rootUser = $rootUser;
    }

    private function getChildsQuery(User $user)
    {
        $users = User::find()->select('u.*,(SELECT COUNT(*) FROM users WHERE referer=u.id) AS childs')->from("users u")->where(['u.referer' => $user->id]);
     //    echo $users->createCommand()->rawSql;
      //   die;
        return $users;
    }

    private function buildTree(User $rootUser)
    {
        $results = [];
        $childs = $this->getChildsQuery($rootUser)->all();
        foreach ($childs as $user) {
            if ($user->childs > 0) {
                $newItem = new InvitationTreeItem($user);
                $newItem->setChilds($this->buildTree($user));
                $results[] = $newItem;
                continue;
            }
            $results[] = new InvitationTreeItem($user);
        }
        return $results;
    }

    public function getTree()
    {
        $tree = $this->buildTree($this->rootUser);
        return $tree;
    }
}