<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 23/05/2019
 * Time: 21:16
 */

namespace app\models\SearchModel;


use app\models\Cashier;
use yii\data\ActiveDataProvider;

class CashierSearchModel extends Cashier
{

public $companyId;
public function setCompanyId(int $companyId) {
	$this->companyId = $companyId;
	return $this;
}
    public function getSearchQuery()
    {
        $query = Cashier::find();
		if(!empty($this->companyId)) {
			$query->where(['organization_id'=>$this->companyId]);
		}
        return $query;
    }

    public function getSearchProvider()
    {
        $provider = new ActiveDataProvider([
            'query' => $this->getSearchQuery(),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                    'name' => SORT_ASC,
                ]
            ],
        ]);
        return $provider;
    }
}