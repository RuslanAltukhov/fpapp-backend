<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 23/05/2019
 * Time: 21:01
 */

namespace app\models\SearchModel;


use app\models\Organization;
use app\models\User;
use yii\data\ActiveDataProvider;

class UsersSearchModel extends User
{

    private $organization;

    public function setOrganization(Organization $organization)
    {
        $this->organization = $organization;
        return $this;
    }

    public function getSearchQuery()
    {
        $query = User::find();
        $query->innerJoin('action_sales', "action_sales.user_id=users.id")
            ->innerJoin("sale_points", 'sale_points.id=action_sales.salepoint_id')
            ->groupBy("users.id")
            ->where(['sale_points.organization_id' => $this->organization->id]);
        return $query;
    }

    public function getSearchProvider()
    {
        $provider = new ActiveDataProvider([
            'query' => $this->getSearchQuery(),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                    'name' => SORT_ASC,
                ]
            ],
        ]);
        return $provider;
    }
}