<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 05/07/2019
 * Time: 09:10
 */

namespace app\models\SearchModel;


use app\models\Category;
use app\models\CategoryGroup;
use yii\base\Model;

class CategorySearch extends Model
{
    public $queryStr;

    public function rules()
    {
        return [
            [['queryStr'], 'string']
        ];
    }

    public function search()
    {
        $query = CategoryGroup::find()->with(['categories' => function ($q) {
            if (!empty($this->queryStr)) {
                $q->where(' categories.title LIKE :category_name')->addParams([':category_name' => $this->queryStr . "%"]);
            }
            return $q;
        }]);

        return $query;
    }
}