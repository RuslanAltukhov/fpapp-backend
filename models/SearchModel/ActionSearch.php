<?php

namespace app\models\SearchModel;

use app\models\Action;
use app\models\Location;
use yii\data\ActiveDataProvider;

/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 20/05/2019
 * Time: 21:25
 */
class   ActionSearch extends Action
{
    public $status_id = '';
    public $distanceKilometers;
    public $category;
    public $companyId;
    public $queryStr;
    public $lat;
    public $lng;

    public function rules()
    {
        return [
            [['status_id', 'queryStr', 'category'], 'safe'],
            [['distanceKilometers', 'companyId'], 'integer'],
            [['lat', 'lng'], 'number']
        ];
    }

    public function setCompanyId(int $companyId)
    {
        $this->companyId = $companyId;
        return $this;
    }

    private function isLocationExist()
    {
        return !empty($this->lat) && !empty($this->lng);
    }

    public function getSearchQuery()
    {
        $query = Action::find()->with("organization")->with(["salePoints" => function ($query) {
            $query->with("workingHours");
            return $query;
        }])->select("actions.*");

        $query->filterWhere(['actions.status_id' => $this->status_id]);
        if (!empty($this->companyId)) {
            $query->andWhere(['actions.organization_id' => $this->companyId]);
        }
        if (!empty($this->queryStr)) {
            $query->andWhere('(actions.name LIKE :query OR actions.description LIKE :query)')
                ->addParams([':query' => $this->queryStr . '%']);
        }
        if (!empty($this->category)) {
            $query->andWhere(['actions.category_id' => $this->category]);
        }
        if ($this->isLocationExist() && $this->distanceKilometers) {
            $query->addSelect(['sale_points.lat', 'sale_points.lng'])->innerJoin("salepoint_actions sa", 'sa.action_id=actions.id')
                ->innerJoin("sale_points", "sale_points.id=sa.sale_point_id")
                ->having("geodist(sale_points.lat,sale_points.lng,:lat,:lng) <= :distance")
                ->addParams([
                    ':lat' => $this->lat,
                    ':lng' => $this->lng,
                    ':distance' => $this->distanceKilometers
                ]);
        }
        return $query;
    }

    public function getSearchProvider()
    {
        $provider = new ActiveDataProvider([
            'query' => $this->getSearchQuery(),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                    'name' => SORT_ASC,
                ]
            ],
        ]);
        return $provider;
    }

    public function attributeLabels()
    {
        return [
            'status_id' => 'Статус'
        ];
    }
}