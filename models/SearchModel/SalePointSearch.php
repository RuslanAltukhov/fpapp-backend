<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 23/05/2019
 * Time: 21:37
 */

namespace app\models\SearchModel;


use app\models\SalePoint;
use yii\data\ActiveDataProvider;

class SalePointSearch extends SalePoint
{
    public $actionId;
    public $companyId;

    public function rules()
    {
        return [
            [['actionId'], 'integer']
        ];
    }

    public function setCompanyId(int $companyId)
    {
        $this->companyId = $companyId;
        return $this;
    }

    public function getSearchQuery()
    {
        $query = SalePoint::find()->onlyActive();
        if (!empty($this->companyId)) {
            $query->andWhere(['sale_points.organization_id' => $this->companyId]);
        }
        if ($this->actionId) {
            $query->innerJoin("salepoint_actions", "salepoint_actions.sale_point_id=sale_points.id")->where(['is_archived' => 0]);
            $query->andWhere(['salepoint_actions.action_id' => $this->actionId]);
        }

        return $query;
    }

    public function getSearchProvider()
    {
        $provider = new ActiveDataProvider([
            'query' => $this->getSearchQuery(),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                    'name' => SORT_ASC,
                ]
            ],
        ]);
        return $provider;
    }
}