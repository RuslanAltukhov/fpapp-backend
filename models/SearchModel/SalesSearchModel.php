<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 23/05/2019
 * Time: 22:18
 */

namespace app\models\SearchModel;


use app\models\Action;
use app\models\ActionSale;
use app\models\ActionSalesQuery;
use yii\data\ActiveDataProvider;

class SalesSearchModel extends ActionSale
{
    public $actionId;

    public function rules()
    {
        return [
            ['actionId', 'safe']
        ];
    }

    public function getSearchQuery()
    {
        $query = ActionSale::find();
        $query->filterWhere(['action_id' => $this->actionId]);
        return $query;
    }

    public function getSearchProvider()
    {
        $provider = new ActiveDataProvider([
            'query' => $this->getSearchQuery(),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,

                ]
            ],
        ]);
        return $provider;
    }
}