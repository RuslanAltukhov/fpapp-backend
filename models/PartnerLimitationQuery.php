<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PartnerLimitation]].
 *
 * @see PartnerLimitation
 */
class PartnerLimitationQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return PartnerLimitation[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return PartnerLimitation|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
