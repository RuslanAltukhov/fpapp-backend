<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "action_sales".
 *
 * @property int $id
 * @property int $action_id
 * @property int $user_id
 * @property string $created_at
 * @property int $type Фишка или плюшка
 */
class ActionSale extends \yii\db\ActiveRecord
{
    const TYPE_SALE = 0;
    const TYPE_GIFT = 1;
    public $count;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'action_sales';
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public function getAction()
    {
        return $this->hasOne(Action::class, ['id' => 'action_id']);
    }

    public function getSalePoint()
    {
        return $this->hasOne(SalePoint::class, ['id' => 'salepoint_id']);
    }

    public function getCashier()
    {
        return $this->hasOne(Cashier::class, ['id' => 'cashier_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['action_id', 'user_id', 'type', 'salepoint_id', 'cashier_id', 'lap'], 'integer'],
            [['created_at'], 'safe'],
        ];
    }

    public static function fromState(int $actionId, int $cashierId, int $salepointId, int $userId, int $lap)
    {
        $model = new static([
            'action_id' => $actionId,
            'cashier_id' => $cashierId,
            'lap' => $lap,
            'user_id' => $userId,
            'salepoint_id' => $salepointId
        ]);
        return $model;
    }

    public function setTypeGift()
    {
        $this->type = self::TYPE_GIFT;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'action_id' => 'Action ID',
            'user_id' => 'User ID',
            'action.name' => 'Акция',
            'user.name' => 'Пользователь',
            'created_at' => 'Дата покупки',
            'type' => 'Тип',
        ];
    }

    /**
     * {@inheritdoc}
     * @return ActionSalesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ActionSalesQuery(get_called_class());
    }
}
