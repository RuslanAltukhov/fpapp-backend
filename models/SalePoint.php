<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sale_points".
 *
 * @property int $id
 * @property int $organization_id
 * @property string $created_at
 * @property string $name
 * @property string $lat
 * @property string $lng
 */
class SalePoint extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sale_points';
    }

    public function getCashiers()
    {
        return $this->hasMany(Cashier::class, ['id' => 'cashier_id'])->viaTable('salepoint_cashiers', ['salepoint_id' => 'id']);
    }

    public function getActions()
    {
        return $this->hasMany(Action::class, ['id' => 'action_id'])->viaTable('salepoint_actions', ['sale_point_id' => 'id']);
    }

    public function getSales()
    {
        return $this->hasMany(ActionSale::class, ['salepoint_id' => 'id']);
    }

    public function getLastSaleDate()
    {
        return $this->getSales()->orderBy("created_at DESC")->one()['created_at'] ?? null;
    }

    public function getTotalSalesCount()
    {
        return $this->getSales()->count();
    }

    public function getGiftsCount()
    {
        return $this->getSales()->where(['type' => ActionSale::TYPE_GIFT])->count();
    }

    public function getWorkingHours()
    {
        return $this->hasMany(SalePointWorkingHour::class, ['sale_point_id' => 'id']);
    }


    public function markAsArchived()
    {
        $this->is_archived = 1;
        if (!$this->save()) {
            throw new \Exception("не удается архивировать точку");
        }
    }

    public function getSoldItemsCount()
    {
        return $this->getSales()->where(['type' => ActionSale::TYPE_SALE])->count();
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['organization_id', 'is_archived'], 'integer'],
            [['address', 'address_custom'], 'string'],
            [['created_at'], 'safe'],
            [['lat', 'lng'], 'number'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'organization_id' => 'Organization ID',
            'created_at' => 'Дата создания',
            'name' => 'Название',
            'address' => 'Адрес',
            'lat' => 'Lat',
            'lng' => 'Lng',
        ];
    }

    /**
     * {@inheritdoc}
     * @return SalePointsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SalePointsQuery(get_called_class());
    }
}
