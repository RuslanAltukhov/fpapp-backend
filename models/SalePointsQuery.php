<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[SalePoint]].
 *
 * @see SalePoint
 */
class SalePointsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return SalePoint[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    public function onlyActive()
    {
        return $this->andWhere(['is_archived' => 0]);
    }

    /**
     * {@inheritdoc}
     * @return SalePoint|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
