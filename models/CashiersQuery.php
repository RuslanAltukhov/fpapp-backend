<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Cashier]].
 *
 * @see Cashier
 */
class CashiersQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Cashier[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Cashier|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
