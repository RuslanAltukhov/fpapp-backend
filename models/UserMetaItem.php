<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_meta".
 *
 * @property int $id
 * @property int $user_id
 * @property boolean $show_contacts
 * @property string $vk_link
 * @property string $fb_link
 * @property string $ok_link
 * @property string $mm_link
 * @property string $instagram_link
 */
class UserMetaItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_meta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'show_contacts'], 'integer'],
            ['show_contacts', 'default', 'value' => 0],
            ['about', 'string'],
            [['vk_link', 'fb_link', 'ok_link', 'mm_link', 'instagram_link'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'vk_link' => 'Vk Link',
            'fb_link' => 'Fb Link',
            'ok_link' => 'Ok Link',
            'mm_link' => 'Mm Link',
            'instagram_link' => 'Instagram Link',
        ];
    }

    public function canShowContacts(): bool
    {
        return $this->show_contacts == 1;
    }

    public static function getByUserId(int $uid)
    {
        if ($meta = static::findOne(['user_id' => $uid])) {
            return $meta;
        }

        $meta = new static(['user_id' => $uid]);
        $meta->loadDefaultValues();
        return $meta;
    }

    /**
     * {@inheritdoc}
     * @return UserMetaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserMetaQuery(get_called_class());
    }
}
