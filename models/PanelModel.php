<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 29/09/2019
 * Time: 21:33
 */

namespace app\models;


use app\classes\ReferalsHistoryComposer;
use app\classes\ReferalStatReader;
use yii\base\Model;
use yii\helpers\Url;

class PanelModel extends Model
{
    private $user;
    private $referalStat;
    private $historyComposer;

    public function __construct(User $user)
    {
        $this->user = $user;
        $this->referalStat = new ReferalStatReader($user);
        $this->historyComposer = new ReferalsHistoryComposer($this->user);

    }

    public function getReferalCode()
    {
        return $this->user->getId();
    }

    public function getSiteReferalLink()
    {
        return Url::base(true) . "/site/" . $this->user->getId();
    }

    public function getRegistrationReferalLink()
    {
        return Url::base(true) . "/" . $this->user->getId();
    }

    public function getWebinarLink()
    {
        return Url::base(true) . "/webinar/" . $this->user->getId();
    }

    public function getVisits()
    {
        return $this->referalStat->getVisitsCount();
    }

    public function getHistory()
    {
        return $this->historyComposer->composeActivityHistory();
    }

    public function getRegistrations()
    {
        return $this->referalStat->getRegistrationsCount();
    }

    public function getConversion()
    {
        return $this->referalStat->getConversion();
    }

    public function getBalance()
    {
        return $this->user->balance;
    }
}