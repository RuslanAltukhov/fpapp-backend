<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "push_tokens".
 *
 * @property int $id
 * @property string $token
 * @property string $created_at
 * @property int $user_id
 */
class PushToken extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'push_tokens';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['token'], 'string'],
            [['created_at'], 'safe'],
            [['user_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'token' => 'Token',
            'created_at' => 'Created At',
            'user_id' => 'User ID',
        ];
    }

    /**
     * {@inheritdoc}
     * @return PushTokensQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PushTokensQuery(get_called_class());
    }
}
