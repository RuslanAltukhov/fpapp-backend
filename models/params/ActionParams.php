<?php

namespace app\models\params;
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 19/05/2019
 * Time: 10:51
 */

class ActionParams
{
    const STATUS_NEW = 1;
    const STATUS_ACTIVE = 2;
    const STATUS_CLOSED = 3;
    const STATUS_REJECTED = 4;
    const MIN_NEED_TO_BUY = 2;
    const MAX_NEED_TO_BUY = 20;
    private static $statusNames = [
        self::STATUS_NEW => "Ожидает проверки",
        self::STATUS_ACTIVE => "Активная",
        self::STATUS_REJECTED => "Отклонена модератором",
        self::STATUS_CLOSED => "В архиве"
    ];
    public static function getAllStatusNames()
    {
        return self::$statusNames;
    }

    public static function getStatusNameById(int $statusId)
    {
        return self::$statusNames[$statusId] ?? '';
    }

    public static function getNeedToBuyRange()
    {
        $params = [];
        for ($i = self::MIN_NEED_TO_BUY; $i <= self::MAX_NEED_TO_BUY; $i++) {
            $params[$i] = $i;
        }
        return $params;
    }
}