<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 19/05/2019
 * Time: 13:41
 */

namespace app\models\params;


class ActionDefaultStyleParams
{
    const DEFAULT_ITEMS_COUNT = 5;

    const ACTION_BG_COLOR = "#540d6e";
    const ACTION_TEXT_COLOR = "#ffffff";
    const ITEM_BG_COLOR = "#540d6e";
    const ITEM_TEXT_COLOR = "#ffffff";
    const ITEM_BORDER_COLOR = "#ffffff";
    const ACTIVE_ITEM_BG_COLOR = "#ee4264";
    const ACTIVE_ITEM_TEXT_COLOR = "#540d6e";
    const ACTIVE_ITEM_BORDER_COLOR = "#540d6e";
    const LAST_ITEM_BG_COLOR = "#516077";
    const LAST_ITEM_TEXT_COLOR = "#540d6e";
    const LAST_ITEM_BORDER_COLOR = "#540d6e";
    const FOOTER_BG_COLOR = "#ffd23f";
    const FOOTER_TEXT_COLOR = "#540d6e";
}