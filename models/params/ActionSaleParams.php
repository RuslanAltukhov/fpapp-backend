<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 23/05/2019
 * Time: 21:55
 */

namespace app\models\params;


class ActionSaleParams
{
    const ITEM_TYPE_SALE = 0;
    const ITEM_TYPE_GIFT = 1;
    private static $typeNames = [
        self::ITEM_TYPE_GIFT => "Плюшка",
        self::ITEM_TYPE_SALE => "Фишка"
    ];

    public static function getItemTypeNameById(int $typeId)
    {
        return self::$typeNames[$typeId] ?? '';
    }
}