<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 28/05/2019
 * Time: 19:45
 */

namespace app\models\params;


use app\models\Cashier;
use app\models\SalePoint;

class SessionData
{
    private $salePointId;

    public function setSalePointId(int $salePointId)
    {
        $this->salePointId = $salePointId;
    }

    public function getSalePoint()
    {
        if (!$this->salePointId) {
            return null;
        }
        return SalePoint::findOne($this->salePointId);
    }
}