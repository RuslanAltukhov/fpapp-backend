<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 23/05/2019
 * Time: 21:25
 */

namespace app\models\params;


class CashierParams
{
    const STATUS_ACTIVE = 0;
    const STATUS_ARCHIVED = 1;
    private static $statusNames = [
        self::STATUS_ACTIVE => "Активный",
        self::STATUS_ARCHIVED => "В архиве"
    ];

    public static function getStatusNameById(int $statusId)
    {
        return self::$statusNames[$statusId] ?? '';
    }
}