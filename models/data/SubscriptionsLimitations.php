<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 15/10/2019
 * Time: 14:33
 */

namespace app\models\data;


class SubscriptionsLimitations
{
    public $salePoints = 0;
    public $actions = 0;
    public $cashiersCount = 0;

    public function __construct($salePoints, $actions, $cashiersCount)
    {
        $this->salePoints = $salePoints;
        $this->actions = $actions;
        $this->cashiersCount = $cashiersCount;
    }

    public static function getDefault()
    {
        return new self(1, 1, 1);
    }
}