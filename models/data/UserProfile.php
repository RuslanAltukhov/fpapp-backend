<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 17/09/2019
 * Time: 23:15
 */

namespace app\models\data;


use app\models\User;
use yii\helpers\Url;

class UserProfile
{
    public $phone;
    public $code;
    public $email;
    public $userType;
    public $sex;
    public $points;
    public $referer;
    public $hasReferer;
    public $notifications;
    public $profilePhoto;
    public $referalLink;
    public $id;

    public function __construct(User $user)
    {
        $this->phone = $user->phone;
        $this->code = $user->code;
        $this->userType = $user->user_type;
        $this->id = $user->getId();
        $this->name = $user->name;
        $this->referer = $user->referer;
        $this->hasReferer = $user->hasReferer();
        $this->points = $user->balance;
        $this->sex = $user->sex;
        $this->notifications = $user->notifications;
        $this->email = $user->email;
        $this->profilePhoto = $this->getProfilePhotoUrl($user);
        $this->referalLink = $this->generateReferalLink($user);

    }

    private function getProfilePhotoUrl(User $user)
    {
        if (empty($user->profile_photo)) {
            return;
        }
        return Url::base(true) . DIRECTORY_SEPARATOR . $user->profile_photo;
    }

    private function generateReferalLink(User $user)
    {
        return "https://cbp.su/" . $user->getId();
    }
}