<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 12/10/2019
 * Time: 13:10
 */

namespace app\models\data;


use app\models\User;
use app\models\UserMetaItem;

class UserStructureItem
{
    public $level;
    public $user;
    public $feeSum;
    public $referalsCount;
    private $feeTransactions;
    public $phone;

    public function __construct(int $level, int $referalsCount, User $user)
    {
        $this->level = $level;
        $this->user = $user;
        $this->referalsCount = $referalsCount;
        $userMeta = UserMetaItem::getByUserId($user->id);
        $this->phone = $userMeta->canShowContacts() ? $user->phone : "скрыто пользователем";
    }

    public function getReferalsCount()
    {
        return $this->referalsCount;
    }

    public function setReferalsCount($referalsCount)
    {
        $this->referalsCount = $referalsCount;
    }

    public function getRefererUser()
    {
        return $this->user->refererUser;
    }

    public function addFeeTransactions(array $transactions)
    {
        $this->feeTransactions = $transactions;
    }

    public function getFeeSum()
    {
        return $this->feeSum;
    }

    public function setFeeSum($feeSum)
    {
        $this->feeSum = $feeSum;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getFeeTransactions()
    {
        return $this->feeTransactions;
    }

    public function getLevel()
    {
        return $this->level;
    }
}