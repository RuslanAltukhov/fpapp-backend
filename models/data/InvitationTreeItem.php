<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 27/09/2019
 * Time: 21:54
 */

namespace app\models\data;


use app\models\User;

class InvitationTreeItem
{
    public $childs = [];
    public $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function addChild(User $user)
    {
        $this->childs[] = $user;
    }

    public function hasChilds()
    {
        return count($this->childs) > 0;
    }

    public function setChilds(array $users)
    {
        $this->childs = $users;
    }
}