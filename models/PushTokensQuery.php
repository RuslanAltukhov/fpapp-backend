<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PushToken]].
 *
 * @see PushToken
 */
class PushTokensQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return PushToken[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return PushToken|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
