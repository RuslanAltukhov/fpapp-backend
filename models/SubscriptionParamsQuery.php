<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[SubscriptionParams]].
 *
 * @see SubscriptionParams
 */
class SubscriptionParamsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return SubscriptionParams[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    public function onlyActive()
    {
        return $this->andWhere(['>=', 'expire_at', time()]);
    }

    public function forOrganization(int $organizationId)
    {
        return $this->andWhere(['organization_id' => $organizationId]);
    }

    /**
     * {@inheritdoc}
     * @return SubscriptionParams|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
