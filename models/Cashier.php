<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cashiers".
 *
 * @property int $id
 * @property int $sale_point_id
 * @property string $name
 * @property string $user_id
 * @property string $created_at
 * @property string $password
 * @property int $is_archived
 */
class Cashier extends \yii\db\ActiveRecord
{
    public $sales_count;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cashiers';
    }

    public function getSalePoints()
    {
        return $this->hasMany(SalePoint::class, ['id' => 'salepoint_id'])->viaTable('salepoint_cashiers', ['cashier_id' => 'id']);;
    }

    public function getSales()
    {
        return $this->hasMany(ActionSale::class, ['cashier_id' => 'id']);
    }

    public function getLastSaleDate()
    {
        return $this->getSales()->orderBy("created_at DESC")->one()['created_at'] ?? null;
    }

    public function getTotalSalesCount()
    {
        return $this->getSales()->count();
    }

    public function getOrganization()
    {
        return $this->hasOne(Organization::class, ['id' => 'organization_id']);
    }

    public function getGiftsCount()
    {
        return $this->getSales()->where(['type' => ActionSale::TYPE_GIFT])->count();
    }

    public function getSoldItemsCount()
    {
        return $this->getSales()->where(['type' => ActionSale::TYPE_SALE])->count();
    }

    public function validatePassword(string $password)
    {
        return $this->password == md5($password);
    }

    public static function getPasswordEncoded(string $password)
    {
        return md5($password);
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['organization_id', 'is_archived', 'user_id'], 'integer'],
            [['created_at'], 'safe'],
            [['name', 'password'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sale_point_id' => 'Sale Point ID',
            'name' => 'Name',
            'created_at' => 'Дата создания',
            'is_archived' => 'Is Archived',
        ];
    }

    /**
     * {@inheritdoc}
     * @return CashiersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CashiersQuery(get_called_class());
    }
}
