<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "category_groups".
 *
 * @property int $id
 * @property string $group_name
 */
class CategoryGroup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category_groups';
    }

    public function getCategories()
    {
        return $this->hasMany(Category::class, ['group_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['group_name'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_name' => 'Group Name',
        ];
    }

    /**
     * {@inheritdoc}
     * @return CategoryGroupsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CategoryGroupsQuery(get_called_class());
    }
}
