<?php

namespace app\models;

use app\models\params\ActionParams;

/**
 * This is the ActiveQuery class for [[Action]].
 *
 * @see Action
 */
class ActionsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Action[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    public function onlyActive()
    {
        return $this->andWhere(['status_id' => ActionParams::STATUS_ACTIVE]);
    }

    public function onlyWithSalePoints()
    {
        return $this->innerJoin("salepoint_actions", "salepoint_actions.action_id=actions.id");
    }

    /**
     * {@inheritdoc}
     * @return Action|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
