<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ReferalStatItem]].
 *
 * @see ReferalStatItem
 */
class ReferalStatQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return ReferalStatItem[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return ReferalStatItem|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
