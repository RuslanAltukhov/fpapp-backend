<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 17/10/2019
 * Time: 23:05
 */

namespace app\models\robokassa;


use app\classes\Exception\PaymentException;
use app\classes\paymentsystem\Robokassa;
use app\classes\TransactionCreator;
use app\models\Transaction;
use yii\base\Model;

class PaymentAcceptor extends Model
{
    const TYPE_RESULT_URL = 1;
    const TYPE_SUCCESS_URL = 2;
    public $OutSum;
    public $InvId;
    public $SignatureValue;
    private $robokassa;
    private $urlType;
    /**
     * @var Transaction
     */
    private $transaction;

    public function __construct(int $urlType)
    {
        $this->urlType = $urlType;
        $this->robokassa = new Robokassa();
        parent::__construct();
    }

    public function rules()
    {
        return [
            [['OutSum', 'InvId', 'SignatureValue'], 'required'],
            ['InvId', 'validateInvoice'],
            ['SignatureValue', 'validateSignature']
        ];
    }

    public function validateInvoice()
    {
        $this->transaction = Transaction::findOne($this->InvId);
        if (!$this->transaction) {
            throw new PaymentException(sprintf("Транзакция с номером %d не найдена", $this->InvId));
        }
    }

    public function validateSignature()
    {
        if (!($this->urlType == self::TYPE_SUCCESS_URL ?
            $this->robokassa->validateSuccessSignature($this->transaction, $this->SignatureValue)
            : $this->robokassa->validateResultSignature($this->transaction, $this->SignatureValue))) {
            throw new PaymentException(sprintf("Не удалось валидировать сигнатуру платежа", $this->SignatureValue));
        }
    }

    public function accept()
    {
        $transactionCreator = new TransactionCreator($this->transaction->dstUser);
        $transactionCreator->confirmIncomeTransaction($this->transaction);
        return true;
    }
}