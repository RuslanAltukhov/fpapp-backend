<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[UserMetaItem]].
 *
 * @see UserMetaItem
 */
class UserMetaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return UserMetaItem[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return UserMetaItem|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
