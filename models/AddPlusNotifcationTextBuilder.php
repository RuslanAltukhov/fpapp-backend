<?php
/**
 * Класс для построение тексто.
 * User: ruslan
 * Date: 04/08/2019
 * Time: 15:01
 */

namespace app\models;


use Kreait\Firebase\Messaging\Notification;

class AddPlusNotifcationTextBuilder
{
    private $items = [];

    public function addItem(ActionSale $item)
    {
        $this->items[] = $item;
    }

    private function getSalesCount()
    {
        return count(array_filter($this->items, function ($item) {
            return $item->type == ActionSale::TYPE_SALE;
        }));
    }

    private function getGiftsCount()
    {
        return count(array_filter($this->items, function ($item) {
            return $item->type == ActionSale::TYPE_GIFT;
        }));
    }

    public function build()
    {
        $text = sprintf("Получено плюсов:%d, получено бонусов:%d", $this->getSalesCount(), $this->getGiftsCount());
        $title = "Comeback plus";
        return Notification::create($title, $text,"ic_notification");
    }
}