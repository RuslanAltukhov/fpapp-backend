<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "partner_limitation".
 *
 * @property int $id
 * @property int $user_id
 * @property int $agent_packets
 * @property int $used_agent_packets
 */
class PartnerLimitation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'partner_limitation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'agent_packets', 'used_agent_packets'], 'integer'],
        ];
    }

    public static function getByUserId(int $userId): PartnerLimitation
    {
        return static::findOne(['user_id' => $userId]) ?? new static(['user_id' => $userId]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'agent_packets' => 'Agent Packets',
            'used_agent_packets' => 'Used Agent Packets',
        ];
    }

    /**
     * {@inheritdoc}
     * @return PartnerLimitationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PartnerLimitationQuery(get_called_class());
    }
}
