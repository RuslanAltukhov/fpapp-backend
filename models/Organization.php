<?php

namespace app\models;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "organizations".
 *
 * @property int $id
 * @property string $name
 * @property string $created_at
 * @property int $owner_uid
 * @property int $mentor_id
 * @property int $city_id
 * @property User $owner
 * @property int $category_id
 */
class Organization extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'organizations';
    }

    public function getActions()
    {
        return $this->hasMany(Action::class, ['organization_id' => 'id']);
    }

    public function getCashiers()
    {
        return $this->hasMany(Cashier::class, ['organization_id' => 'id']);
    }

    public function getOwner()
    {
        return $this->hasOne(User::class, ['id' => 'owner_uid']);
    }

    public function hasMentor()
    {
        return $this->mentor_id > 0;
    }

    public function getMentor()
    {
        return $this->hasOne(User::class, ['id' => 'mentor_id']);
    }

    public static function createEmptyForUser(User $user)
    {
        $model = new static([
            'owner_uid' => $user->getId()
        ]);
        if (!$model->save()) {
            throw new \Exception("не удается создать организацию");
        }
        return $model;
    }

    public function getSalePoints()
    {
        return $this->hasMany(SalePoint::class, ['organization_id' => 'id']);
    }

    public function isFull()
    {
        $attributes = ['name', 'contact_person', 'contact_person_phone', 'contact_person_employment', 'contact_email'];
        foreach ($attributes as $attribute) {
            if (empty($this->{$attribute})) {
                return false;
            }
        }
        return true;
    }

    public function getLogoPath()
    {
        return Url::base() . "/" . $this->logo_path;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at'], 'safe'],
            [['owner_uid', 'city_id', 'category_id', 'mentor_id'], 'integer'],
            [['name', 'contact_person', 'contact_person_phone', 'contact_person_employment', 'contact_email'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'created_at' => 'Created At',
            'owner_uid' => 'Owner Uid',
            'city_id' => 'City ID',
            'category_id' => 'Category ID',
        ];
    }

    /**
     * {@inheritdoc}
     * @return OrganizationsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrganizationsQuery(get_called_class());
    }
}
