<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transactions".
 *
 * @property int $id
 * @property int $user_id
 * @property int $created_at
 * @property int $processed_at
 * @property string $sum
 * @property int $status
 * @property string $data
 * @property int $type
 * @property  int $source_user_id
 */
class Transaction extends \yii\db\ActiveRecord
{
    const DIRECTION_INCOME = 1;
    const DIRECTION_OUTGONE = 2;


    const TYPE_REGISTRATION_FEE = 1;
    const TYPE_AGENT_SUBSCRIPTION_FEE = 2;
    const TYPE_PARTNER_SUBSCRIPTION_FEE = 3;
    const TYPE_PARTNER_LEVEL_FEE = 4;
    const TYPE_PARTNER_SUBSCRIPTION = 5; //покупка партнерского пакета
    const TYPE_AGENT_SUBSCRIPTION = 6; //покупка агентского пакета
    const TYPE_BALANCE_CHARGE = 7;//Пополнение баланса
    const TYPE_SUBSCRIPTION_BUY = 8; // Покупка подписки
    const TYPE_ORG_SUBSCRIPTION_FEE = 9;// Комиссия за покупку подписки организацией
    const STATUS_NEW = 1;
    const STATUS_DONE = 3;
    const STATUS_IN_PROGRESS = 2;

    public static $transactionTypeNames = [
        self::TYPE_REGISTRATION_FEE => 'Вознаграждение за регистрацию пользователя',
        self::TYPE_ORG_SUBSCRIPTION_FEE => 'Вознаграждение за покупку подписки организацией',
        self::TYPE_AGENT_SUBSCRIPTION_FEE => 'Вознаграждение за покупку агентского пакета рефералом',
        self::TYPE_PARTNER_LEVEL_FEE => 'Вознаграждение от структуры',
        self::TYPE_PARTNER_SUBSCRIPTION_FEE => 'Вознаграждение за покупку партнерского пакета рефералом',
        self::TYPE_PARTNER_SUBSCRIPTION => 'Покупка партнерского пакета',
        self::TYPE_AGENT_SUBSCRIPTION => 'Покупка агентского пакета',
        self::TYPE_BALANCE_CHARGE => 'Пополнение баланса',
        self::TYPE_SUBSCRIPTION_BUY => 'Покупка подписки',
        self::TYPE_ORG_SUBSCRIPTION_FEE => 'Вознаграждение за покупку рефералом подписки для организации'
    ];
    public static $statusNames = [
        self::STATUS_NEW => 'Новая',
        self::STATUS_DONE => 'Выполнена',
        self::STATUS_IN_PROGRESS => 'Выполняется',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transactions';
    }

    public function isDone()
    {
        return $this->status == self::STATUS_DONE;
    }

    public function getSourceUser()
    {
        return $this->hasOne(User::class, ['id' => 'source_user_id']);
    }

    public function getDstUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'created_at', 'processed_at', 'status', 'type', 'direction'], 'integer'],
            ['data', 'string'],
            [['sum'], 'number'],
        ];
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->created_at = time();
        }
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function markAsDone()
    {
        $this->status = self::STATUS_DONE;
        $this->processed_at = time();
        if (!$this->save()) {
            throw new \Exception(sprintf("Can't mark transaction with id %d as DONE", $this->id));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'created_at' => 'Дата',
            'processed_at' => 'Processed At',
            'sum' => 'Сумма',
            'status' => 'Cтатус',
            'type' => 'Тип',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TransactionsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TransactionsQuery(get_called_class());
    }
}
