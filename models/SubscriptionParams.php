<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "subscription_params".
 *
 * @property int $id
 * @property int $actions_count
 * @property int $salepoints_count
 * @property int $subscription_id
 * @property int $expire_at
 * @property int $cashiers_count
 * @property int $organization_id
 */
class SubscriptionParams extends \yii\db\ActiveRecord
{
    const BUSINESS_DEFAULT_ACTIONS_COUNT = 5;
    const BUSINESS_DEFAULT_CASHIERS_COUNT = 5;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subscription_params';
    }

    public static function getDefaultParams()
    {
        $model = new static();
        $model->loadDefaultValues();
        return $model;
    }

    public function loadDefaultBusinessParams()
    {
        $this->actions_count = self::BUSINESS_DEFAULT_ACTIONS_COUNT;
        $this->cashiers_count = self::BUSINESS_DEFAULT_CASHIERS_COUNT;
    }

    public static function getSubscriptionParamsForOrg(int $orgId)
    {
        return static::findOne(['organization_id' => $orgId]) ?? self::getDefaultParams();
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['actions_count', 'salepoints_count', 'cashiers_count', 'organization_id', 'expire_at', 'subscription_id'], 'integer'],
            [['actions_count', 'salepoints_count', 'cashiers_count'], 'default', 'value' => 1],
            ['subscription_id', 'required'],
            [['expire_at'], 'default', 'value' => 0]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'actions_count' => 'Actions Count',
            'salepoints_count' => 'Salepoints Count',
            'cashiers_count' => 'Cashiers Count',
            'organization_id' => 'Organization ID',
        ];
    }

    /**
     * {@inheritdoc}
     * @return SubscriptionParamsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SubscriptionParamsQuery(get_called_class());
    }
}
