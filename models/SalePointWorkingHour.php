<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sale_point_working_hours".
 *
 * @property int $id
 * @property int $sale_point_id
 * @property string $created_at
 * @property int $day_of_week
 * @property string $open
 * @property string $close
 */
class SalePointWorkingHour extends \yii\db\ActiveRecord
{
    const MONDAY = 1;
    const TUESDAY = 2;
    const WEDNESDAY = 3;
    const THURSDAY = 4;
    const FRIDAY = 5;
    const SATURDAY = 6;
    const SUNDAY = 0;

    public $namesOfDay = [
        self::MONDAY => 'Понедельник',
        self::TUESDAY => 'Вторник',
        self::WEDNESDAY => 'Среда',
        self::THURSDAY => 'Четверг',
        self::FRIDAY => 'Пятница',
        self::SATURDAY => 'Суббота',
        self::SUNDAY => 'Воскресенье'
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sale_point_working_hours';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sale_point_id', 'day_of_week', 'every_time'], 'integer'],
            [['created_at', 'open', 'close'], 'safe'],
            [['day_of_week'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sale_point_id' => 'Sale Point ID',
            'created_at' => 'Created At',
            'day_of_week' => 'Day Of Week',
            'open' => 'Open',
            'close' => 'Close',
        ];
    }

    /**
     * {@inheritdoc}
     * @return SalePointWorkingHoursQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SalePointWorkingHoursQuery(get_called_class());
    }
}
