<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "referal_stat".
 *
 * @property int $id
 * @property int $user_id
 * @property int $action_type
 * @property string $action_date
 */
class ReferalStatItem extends \yii\db\ActiveRecord
{
    const ACTION_TYPE_REGISTRATION = 1;
    const ACTION_TYPE_VISIT = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'referal_stat';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'action_type'], 'integer'],
            [['action_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'action_type' => 'Action Type',
            'action_date' => 'Action Date',
        ];
    }

    /**
     * {@inheritdoc}
     * @return ReferalStatQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ReferalStatQuery(get_called_class());
    }
}
