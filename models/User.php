<?php

namespace app\models;

use app\classes\PhoneNumberFormatter;
use libphonenumber\NumberParseException;
use yii\db\ActiveRecord;

class User extends ActiveRecord implements \yii\web\IdentityInterface
{

    public $authKey;
    public $childs;
    public $bought_count;
    const SEX_MAN = 1;
    const SEX_WOMAN = 2;
    const USER_TYPE_DEFAULT = 1;
    const USER_TYPE_AGENT = 2;
    const USER_TYPE_PARTNER = 3;
    static $userTypeLabels = [
        self::USER_TYPE_DEFAULT => 'Пользователь',
        self::USER_TYPE_AGENT => 'Агент',
        self::USER_TYPE_PARTNER => 'Партнер',
    ];

    public static function tableName()
    {
        return "users";
    }

    public function rules()
    {
        return [
            [['phone', 'password', 'name', 'code', 'email', 'surname', 'profile_photo'], 'string'],
            [['balance'], 'number'],
            [['notifications', 'sex', 'points', 'referer', 'user_type'], 'integer']
        ];
    }

    public function getStatusName()
    {
        return self::$userTypeLabels[$this->user_type] ?? '';
    }

    public function getOrganization()
    {
        return $this->hasOne(Organization::class, ['owner_uid' => 'id']);
    }

    public function isAgent()
    {
        return $this->user_type == self::USER_TYPE_AGENT;
    }

    public function isPartner(): bool
    {
        return $this->user_type == self::USER_TYPE_PARTNER;
    }

    public function setRefererIfNotExist(int $refererId)
    {
        if (empty($this->referer_organization)) {
            $this->referer_organization = $refererId;
        }
        return $this;
    }

    public function getCreateDateFormatted()
    {
        return date("d.m.Y в H:i", strtotime($this->created_at));
    }

    public function hasReferer()
    {
        return $this->referer > 0;
    }

    public function increaseBalance($sum)
    {
        $this->balance += $sum;
        if (!$this->save()) {
            throw new \Exception(sprintf("Can't increase balance for user %d for sum %f", $this->id, $sum));
        }
    }


    public function getNameFormatted()
    {
        return $this->name ?? 'Имя не указано';
    }

    public function hasEnoughtMoney($sum)
    {
        return $this->balance >= $sum;
    }

    public function decreaseBalance($sum)
    {
        $this->balance -= $sum;
        if (!$this->save()) {
            throw new \Exception(sprintf("Can't decrease user balance to %d for user %d", $sum, $this->getId()));
        }
    }

    public function getRefererUser()
    {
        return $this->hasOne(User::class, ['id' => 'referer']);
    }

    public static function getPasswordEncoded($password)
    {
        return md5($password);
    }

    public function getActions()
    {
        return $this->hasMany(Action::class, ['id' => 'action_id'])->viaTable('action_sales', ['user_id' => 'id']);
    }

    public function getAllBoughtsAndGifst()
    {
        return $this->hasMany(ActionSale::class, ['user_id' => 'id']);

    }

    public function getPushTokens()
    {
        return $this->hasMany(PushToken::class, ['user_id' => 'id']);
    }

    public function getGiftsCount(Organization $organization)
    {
        return $this->getAllBoughtsAndGifst()
            ->where(['type' => ActionSale::TYPE_GIFT])
            ->innerJoin("sale_points", "sale_points.organization_id={$organization->id} AND sale_points.id=action_sales.salepoint_id")->count();
    }

    public function getBoughtsCount(Organization $organization)
    {
        return $this->getAllBoughtsAndGifst()
            ->where(['type' => ActionSale::TYPE_SALE])
            ->innerJoin("sale_points", "sale_points.organization_id={$organization->id} AND sale_points.id=action_sales.salepoint_id")->count();
    }

    public function getLastBoughtDate()
    {
        return $this->getAllBoughtsAndGifst()->orderBy("created_at DESC")->one()->created_at ?? null;
    }

    public function getMentor()
    {
        return User::find()->innerJoin('partner_structure ps', 'ps.mentor_id=users.id')->where("ps.user_id={$this->getId()}")->one();
    }

    public function getFirstBought()
    {
        return $this->getAllBoughtsAndGifst()->orderBy("created_at ASC")->one();
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public function getAuthTokens()
    {
        return $this->hasMany(AuthToken::class, ['user_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        if ($token = AuthToken::getByToken($token)) {
            return $token->user;
        }
        return null;
    }

    public static function findByPhone($phone)
    {
        try {
            $phoneFormatter = new PhoneNumberFormatter();
            $phone = $phoneFormatter->getFormattedNumber($phone);
            return self::findOne(['phone' => $phone]);
        } catch (NumberParseException $e) {
            return null;
        }
    }

    public static function findByPhoneOrId($idOrPhone)
    {
        if (!$idOrPhone) {
            return;
        }
        return static::findOne($idOrPhone) ?? self::findByPhone($idOrPhone);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        if ($model = static::findOne(['phone' => $username])) {
            return $model;
        }
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public
    function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public
    function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public
    function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public
    function validatePassword($password)
    {
        return $this->password === self::getPasswordEncoded($password);
    }

    public
    function attributeLabels()
    {
        return [
            'name' => ' Имя',
        ];
    }

    public
    static function find()
    {
        return new UsersQuery(get_called_class());
    }
}
