<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 02/10/2019
 * Time: 22:28
 */

namespace app\models\backoffice;


use app\models\User;
use app\models\UserMetaItem;
use yii\base\Model;

class UserProfile extends Model
{
    public $name;
    public $surname;
    public $email;
    public $phone;
    public $show_contacts;
    public $referer;
    public $about;
    public $vk_link;
    public $fb_link;
    public $instagram_link;
    public $ok_link;
    public $mm_link;
    public $new_password;
    public $repeat_password;
    private $user;
    private $userMeta;
    public $profilePhoto;
    const PROFILE_PHOTO_UPLOAD_DIR = 'uploads/profile-photo';

    public function __construct(User $user)
    {
        parent::__construct();
        $this->user = $user;
        $this->userMeta = UserMetaItem::getByUserId($user->getId());
        $this->loadDataToModel();
    }

    private function loadDataToModel()
    {
        $this->name = $this->user->name;
        $this->show_contacts = $this->userMeta->show_contacts;
        $this->surname = $this->user->surname;
        $this->email = $this->user->email;
        $this->referer = $this->user->referer;
        $this->about = $this->userMeta->about;
        $this->fb_link = $this->userMeta->fb_link;
        $this->vk_link = $this->userMeta->vk_link;
        $this->ok_link = $this->userMeta->ok_link;
        $this->instagram_link = $this->userMeta->instagram_link;
        $this->mm_link = $this->userMeta->mm_link;
    }

    private function loadDataFromModel()
    {
        $this->user->referer = $this->referer;
        $this->userMeta->show_contacts = $this->show_contacts;
        $this->user->name = $this->name;
        $this->user->surname = $this->surname;
        $this->user->email = $this->email;
        $this->userMeta->about = $this->about;
        $this->userMeta->fb_link = $this->fb_link;
        $this->userMeta->vk_link = $this->vk_link;
        $this->userMeta->ok_link = $this->ok_link;
        $this->userMeta->mm_link = $this->mm_link;
        $this->userMeta->instagram_link = $this->instagram_link;
    }

    public function rules()
    {
        return [
            [[
                'name',
                'surname',
                'email',
                'about',
                'fb_link',
                'ok_link',
                'vk_link',
                'referer',
                'instagram_link',
                'mm_link',
                'new_password',
                'repeat_password'
            ], 'string'],
            ['referer', 'validateReferer'],
            [['show_contacts'], 'integer'],
            [['profilePhoto'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
            [['name', 'surname', 'email'], 'required']
        ];
    }

    public function validateReferer()
    {
        if ($this->user->hasReferer() && $this->user->referer != $this->referer) {
            $this->addError("referer", "Вы не можете сменить реферера");
            return;
        }

        if ($this->user->getId() == $this->referer) {
            $this->addError("referer", "Не верно указан код");
        }

        if (!User::findOne($this->referer)) {
            $this->addError("Пользователь с кодом {$this->referer} не найден");
        }
    }

    private function createProfileUploadDirIfNotExist()
    {
        if (!is_dir('uploads/profile-photo')) {
            mkdir('uploads/profile-photo');
        }
    }

    private function removeOldUserPhoto()
    {
        if (!empty($this->user->profile_photo)) {
            @unlink($this->user->profile_photo);
        }
    }

    public function upload()
    {
        $this->createProfileUploadDirIfNotExist();
        if ($this->profilePhoto) {
            $this->removeOldUserPhoto();
            $fname = self::PROFILE_PHOTO_UPLOAD_DIR . '/' . sha1(time() . $this->user->getId()) . '.' . $this->profilePhoto->extension;
            $this->user->profile_photo = $fname;
            $this->profilePhoto->saveAs($fname) && $this->user->save();
            return true;
        }

        return false;
    }

    public function save()
    {
        $this->loadDataFromModel();
        return $this->user->save() && $this->userMeta->save();
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'referer' => 'Id наставника',
            'show_contacts' => 'Показывать контакты ',
            'profilePhoto' => 'Фото профиля',
            'about' => 'О себе',
            'new_password' => 'Новый пароль',
            'repeat_password' => 'Повтор пароля'
        ];
    }

}