<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 06/10/2019
 * Time: 11:42
 */

namespace app\models\backoffice;


use app\models\User;
use yii\base\Model;

class FinancialDashboardModel extends Model
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getBalance()
    {
        return $this->user->balance;
    }

}