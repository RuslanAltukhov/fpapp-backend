<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ActionsStyle]].
 *
 * @see ActionStyle
 */
class ActionsStylesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return ActionStyle[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return ActionStyle|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
