<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[SalePointWorkingHour]].
 *
 * @see SalePointWorkingHour
 */
class SalePointWorkingHoursQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return SalePointWorkingHour[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return SalePointWorkingHour|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
