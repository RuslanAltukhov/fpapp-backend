<?php

namespace app\models;

use app\models\params\ActionDefaultStyleParams;
use app\models\params\ActionParams;
use app\models\params\ActionSaleParams;
use Yii;

/**
 * This is the model class for table "actions".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $created_at
 * @property in $action_id
 * @property string $start_at
 * @property string $end_at
 * @property string $item_description Описание одной фишки
 * @property int $need_to_buy Кол-во фишек для получения бонуса
 * @property int $sale_point id торговой точки в которой прохолдить данная акция
 * @property int $status_id
 */
class Action extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'actions';
    }

    public function getStyle()
    {
        return $this->hasOne(ActionStyle::class, ['action_id' => 'id']);
    }

    public function getCategories()
    {
        return $this->hasMany(Category::class, ['id' => 'category_id'])->viaTable("action_categories", ['action_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at'], 'safe'],
            [['need_to_buy', 'status_id', 'category_id'], 'integer'],
            [['start_at', 'end_at'], 'string'],
            ['status_id', 'default', 'value' => ActionParams::STATUS_ACTIVE],
            [['name', 'description', 'item_description'], 'string', 'max' => 255],
        ];
    }

    public function getOrganization()
    {
        return $this->hasOne(Organization::class, ['id' => 'organization_id']);
    }

    public function getSalePoints()
    {
        return $this->hasMany(SalePoint::class, ['id' => 'sale_point_id'])->viaTable('salepoint_actions', ['action_id' => 'id']);
    }

    public function loadDefaultParams()
    {
        $this->attributes = [
            'need_to_buy' => ActionDefaultStyleParams::DEFAULT_ITEMS_COUNT
        ];
        return $this;
    }

    public function getActivity()
    {
        return $this->hasMany(ActionSale::class, ['action_id' => 'id']);
    }

    public function getActionSales()
    {
        return $this->hasMany(ActionSale::class, ['action_id' => 'id'])->where(['type' => ActionSaleParams::ITEM_TYPE_SALE]);
    }

    public function getFirstSale()
    {
        $firstSale = $this->getActionSales()->orderBy("created_at DESC")->one();
        return $firstSale;
    }

    public function getActionGifts()
    {
        return $this->hasMany(ActionSale::class, ['action_id' => 'id'])->where(['type' => ActionSaleParams::ITEM_TYPE_GIFT]);

    }

    public function isUpdated()
    {
        $attrs = ["name", "description"];
        foreach ($attrs as $attr) {
            if ($this->isAttributeChanged($attr)) {
                return true;
            }
        }
        return false;
    }

    public function markAsInModerate()
    {
        $this->status_id = ActionParams::STATUS_NEW;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'description' => 'Описание',
            'created_at' => 'Дата создания',
            'start_at' => 'Дата начала',
            'end_at' => 'Дата окончания',
            'item_description' => 'Одна фишка',
            'need_to_buy' => 'Need To Buy',
            'sale_point' => 'Торговая точка',
            'status_id' => 'Статус акции',
        ];
    }

    /**
     * {@inheritdoc}
     * @return ActionsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ActionsQuery(get_called_class());
    }
}
