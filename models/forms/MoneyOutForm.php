<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 06/10/2019
 * Time: 11:55
 */

namespace app\models\forms;


use app\models\User;
use yii\base\Model;

class MoneyOutForm extends Model
{
    private $user;
    public $sum;

    public function __construct(User $user)
    {
        $this->user = $user;
        parent::__construct();
    }

    public function rules()
    {
        return [
            ['sum', 'number'],
            ['sum', 'validateSum']
        ];
    }

    public function validateSum()
    {

        if ($this->user->balance < $this->sum) {
            $this->addError("sum", 'У вас не достаточно средств для вывода');
            return false;
        }
        return true;
    }

    public function createRequest()
    {
        $transaction = \Yii::$app->getDb()->beginTransaction();
       /* if (!$this->validateSum()) {
            return false;
        }*/

    }
}