<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 15/10/2019
 * Time: 23:40
 */

namespace app\models\forms;


use app\classes\OrganizationSubscription;
use app\models\Organization;
use app\models\Subscription;
use app\models\SubscriptionParams;
use yii\base\Model;

class BuySubscriptionForm extends Model
{
    public $subscriptionType;
    public $period;
    public $salePointsCount = 1;
    private $organization;
    private $organizationSubscriptition;

    public function __construct(Organization $organization)
    {
        $this->organization = $organization;
        $this->organizationSubscriptition = new OrganizationSubscription($organization);
        parent::__construct();
    }

    public function rules()
    {
        return [
            [['subscriptionType', 'salePointsCount', 'period'], 'integer'],
            ['subscriptionType', 'in', 'range' => [Subscription::SUBSCRIPTION_TYPE_BUSINESS]],
            [['subscriptionType', 'salePointsCount', 'period'], 'required']
        ];
    }

    private function generateSubscriptionParams()
    {
        $model = new SubscriptionParams();
        $model->loadDefaultBusinessParams();
        $model->subscription_id = $this->subscriptionType;
        $model->organization_id = $this->organization->id;
        $model->salepoints_count = $this->salePointsCount;
        return $model;
    }

    public function buy()
    {
        $subscriptionParams = $this->generateSubscriptionParams();
        $this->organizationSubscriptition->buySubscription($subscriptionParams, $this->period);
    }
}