<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 17/07/2019
 * Time: 20:28
 */

namespace app\models\forms;

use app\models\User;
use yii\base\Model;
use app\models\Organization;

class RestorePasswordForm extends Model
{
    public $username;

    public function rules()
    {
        return [
            ['username', 'string'],
            ['username', 'required'],
            ['username', 'checkUserExistance']
        ];
    }

    public function checkUserExistance()
    {
        $user = User::findOne(['phone' => $this->username]);
        if (!$user || !Organization::findOne(['owner_uid' => $user->id])) {
            $this->addError("username", "Пользователь отсутствует");
        }
    }

    public function sendRequest()
    {
        return true;
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Логин'
        ];
    }
}