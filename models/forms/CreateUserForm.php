<?php
/**
 * Класс для создания пользователя
 * User: ruslan
 * Date: 16/11/2019
 * Time: 00:50
 */

namespace app\models\forms;


use app\models\User;

class CreateUserForm extends \app\models\forms\api\RegistrationForm
{
    public $password;

    public function __construct(User $referer)
    {
        $this->referer = $referer->getId();
        parent::__construct();
    }

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = ['password', 'string', 'length' => [4]];
        return $rules;
    }

    protected function writeRegistrationFee(?User $referer, User $referalUser)
    {

    }

    protected function getPassword()
    {
        return $this->password;
    }

    protected function writeReferalStat(User $user)
    {

    }

    public function attributeLabels()
    {
        return [
            'phone' => 'Телефон',
            'password' => 'Пароль',
        ];
    }

    private function clearForm()
    {
        $this->phone = "";
        $this->password = "";
    }

    public function createUser()
    {
        $password = $this->getPassword();
        $transaction = \Yii::$app->getDb()->beginTransaction();
        $userModel = new User([
            'phone' => $this->phone,
            'password' => User::getPasswordEncoded($password)
        ]);

        if (!empty($this->referer)) {
            $userModel->referer = $this->referer;
        }

        if (!$userModel->save()) {
            return false;
        }

        $userModel->code = $this->generateCode($userModel->getId());
        if (!$userModel->save()) {
            return false;
        }

        if ($userModel->hasReferer()) {
            $this->writeReferalStat($userModel->refererUser);
        }
        $this->writeRegistrationFee($userModel->refererUser, $userModel);
        $transaction->commit();
        $this->clearForm();

        return $userModel;
    }
}