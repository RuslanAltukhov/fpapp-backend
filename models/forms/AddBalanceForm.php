<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 04/10/2019
 * Time: 15:46
 */

namespace app\models\forms;


use app\classes\paymentsystem\PaymentSystemInterface;
use app\classes\paymentsystem\Robokassa;
use app\classes\TransactionCreator;
use app\models\Transaction;
use app\models\User;
use yii\base\Model;

class AddBalanceForm extends Model
{
    private $user;
    public $sum;
    public $paymentSystem;
    public $transactionCreator;

    public function rules()
    {
        return [
            ['sum', 'integer'],
            ['sum', 'required']
        ];
    }

    public function __construct(User $user, PaymentSystemInterface $paymentSystem)
    {
        $this->user = $user;
        $this->paymentSystem = $paymentSystem;
        $this->transactionCreator = new TransactionCreator($user);
        parent::__construct();
    }

    public function attributeLabels()
    {
        return [
            'sum' => 'Сумма для пополнения'
        ];
    }

    public function getPaymentLink()
    {
        return $this->paymentSystem->getPaymentLink($this->user, $this->sum);
    }
}