<?php

namespace app\models\forms;

use app\classes\OrganizationCreator;
use app\classes\PhoneNumberFormatter;
use app\models\Organization;
use app\models\User;
use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    protected $_user = false;

    private $phoneFormatter;

    public function __construct()
    {
        $this->phoneFormatter = new PhoneNumberFormatter();
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            ['username', 'filter', 'filter' => function ($value) {
                return $this->phoneFormatter->getFormattedNumber($value);
            }],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            ['username', 'validateUsername'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    public function validateUsername()
    {
        $user = $this->getUser();

        if (!$user) {
            $this->addError("username", "Пользователь не найден");
            return;
        }

        if (!$this->isOrganizationExistForUser($user)) {
            $organizationCreator = new OrganizationCreator($user->refererUser, $user);
            $organizationCreator->create();
        }
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$this->isOrganizationExistForUser($user) || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Не верный логин или пароль');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        }
        return false;
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Логин',
            'password' => 'Пароль',
        ];
    }

    public function isOrganizationExistForUser($user)
    {
        return !empty($user->organization);
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);

        }

        return $this->_user;
    }
}
