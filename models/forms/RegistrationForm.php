<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 20/07/2019
 * Time: 08:04
 */

namespace app\models\forms;


use app\models\Organization;
use app\models\User;
use Yii;
use yii\base\Model;

class RegistrationForm extends \app\models\forms\api\RegistrationForm
{
    public $companyName;
    public $phone;

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['companyName'], 'string'];
        $rules[] = [['companyName'], 'required'];
        return $rules;
    }

    public function createCompany()
    {
        $transaction = Yii::$app->getDb()->beginTransaction();
        $userModel = parent::createUser();

        if (!$userModel) {
            return false;
        }
        $transaction->commit();
        return Yii::$app->infoMailer->compose("new_user", [
            'phone' => $userModel->phone,
            'companyName' => $this->companyName
        ])
            ->setFrom('info@comeback.plus')
            ->setTo('info@comeback.plus')// кому отправляем - реальный адрес куда придёт письмо формата asdf @asdf.com
            ->setSubject('Новый пользователь')// тема письма
            ->send();
    }

    public function attributeLabels()
    {
        return [
            'phone' => 'Телефон',
            'companyName' => 'Название кампании'
        ];
    }
}