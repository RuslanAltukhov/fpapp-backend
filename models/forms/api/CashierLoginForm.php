<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 23/07/2019
 * Time: 21:37
 */

namespace app\models\forms\api;


use app\models\AuthToken;
use app\models\Cashier;
use app\models\CashierSession;
use yii\base\Model;

class CashierLoginForm extends \app\models\forms\LoginForm
{
    private $cashier;

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = ['username', 'findCashierByUsername'];
        return $rules;
    }

    public function getCashier()
    {
        return $this->cashier;
    }

    public function getAuthToken()
    {
        if (!$this->_user) {
            return null;
        }
        $token = new AuthToken();
        $token->setUserId($this->_user->id);
        $token->itIsCashier();
        if (!$token->save()) {
            return null;
        }

        return $token;
    }

    public function findCashierByUsername()
    {
        $user = $this->getUser();
        if (!$user || !$cashier = Cashier::findOne(['user_id' => $user->id])) {
            $this->addError("cashierId", "Кассир не найден");
            return;
        }
        $this->cashier = Cashier::findOne(['user_id' => $user->id]);

    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Не верный логин или пароль');
            }
        }
    }

    public function getErrorsFormatted()
    {
        return implode(",", array_merge($this->getErrors("cashierId"), $this->getErrors("password")));
    }

    public function login()
    {
        return parent::login();
    }

}