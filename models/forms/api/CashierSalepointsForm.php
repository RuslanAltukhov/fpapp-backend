<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 26/07/2019
 * Time: 07:47
 */

namespace app\models\forms\api;


use app\models\Cashier;
use yii\base\Model;

class CashierSalepointsForm extends Model
{
    private $cashier;

    public function setCashier(Cashier $cashier)
    {
        $this->cashier = $cashier;
        return $this;
    }

    public function getSalePoints()
    {
        return $this->cashier->getSalepoints()->onlyActive()->all();
    }
}