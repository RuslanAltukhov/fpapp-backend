<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 08/08/2019
 * Time: 21:54
 */

namespace app\models\forms\api;


use app\models\Cashier;
use Yii;
use yii\base\Model;

class CashierSupportMessageForm extends Model
{
    public $message;
    private $cashier;

    public function __construct(Cashier $cashier)
    {
        $this->cashier = $cashier;
        parent::__construct();
    }

    public function rules()
    {
        return [
            ['message', 'string'],
            ['message', 'required']
        ];
    }

    public function send()
    {
        return Yii::$app->supportMailer->compose("support_message", [
            'message' => $this->message,
            'organization' => $this->cashier->organization->name,
            'cashierId' => $this->cashier->id,
            'cashierName' => $this->cashier->name,
            'phone' => $this->cashier->user->phone

        ])
            ->setFrom('support@comeback.plus')
            ->setTo('support@comeback.plus')// кому отправляем - реальный адрес куда придёт письмо формата asdf @asdf.com
            ->setSubject('Обращение в поддержку')// тема письма
            ->send();
    }

}