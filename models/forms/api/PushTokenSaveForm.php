<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 03/08/2019
 * Time: 11:48
 */

namespace app\models\forms\api;


use app\models\PushToken;
use app\models\User;
use yii\base\Model;

class PushTokenSaveForm extends Model
{
    public $token;
    private $user;

    /**
     * PushTokenSaveForm constructor.
     * @param User $model
     */
    public function __construct(User $model)
    {
        $this->user = $model;
    }

    public function rules()
    {
        return [
            ['token', 'required'],
            ['token', 'string'],
            ['token', 'validateToken']
        ];
    }

    public function validateToken()
    {
        if (PushToken::findOne(['token' => $this->token])) {
            $this->addError("token", "Токен уже существует");
        }
    }

    public function save()
    {
        $model = new PushToken();
        $model->token = $this->token;
        $model->user_id = $this->user->getId();
        return $model->save();
    }
}