<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 27/07/2019
 * Time: 07:57
 */

namespace app\models\forms\api;


use app\classes\PushMessageSender;
use app\classes\UserActions;
use app\models\Action;
use app\models\ActionSale;
use app\models\AddPlusNotifcationTextBuilder;
use app\models\Cashier;
use app\models\SalePoint;
use app\models\User;
use yii\base\Model;

class CashierAddPlusForm extends Model
{
    public $items;
    private $cashier;
    private $user;
    private $userActions;
    private $salePoint;
    private $notifcationTextBuilder;
    private $pushMessageSender;

    public function __construct(User $user,
                                Cashier $cashier,
                                SalePoint $salePoint,
                                UserActions $userActions
    )
    {
        $this->user = $user;
        $this->cashier = $cashier;
        $this->salePoint = $salePoint;
        $this->userActions = $userActions;
        $this->notifcationTextBuilder = new AddPlusNotifcationTextBuilder();
        $this->pushMessageSender = new PushMessageSender();

        parent::__construct();
    }


    public function rules()
    {
        return [
            ['items', 'safe'],
            ['items', 'validateData']
        ];
    }

    public function validateData()
    {
        if (!is_array($this->items)) {
            $this->addError("items", "Не верный формат данных");
            return;
        }
        foreach ($this->items as $item) {
            if (!array_key_exists("actionId", $item) || !array_key_exists("count", $item)) {
                $this->addError("items", "В запросе отсутсвует необходимые данные");
                return;
            }
        }

    }

    private function savePurchasedItemsForAction(array $item)
    {
        $action = Action::findOne($item['actionId']);
        $currentLapNum = $this->userActions->getCurrentLapNum($action);
        $lapRemainItemsCount = $this->userActions->getActionLapRemainItems($action);
        $pos = 0;
        while (($pos++) < $item['count'] && $lapRemainItemsCount >= $pos) {
            $model = ActionSale::fromState($item['actionId'], $this->cashier->id, $this->salePoint->id, $this->user->id, $currentLapNum);
            $this->notifcationTextBuilder->addItem($model);
            $model->save();
        }
        if ($lapRemainItemsCount < $item['count']) {
            $model = ActionSale::fromState($item['actionId'], $this->cashier->id, $this->salePoint->id, $this->user->id, $currentLapNum);
            $model->setTypeGift()->save();
            $this->notifcationTextBuilder->addItem($model);
        }
        return;


    }

    public function save()
    {
        $transaction = \Yii::$app->getDb()->beginTransaction();
        foreach ($this->items as $item) {
            $this->savePurchasedItemsForAction($item);
        }
        $this->user->setRefererIfNotExist($this->cashier->organization->id);
        $this->user->save();
        $transaction->commit();
        try {
            $result = $this->pushMessageSender->setUsers($this->user)->sendNotification($this->notifcationTextBuilder->build(), [
                'action' => 'plus'
            ]);
        } catch (\Exception $e) {

        }
        return true;
    }
}