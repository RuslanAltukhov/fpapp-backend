<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 27/05/2019
 * Time: 16:33
 */

namespace app\models\forms\api;


use app\classes\BarcodeGenerator;
use app\models\AuthToken;
use app\models\User;
use yii\base\Model;

class LoginForm extends \app\models\forms\LoginForm
{


    public function getAuthToken()
    {
        $user = $this->getUser();
        if (!$user) {
            return null;
        }

        $token = new AuthToken([
            'user_id' => $user->id
        ]);
        if (!$token->save()) {
            return null;
        }

        return $token;
    }
    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Не верный логин или пароль');
            }
        }
    }

    public function login()
    {
        $loginResult = parent::login();
		if($loginResult) {
			$user = parent::getUser();
			$barcoderGenerator = new BarcodeGenerator($user);
			$barcoderGenerator->generateBarcodeForUser();
		}
		return $loginResult;
    }

}