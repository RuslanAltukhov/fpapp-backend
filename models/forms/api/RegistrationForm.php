<?php

namespace app\models\forms\api;

use app\classes\helpers\UserHelper;
use app\classes\PartnerProgrammPayment;
use app\classes\PhoneNumberFormatter;
use app\classes\ReferalStatReader;
use app\classes\ReferalStatWriter;
use app\classes\SmscSender;
use app\classes\StreamTelecomSmsSender;
use app\models\User;
use libphonenumber\NumberParseException;
use yii\base\Model;

/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 27/05/2019
 * Time: 16:05
 */
class RegistrationForm extends Model
{
    public $phone;
    public $referer;
    const PASSWORD_LENGTH = 4;

    private $phoneFormatter;

    public function __construct()
    {
        $this->phoneFormatter = new PhoneNumberFormatter();
    }

    public function rules()
    {
        return [
            [['phone', 'referer'], 'safe'],
            ['phone', 'required'],
            ['referer', 'validateReferer'],
            ['phone', 'filter', 'filter' => function ($value) {
                try {
                    return $this->phoneFormatter->getFormattedNumber($value);
                } catch (NumberParseException $e) {
                    $this->addError("phone", "Не верный формат номера телефона");
                }
            }],
            ['phone', 'unique', 'targetAttribute' => 'phone', 'targetClass' => User::class],
        ];
    }

    public function attributeLabels()
    {
        return [
            'phone' => 'Телефон'
        ];
    }

    public function validateReferer()
    {
        if (empty($this->referer)) {
            return;
        }

        $referer = User::findByPhoneOrId($this->referer);

        if (!$referer) {
            $this->addError("referer", "Указан не верный код приглашения");
        }

    }

    protected function generateCode(string $data)
    {
        return $data;
    }

    protected function writeReferalStat(User $user)
    {
        $referalStatWriter = new ReferalStatWriter($user);
        $referalStatWriter->incrementRegistrationsCounter();
    }

    protected function writeRegistrationFee(?User $referer, User $referalUser)
    {
        if (!$referer) {
            return;
        }
        $partnerProgramm = new PartnerProgrammPayment($referer);
        $partnerProgramm->onChildUserRegistered($referalUser);
    }

    protected function getPassword()
    {
        return UserHelper::generateRandomPassword(self::PASSWORD_LENGTH);
    }

    public function createUser()
    {
        $password = $this->getPassword();
        $transaction = \Yii::$app->getDb()->beginTransaction();
        $userModel = new User([
            'phone' => $this->phone,
            'password' => User::getPasswordEncoded($password)
        ]);

        if (!empty($this->referer)) {
            $userModel->referer = $this->referer;
        }

        if (!$userModel->save()) {
            return false;
        }

        $userModel->code = $this->generateCode($userModel->getId());
        if (!$userModel->save()) {
            return false;
        }

        if ($userModel->hasReferer()) {
            $this->writeReferalStat($userModel->refererUser);
        }
        $this->writeRegistrationFee($userModel->refererUser, $userModel);
        $transaction->commit();
        $smsSender = SmscSender::getInstance();
        $smsSender->sendMessage("Ваш пароль:" . $password, $this->phone);
        return $userModel;
    }
}