<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 06/08/2019
 * Time: 16:40
 */

namespace app\models\forms\api;


use app\classes\helpers\UserHelper;
use app\classes\PhoneNumberFormatter;
use app\classes\SmscSender;
use app\classes\StreamTelecomSmsSender;
use app\models\User;
use yii\base\Model;

class UserRestorePasswordForm extends Model
{
    public $phone;
    private $user;
    private $messageSender;
    const PASSWORD_LENGTH = 4;
    private $phoneFormatter;

    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->messageSender = SmscSender::getInstance();
        $this->phoneFormatter = new PhoneNumberFormatter();
    }

    public function rules()
    {
        return [
            ['phone', 'required'],
            ['phone', 'string'],
            ['phone', 'filter', 'filter' => function ($value) {
                return $this->phoneFormatter->getFormattedNumber($value);
            }],
            ['phone', 'validateUser'],
        ];
    }

    public function validateUser()
    {
        $this->user = User::findOne(['phone' => $this->phone]);
        if (!$this->user) {
            $this->addError("phone", "Пользователь с таким номером не найден");
        }
    }

    public function restorePassword()
    {
        $newPass = UserHelper::generateRandomPassword(self::PASSWORD_LENGTH);
        $this->user->password = User::getPasswordEncoded($newPass);
        if ($this->user->save()) {
            $this->messageSender->sendMessage("Ваш новый пароль:" . $newPass, $this->user->phone);
            return true;
        }
        return false;
    }

}