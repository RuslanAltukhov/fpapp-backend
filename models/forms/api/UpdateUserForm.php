<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 08/07/2019
 * Time: 10:03
 */

namespace app\models\forms\api;


use yii\base\Model;
use app\models\User;

class UpdateUserForm extends Model
{
    public $name;
    public $email;
    public $new_password;
    public $gender;
    public $notifications;
    /**
     * @var User
     */
    public $userModel;
    public $refererCode;

    public function rules()
    {
        return [
            [['email', 'name', 'new_password'], 'string'],
            [['email', 'name'], 'required'],
            [['notifications', 'refererCode'], 'safe'],
            ['refererCode', 'validateReferer'],
            ['gender', 'in', 'range' => [User::SEX_MAN, User::SEX_WOMAN]]
        ];
    }

    public function validateReferer()
    {
        if (empty($this->refererCode) || $this->userModel->hasReferer()) {
            return;
        }
        if ($user = User::findOne($this->refererCode)) {
            $this->userModel->referer = $this->refererCode;
            return;
        }
        $this->addError("refererCode", "Пользователь с кодом {$this->refererCode}  не найден");
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя'
        ];
    }

    public function setUserModel(User $model)
    {
        $this->userModel = $model;
        return $this;
    }

    public function save()
    {
        $this->userModel->name = $this->name;
        $this->userModel->email = $this->email;
        $this->userModel->notifications = !empty($this->notifications) ? 1 : 0;
        $this->userModel->sex = $this->gender;
        if (!empty($this->new_password)) {
            $this->userModel->password = User::getPasswordEncoded($this->new_password);
        }
        return $this->userModel->save();
    }
}