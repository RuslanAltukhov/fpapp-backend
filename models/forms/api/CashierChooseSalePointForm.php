<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 27/05/2019
 * Time: 22:01
 */

namespace app\models\forms\api;


use app\classes\adapters\UserAuthDataAdapter;
use app\classes\CashierContext;
use app\models\AuthToken;
use app\models\Cashier;
use app\models\SalePoint;
use app\models\User;
use yii\base\Model;

class CashierChooseSalePointForm extends Model
{
    public $salepointId;
    private $authToken;
    private $cashier;

    public function rules()
    {
        return [
            ['salepointId', 'integer'],
            ['salepointId', 'required'],
            ['salepointId', 'validateSalePoint']
        ];
    }

    public function getErrorsFormatted()
    {
        return implode(",", $this->getErrorSummary(true));
    }

    public function setCashier(Cashier $cashier)
    {
        $this->cashier = $cashier;
        return $this;
    }

    public function setAuthToken(AuthToken $authToken)
    {
        $this->authToken = $authToken;
        return $this;
    }

    public function validateSalePoint()
    {

        if (!($model = SalePoint::findOne($this->salepointId)) || !(new CashierContext($this->cashier))->isRelatedToSalePoint($model)) {
            $this->addError("salePointId", "Торговая точка не найдена");
        }
    }

    public function saveSelection()
    {
        $sessionData = $this->authToken->getSessionData();
        $sessionData->setSalePointId($this->salepointId);
        $this->authToken->setSessionData($sessionData);
        return true;
    }
}