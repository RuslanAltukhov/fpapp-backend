<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 14/07/2019
 * Time: 22:19
 */

namespace app\models\forms;


use app\models\Organization;
use yii\base\Model;
use yii\web\UploadedFile;

class OrganizationUpdateForm extends Model
{
    const LOGO_FILE_NAME_PATTERN = 'company_%d_logo.%s';
    public $companyName;
    public $contactPerson;
    public $contactPersonPhone;
    public $contactPersonEmployment;
    public $companyContactEmail;
    public $logoPath;
    public $logo;
    private $organization;
    public $uploadedImage;

    public function rules()
    {
        return [
            ['companyContactEmail', 'email'],
            ['logo', 'image', 'skipOnEmpty' => true],
            [['companyName'], 'required'],
            [['contactPerson', 'contactPersonPhone', 'contactPersonEmployment'], 'string'],
        ];
    }

    public function setOrganization(Organization $organization)
    {
        $this->organization = $organization;
        $this->loadFromModel();
        return $this;
    }

    public function getCurrentLogo()
    {
        return '/' . $this->logoPath ?? "http://placehold.it/450x450";
    }

    private function loadFromModel()
    {
        $this->companyName = $this->organization->name;
        $this->contactPerson = $this->organization->contact_person;
        $this->contactPersonPhone = $this->organization->contact_person_phone;
        $this->contactPersonEmployment = $this->organization->contact_person_employment;
        $this->companyContactEmail = $this->organization->contact_email;
        $this->logoPath = $this->organization->logo_path;
    }

    private function loadToModel()
    {
        $this->organization->name = $this->companyName;
        $this->organization->contact_person = $this->contactPerson;
        $this->organization->contact_person_phone = $this->contactPersonPhone;
        $this->organization->contact_person_employment = $this->contactPersonEmployment;
        $this->organization->contact_email = $this->companyContactEmail;
        if ($fname = $this->saveLogo()) {
            $this->organization->logo_path = $fname;
        }

    }

    public function attributeLabels()
    {
        return [
            'companyName' => 'Название кампании',
            'contactPerson' => 'Контактное лицо',
            'contactPersonEmployment' => 'Должность',
            'contactPersonPhone' => 'Телефон для связи',
            'logo' => 'Логотип',
            'companyContactEmail' => 'Почта для оповещений'
        ];
    }

    private function saveLogo(): ?string
    {
        if ($this->uploadedImage instanceof UploadedFile) {
            $fname = \Yii::$app->params['uploadsPath'] . sprintf(self::LOGO_FILE_NAME_PATTERN, $this->organization->id, $this->uploadedImage->extension);
            if (!$this->uploadedImage->saveAs($fname)) {
                return null;
            }
            return $fname;
        }

        return null;
    }

    public function save()
    {
        $this->loadToModel();
        return $this->organization->save();
    }

}