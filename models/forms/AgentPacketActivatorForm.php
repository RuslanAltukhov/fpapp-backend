<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 18/11/2019
 * Time: 22:54
 */

namespace app\models\forms;


use app\classes\adapters\PartnerLimitationsAdapter;
use app\classes\AgentPacketActivator;
use app\models\User;
use yii\base\Model;

class AgentPacketActivatorForm extends Model
{
    public $userId;
    private $limitationsAdapter;

    public function __construct(PartnerLimitationsAdapter $limitationsAdapter)
    {
        $this->limitationsAdapter = $limitationsAdapter;
        parent::__construct();
    }

    public function rules()
    {
        return [
            ['userId', 'required'],
            ['userId', 'integer'],
            ['userId', 'validateUserId']
        ];
    }

    public function validateUserId()
    {
        if (!User::findOne($this->userId)) {
            $this->addError("userId", "Пользователь не найден");
        }
    }

    public function activate()
    {
        $packageActivator = new AgentPacketActivator($this->limitationsAdapter, User::findOne($this->userId));
        $packageActivator->activate();
    }
}