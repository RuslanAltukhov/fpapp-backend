<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 07/11/2019
 * Time: 12:37
 */

namespace app\models\tinkoff;


use app\classes\TransactionCreator;
use app\models\Transaction;
use yii\base\Model;

class PaymentAcceptor extends Model
{
    public $TerminalKey;
    public $OrderId;
    public $Success;
    public $Status;
    private $tKey;
    const SUCCESS_CODE = 'CONFIRMED';

    public function __construct($terminalKey)
    {
        $this->tKey = $terminalKey;
    }

    public function rules()
    {
        return [
            [['TerminalKey', 'OrderId', 'Success', 'Status'], 'required'],
            ['TerminalKey', 'validateTerminalKey'],
            ['Status', 'validateStatus'],
            ['OrderId', 'validateOrderId'],
        ];
    }

    public function validateStatus()
    {
        if ($this->Status != self::SUCCESS_CODE) {
            throw new \Exception("Статус платежа 'не выполнен'");
        }
    }

    public function validateTerminalKey()
    {
        if ($this->TerminalKey != $this->tKey) {
            throw new \Exception("Не валидный ключ терминала");
        }
    }

    public function validateOrderId()
    {
        $order = Transaction::findOne($this->OrderId);
        if (!$order) {
            throw new \Exception("Заказ с номером {$this->OrderId}");
        }

        if ($order->isDone()) {
            throw new \Exception("Заказ с номером {$this->OrderId} был оплачен ранее");
        }

    }

    public function accept()
    {
        $transaction = Transaction::findOne($this->OrderId);
        $transactionCreator = new TransactionCreator($transaction->dstUser);
        $transactionCreator->confirmIncomeTransaction($transaction);
        return true;
    }

}