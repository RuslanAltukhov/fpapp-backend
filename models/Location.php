<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 04/07/2019
 * Time: 07:27
 */

namespace app\models;


class Location
{
    private $lat;
    private $lng;

    public function __construct($lat, $lng)
    {
        $this->lat = $lat;
        $this->lng = $lng;
    }

    public function getLat()
    {
        return $this->lat;
    }

    public function getLng()
    {
        return $this->lng;
    }
}