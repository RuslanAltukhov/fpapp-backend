<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 15/10/2019
 * Time: 12:57
 */

namespace app\models;


class Subscription
{
    const SUBSCRIPTION_TYPE_DEFAULT = 1;
    const SUBSCRIPTION_TYPE_BUSINESS = 2;
    public static $subscriptionNames = [
        self::SUBSCRIPTION_TYPE_BUSINESS => "Бизнес",
        self::SUBSCRIPTION_TYPE_DEFAULT => "Старт"
    ];
}