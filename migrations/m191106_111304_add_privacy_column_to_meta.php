<?php

use yii\db\Migration;

/**
 * Class m191106_111304_add_privacy_column_to_meta
 */
class m191106_111304_add_privacy_column_to_meta extends Migration
{
    private $table = "user_meta";
    private $column = "show_contacts";

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->table, $this->column, $this->boolean()->defaultValue(true));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, $this->column);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191106_111304_add_privacy_column_to_meta cannot be reverted.\n";

        return false;
    }
    */
}
