<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%users}}`.
 */
class m190518_142413_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%users}}', [
            'id' => $this->primaryKey(),
            'phone' => $this->string(),
            'password' => $this->string(),
            'email' => $this->string(),
            'name' => $this->string(),
            'code' => $this->string(),
            'sex' => $this->tinyInteger(1)->comment("1-мужской, 2-женский"),
            'notifications' => $this->tinyInteger(1)->comment("Включены ли уведомления у данного пользователя"),
            'referer_organization' => $this->integer(),
            'created_at' => $this->timestamp(),
            'barcode_base64' => "varchar(600)",
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%users}}');
    }
}
