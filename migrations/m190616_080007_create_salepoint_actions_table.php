<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%salepoint_actions}}`.
 */
class m190616_080007_create_salepoint_actions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%salepoint_actions}}', [
            'id' => $this->primaryKey(),
            'created_at' => $this->timestamp(),
            'action_id' => $this->integer(),
            'sale_point_id' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%salepoint_actions}}');
    }
}
