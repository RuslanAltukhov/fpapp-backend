<?php

use yii\db\Migration;

/**
 * Class m190713_192922_add_text_colors_column
 */
class m190713_192922_add_text_colors_column extends Migration
{
    private $table = "actions_styles";

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->table, "item_text_color", $this->char(7));
        $this->addColumn($this->table, "active_item_text_color", $this->char(7));
        $this->addColumn($this->table, "last_item_text_color", $this->char(7));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, "item_text_color");
        $this->dropColumn($this->table, "active_item_text_color");
        $this->dropColumn($this->table, "last_item_text_color");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190713_192922_add_text_colors_column cannot be reverted.\n";

        return false;
    }
    */
}
