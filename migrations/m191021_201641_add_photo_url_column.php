<?php

use yii\db\Migration;

/**
 * Class m191021_201641_add_photo_url_column
 */
class m191021_201641_add_photo_url_column extends Migration
{
    private $table = "users";
    private $column = "profile_photo";

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->table, $this->column, $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, $this->column);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191021_201641_add_photo_url_column cannot be reverted.\n";

        return false;
    }
    */
}
