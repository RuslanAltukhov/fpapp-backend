<?php

use yii\db\Migration;

/**
 * Class m190929_192505_add_balance_column
 */
class m190929_192505_add_balance_column extends Migration
{
    private $table = "users";
    private $column = "balance";

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->table, $this->column, $this->decimal(16, 2)->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, $this->column);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190929_192505_add_balance_column cannot be reverted.\n";

        return false;
    }
    */
}
