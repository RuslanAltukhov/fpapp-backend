<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%cashier}}`.
 */
class m190518_145602_create_cashier_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%cashiers}}', [
            'id' => $this->primaryKey(),
            'organization_id' => $this->integer(),
            'name' => $this->string(),
            'created_at' => $this->timestamp(),
            'user_id' => $this->integer(),
            'is_archived' => $this->tinyInteger(1)->defaultValue(0)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%cashiers}}');
    }
}
