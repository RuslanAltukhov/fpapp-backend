<?php

use yii\db\Migration;

/**
 * Class m190709_104306_drop_updated_at_column
 */
class m190709_104306_drop_updated_at_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn("auth_tokens", "updated_at");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190709_104306_drop_updated_at_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190709_104306_drop_updated_at_column cannot be reverted.\n";

        return false;
    }
    */
}
