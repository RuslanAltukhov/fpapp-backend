<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sale_point_working_hours}}`.
 */
class m190616_065349_create_sale_point_working_hours_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%sale_point_working_hours}}', [
            'id' => $this->primaryKey(),
            'sale_point_id' => $this->integer(),
            'created_at' => $this->timestamp(),
            'day_of_week' => $this->tinyInteger()->notNull(),
            'open' => $this->time(),
            'close' => $this->time()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%sale_point_working_hours}}');
    }
}
