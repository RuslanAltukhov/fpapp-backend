<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%referal_stat}}`.
 */
class m190929_193941_create_referal_stat_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%referal_stat}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'action_type' => $this->tinyInteger(),
            'action_date' => $this->timestamp(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%referal_stat}}');
    }
}
