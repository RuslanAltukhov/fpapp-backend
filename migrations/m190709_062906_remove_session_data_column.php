<?php

use yii\db\Migration;

/**
 * Class m190709_062906_remove_session_data_column
 */
class m190709_062906_remove_session_data_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn("auth_tokens", "session_data");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190709_062906_remove_session_data_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190709_062906_remove_session_data_column cannot be reverted.\n";

        return false;
    }
    */
}
