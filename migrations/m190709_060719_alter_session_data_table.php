<?php

use yii\db\Migration;

/**
 * Class m190709_060719_alter_session_data_table
 */
class m190709_060719_alter_session_data_table extends Migration
{
    private $table = "auth_tokens";
    private $column = "session_data";

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->getDb()->createCommand("ALTER TABLE auth_tokens MODIFY session_data TEXT DEFAULT NULL");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190709_060719_alter_session_data_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190709_060719_alter_session_data_table cannot be reverted.\n";

        return false;
    }
    */
}
