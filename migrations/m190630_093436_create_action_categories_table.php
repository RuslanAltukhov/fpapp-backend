<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%action_categories}}`.
 */
class m190630_093436_create_action_categories_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%action_categories}}', [
            'id' => $this->primaryKey(),
            'action_id' => $this->integer(),
            'category_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%action_categories}}');
    }
}
