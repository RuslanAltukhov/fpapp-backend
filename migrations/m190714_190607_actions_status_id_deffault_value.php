<?php

use yii\db\Migration;

/**
 * Class m190714_190607_actions_status_id_deffault_value
 */
class m190714_190607_actions_status_id_deffault_value extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn("actions", "status_id", $this->tinyInteger(1)->defaultValue(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190714_190607_actions_status_id_deffault_value cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190714_190607_actions_status_id_deffault_value cannot be reverted.\n";

        return false;
    }
    */
}
