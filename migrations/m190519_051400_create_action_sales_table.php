<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%action_sales}}`.
 */
class m190519_051400_create_action_sales_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%action_sales}}', [
            'id' => $this->primaryKey(),
            'action_id' => $this->integer(),
            'user_id' => $this->integer(),
            'created_at' => $this->timestamp(),
            'cashier_id' => $this->integer(),
            'salepoint_id' => $this->integer(),
            'lap' => $this->integer()->notNull(),
            'type' => $this->tinyInteger(1)->defaultValue(0)->comment("Фишка или плюшка")
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%action_sales}}');
    }
}
