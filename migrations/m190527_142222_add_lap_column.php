<?php

use yii\db\Migration;

/**
 * Class m190527_142222_add_history_column
 */
class m190527_142222_add_lap_column extends Migration
{
    private $table = "action_sales";
    private $column = "lap";

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->table, $this->column, $this->tinyInteger(1)->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, $this->column);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190527_142222_add_history_column cannot be reverted.\n";

        return false;
    }
    */
}
