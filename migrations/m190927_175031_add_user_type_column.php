<?php

use yii\db\Migration;

/**
 * Class m190927_175031_add_user_type_column
 */
class m190927_175031_add_user_type_column extends Migration
{
    private $table = "users";
    private $column = "user_type";


    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->table, $this->column, $this->tinyInteger()->defaultValue(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, $this->column);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190927_175031_add_user_type_column cannot be reverted.\n";

        return false;
    }
    */
}
