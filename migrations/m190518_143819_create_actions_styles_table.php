<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%actions_styles}}`.
 */
class m190518_143819_create_actions_styles_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%actions_styles}}', [
            'id' => $this->primaryKey(),
            'action_id' => $this->integer(),
            'is_template' => $this->tinyInteger(1)->defaultValue(0),
            'created_at' => $this->timestamp(),
            'action_bg_color' => $this->char(7),
            'action_text_color' => $this->char(7),
            'logo_url' => $this->string(),
            'item_bg_color' => $this->char(7)->comment("Фон не активной фишки"),
            'item_border_color' => $this->char(7)->comment("Цвет обводки не активной фишки"),
            'active_item_bg_color' => $this->char(7)->comment("Фон активной фишки"),
            'active_item_border_color' => $this->char(7)->comment("Цвет обводки активной фишки"),
            'last_item_bg_color' => $this->char(7)->comment("Цвет фона плюшки"),
            'last_item_border_color' => $this->char(7)->comment("Цвет обводки плюшки"),
            'footer_bg_color' => $this->char(7)->comment("Цвет фона футера карточки"),
            'footer_text_color' => $this->char(7)->comment("Цвет текста футера")

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%actions_styles}}');
    }
}
