<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_meta}}`.
 */
class m191002_191130_create_user_meta_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_meta}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'vk_link' => $this->string(),
            'fb_link' => $this->string(),
            'ok_link' => $this->string(),
            'mm_link' => $this->string(),
            'about' => $this->text(),
            'instagram_link' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_meta}}');
    }
}
