<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%subscription_params}}`.
 */
class m191014_190109_create_subscription_params_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%subscription_params}}', [
            'id' => $this->primaryKey(),
            'actions_count' => $this->integer(),
            'salepoints_count' => $this->integer(),
            'cashiers_count' => $this->integer(),
            'organization_id' => $this->integer(),
            'subscription_id' => $this->integer()->notNull(),
            'expire_at' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%subscription_params}}');
    }
}
