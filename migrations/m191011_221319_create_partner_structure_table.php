<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%partner_structure}}`.
 */
class m191011_221319_create_partner_structure_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%partner_structure}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'mentor_id' => $this->integer(),
            'created_at' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%partner_structure}}');
    }
}
