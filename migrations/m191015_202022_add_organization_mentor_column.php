<?php

use yii\db\Migration;

/**
 * Class m191015_202022_add_organization_mentor_column
 */
class m191015_202022_add_organization_mentor_column extends Migration
{

    private $table = "organizations";
    private $column = "mentor_id";

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->table, $this->column, $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, $this->column);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191015_202022_add_organization_mentor_column cannot be reverted.\n";

        return false;
    }
    */
}
