<?php

use yii\db\Migration;

/**
 * Class m190731_214354_add_24hour_column
 */
class m190731_214354_add_24hour_column extends Migration
{
    private $table = "sale_point_working_hours";
    private $column = "every_time";

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->table, $this->column, $this->tinyInteger()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, $this->column);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190731_214354_add_24hour_column cannot be reverted.\n";

        return false;
    }
    */
}
