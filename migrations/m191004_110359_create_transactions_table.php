<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%transactions}}`.
 */
class m191004_110359_create_transactions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%transactions}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(20),
            'created_at' => $this->integer(),
            'processed_at' => $this->integer(),
            'sum' => $this->decimal(15, 2),
            'status' => $this->tinyInteger()->defaultValue(1),
            'source_user_id' => $this->integer()->defaultValue(0),
            'direction' => $this->integer(),
            'type' => $this->tinyInteger()->defaultValue(1),
            'data' => $this->text()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%transactions}}');
    }
}
