<?php

use yii\db\Migration;

/**
 * Class m190704_043030_add_geodist_procedure
 */
class m190704_043030_add_geodist_procedure extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->getDb()->createCommand("
CREATE DEFINER=`root`@`localhost` FUNCTION `geodist`(
  src_lat DECIMAL(9,6), src_lon DECIMAL(9,6),
  dst_lat DECIMAL(9,6), dst_lon DECIMAL(9,6)
) RETURNS decimal(6,2)
    DETERMINISTIC
BEGIN
 SET @dist := 6371 * 2 * ASIN(SQRT(
      POWER(SIN((src_lat - ABS(dst_lat)) * PI()/180 / 2), 2) +
      COS(src_lat * PI()/180) *
      COS(ABS(dst_lat) * PI()/180) *
      POWER(SIN((src_lon - dst_lon) * PI()/180 / 2), 2)
    ));
 RETURN @dist;
END")->query();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190704_043030_add_geodist_procedure cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190704_043030_add_geodist_procedure cannot be reverted.\n";

        return false;
    }
    */
}
