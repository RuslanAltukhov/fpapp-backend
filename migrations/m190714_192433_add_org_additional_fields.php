<?php

use yii\db\Migration;

/**
 * Class m190714_192433_add_org_additional_fields
 */
class m190714_192433_add_org_additional_fields extends Migration
{
    private $table = "organizations";

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->table, "contact_person", $this->string());
        $this->addColumn($this->table, "contact_person_phone", $this->string());
        $this->addColumn($this->table, 'contact_person_employment', $this->string());
        $this->addColumn($this->table, "contact_email", $this->string());
        $this->addColumn($this->table, "logo_path", $this->string()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, "contact_person");
        $this->dropColumn($this->table, "contact_person_phone");
        $this->dropColumn($this->table, "contact_person_employment");
        $this->dropColumn($this->table, "contact_email");
        $this->dropColumn($this->table, "logo_path");

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190714_192433_add_org_additional_fields cannot be reverted.\n";

        return false;
    }
    */
}
