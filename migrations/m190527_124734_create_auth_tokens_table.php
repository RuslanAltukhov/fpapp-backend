<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%auth_tokens}}`.
 */
class m190527_124734_create_auth_tokens_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%auth_tokens}}', [
            'id' => $this->primaryKey(),
            'token' => $this->string(),
            'user_id' => $this->integer(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'session_data' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%auth_tokens}}');
    }
}
