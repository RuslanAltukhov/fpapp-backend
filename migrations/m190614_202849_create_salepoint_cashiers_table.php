<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%salepoint_cashiers}}`.
 */
class m190614_202849_create_salepoint_cashiers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%salepoint_cashiers}}', [
            'id' => $this->primaryKey(),
            'salepoint_id' => $this->integer(),
            'cashier_id' => $this->integer(),
            'created_at' => $this->timestamp()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%salepoint_cashiers}}');
    }
}
