<?php

use yii\db\Migration;

/**
 * Class m190714_190327_update_actions_created_at
 */
class m190714_190327_update_actions_created_at extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn("actions", "created_at", $this->timestamp()->defaultExpression("NOW()"));
        $this->alterColumn("actions", "start_at", $this->timestamp()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190714_190327_update_actions_created_at cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190714_190327_update_actions_created_at cannot be reverted.\n";

        return false;
    }
    */
}
