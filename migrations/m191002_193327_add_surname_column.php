<?php

use yii\db\Migration;

/**
 * Class m191002_193327_add_surname_column
 */
class m191002_193327_add_surname_column extends Migration
{
    private $table = "users";
    private $column = "surname";

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->table, $this->column, $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, $this->column);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191002_193327_add_surname_column cannot be reverted.\n";

        return false;
    }
    */
}
