<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%category_groups}}`.
 */
class m190705_055043_create_category_groups_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%category_groups}}', [
            'id' => $this->primaryKey(),
            'group_name'=>"varchar(200)",
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%category_groups}}');
    }
}
