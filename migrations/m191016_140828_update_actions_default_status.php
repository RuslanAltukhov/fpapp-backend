<?php

use yii\db\Migration;

/**
 * Class m191016_140828_update_actions_default_status
 */
class m191016_140828_update_actions_default_status extends Migration
{
    private $table = "actions";
    private $column = "status_id";

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn($this->table, $this->column, $this->tinyInteger()->defaultValue(\app\models\params\ActionParams::STATUS_ACTIVE));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191016_140828_update_actions_default_status cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191016_140828_update_actions_default_status cannot be reverted.\n";

        return false;
    }
    */
}
