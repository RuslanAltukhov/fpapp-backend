<?php

use yii\db\Migration;

/**
 * Class m190724_052101_add_cashier_password_column
 */
class m190724_052101_add_cashier_password_column extends Migration
{
    private $table = "cashiers";
    private $column = "password";

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->table, $this->column, $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, $this->column);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190724_052101_add_cashier_password_column cannot be reverted.\n";

        return false;
    }
    */
}
