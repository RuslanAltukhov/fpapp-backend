<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sale_points}}`.
 */
class m190518_143421_create_sale_points_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%sale_points}}', [
            'id' => $this->primaryKey(),
            'organization_id' => $this->integer(),
            'created_at' => $this->timestamp(),
            'name' => $this->string(),
            'is_archived' => $this->boolean()->defaultValue(false),
            'address_custom' => $this->string()->comment("Самописный адрес"),
            'address' => $this->string()->comment("Адрес"),
            'lat' => $this->decimal(11, 8),
            'lng' => $this->decimal(11, 8)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%sale_points}}');
    }
}
