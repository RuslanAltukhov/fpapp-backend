<?php

use app\models\User;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%partner_limitation}}`.
 */
class m191118_192426_create_partner_limitation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%partner_limitation}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'agent_packets' => $this->integer()->defaultValue(0),
            'used_agent_packets' => $this->integer()->defaultValue(0),
        ]);
        $partners = User::find()->where(['user_type' => User::USER_TYPE_PARTNER])->all();
        foreach ($partners as $partner) {
            $limitation = \app\models\PartnerLimitation::getByUserId($partner->getId());
            $limitation->agent_packets = 10;
            $limitation->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%partner_limitation}}');
    }
}
