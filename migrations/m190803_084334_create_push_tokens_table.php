<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%push_tokens}}`.
 */
class m190803_084334_create_push_tokens_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%push_tokens}}', [
            'id' => $this->primaryKey(),
            'token' => $this->text(),
            'created_at' => $this->timestamp(),
            'user_id' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%push_tokens}}');
    }
}
