<?php

use app\models\params\ActionParams;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%actions}}`.
 */
class m190518_142902_create_actions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%actions}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'description' => $this->string(),
            'created_at' => $this->timestamp(),
            'start_at' => $this->timestamp(),
            'end_at' => $this->timestamp()->null(),
            'category_id' => $this->integer(),
            'item_description' => $this->string()->comment("Описание одной фишки"),
            'need_to_buy' => $this->smallInteger()->comment("Кол-во фишек для получения бонуса"),
            'organization_id' => $this->integer()->comment("Организация, к которой относится акция"),
            'status_id' => $this->smallInteger()->defaultValue(ActionParams::STATUS_NEW)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%actions}}');
    }
}
