<?php

use yii\db\Migration;

/**
 * Class m190913_144403_add_points_column_to_users
 */
class m190913_144403_add_points_column_to_users extends Migration
{
    private $table = "users";
    private $column = "points";

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->table, $this->column, $this->integer()->defaultValue(0));
        $this->addColumn($this->table, "referer", $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, $this->column);
       $this->dropColumn($this->table, "referer");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190913_144403_add_points_column_to_users cannot be reverted.\n";

        return false;
    }
    */
}
