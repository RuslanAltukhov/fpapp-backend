<?php

use yii\db\Migration;

/**
 * Class m190724_204810_add_auth_tokens_type
 */
class m190724_204810_add_auth_tokens_type_and_meta extends Migration
{
    private $table = "auth_tokens";
    private $column = "is_cashier";

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->table, $this->column, $this->tinyInteger(1)->defaultValue(0));
        $this->addColumn($this->table, "params", $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, $this->column);
        $this->dropColumn($this->table, "params");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190724_204810_add_auth_tokens_type cannot be reverted.\n";

        return false;
    }
    */
}
