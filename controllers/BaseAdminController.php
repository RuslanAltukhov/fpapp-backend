<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 30/06/2019
 * Time: 16:23
 */

namespace app\controllers;


use app\classes\OrganizationSubscription;
use app\models\Organization;
use app\models\Subscription;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UnauthorizedHttpException;

class BaseAdminController extends Controller
{
    protected $organization;
    /**
     * @var OrganizationSubscription
     */
    protected  $subscription;

    private function setLayotParams()
    {
        $this->subscription = new OrganizationSubscription($this->organization);
        \Yii::$app->view->params['organization'] = $this->organization;
        \Yii::$app->view->params['subscriptionName'] = Subscription::$subscriptionNames[$this->subscription->getSubscriptionType()];
        //  print_r($organizationSubscription->getSubscriptionsLimitationsSummary());
        // die;
    }

    private function checkDomain()
    {
        if (preg_match("/(http|https)\:\/\/partner\./i", Url::base(true))) {
            throw new NotFoundHttpException("Раздел не найден");
        }
    }

    public function beforeAction($action)
    {
        if (!\Yii::$app->user->isGuest) {
            $this->checkDomain();
            $this->organization = Organization::findOne(['owner_uid' => \Yii::$app->user->identity->id]);
            $this->setLayotParams();
            return parent::beforeAction($action);
        }
        return $this->redirect("/");
    }

}