<?php

namespace app\controllers\actions;

use app\models\SearchModel\SalesSearchModel;

class SalesController extends \yii\web\Controller
{
    public $layout = "admin";

    public function actionIndex($actionId)
    {
        $searchModel = new SalesSearchModel();
        if ($actionId) {
            $searchModel->actionId = $actionId;
        }
        $dataProvider = $searchModel->getSearchProvider();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

}
