<?php

namespace app\controllers\actions;

use app\classes\ActionSaleStatistic;
use app\controllers\BaseAdminController;
use app\models\Action;
use app\models\ActionSale;
use app\models\Category;
use app\models\forms\ActionForm;
use app\models\SalePoint;
use app\models\SearchModel\ActionSearch;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\web\UrlManager;

class ActionsController extends BaseAdminController
{
    public $layout = "admin";

    public function getViewPath()
    {
        return "@app/views/actions";
    }

    public function actionIndex()
    {
        $searchModel = new ActionSearch();
        $searchModel->setCompanyId($this->organization->id);
        $searchModel->load(\Yii::$app->request->get());
        return $this->render('index', [
            'actionsProvider' => $searchModel->getSearchProvider(),
            'searchModel' => $searchModel
        ]);
    }

    public function actionUpdate($actionId)
    {
        if ($actionModel = Action::findOne($actionId)) {
            $model = new ActionForm($this->subscription->getSubscriptionsLimitationsSummary());
            $model->setOrganization($this->organization);
            $categories = Category::find()->all();
            $model->setModel($actionModel);

            if (\Yii::$app->request->isPost) {
                $model->logoFile = UploadedFile::getInstance($model, 'logo');
            }

            if ($model->load(\Yii::$app->request->post())
                && $model->validate() && $model->save()) {

                \Yii::$app->session->setFlash("message", "Акция обновлена");
            }

            return $this->render("constructor/form", [
                'categories' => $categories,
                'model' => $model,
                'salePoints'=>SalePoint::find()->onlyActive()->andWhere(['organization_id'=>$this->organization->id])->all(),
                'organization' => $this->organization
            ]);
        } else {
            throw new NotFoundHttpException();
        }
    }

    public function actionCreate()
    {
        $model = new ActionForm($this->subscription->getSubscriptionsLimitationsSummary());
        $model->setOrganization($this->organization);
        $categories = Category::find()->all();
        $model->setModel(new Action());
        $model->logo = UploadedFile::getInstance($model, 'logo');
        if ($model->load(\Yii::$app->request->post()) && $model->validate() && $model->save()) {
            \Yii::$app->session->setFlash("success", "Акция успешно создана");
            return $this->redirect("/actions");
        }
        return $this->render("constructor/form", [
            'categories' => $categories,
            'model' => $model,
            'salePoints'=>SalePoint::find()->onlyActive()->andWhere(['organization_id'=>$this->organization->id])->all(),
            'organization' => $this->organization
        ]);
    }

    public function actionView(int $id)
    {
        //TODO Добавить условия получения акции только для текущей организаци
        $model = Action::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException("Акция не найдена");
        }
        $salesStatistic = new ActionSaleStatistic($model);
        return $this->render("view", [
            'model' => $model,
            'saleStatistic' => $salesStatistic
        ]);
    }

    public function actionActivate(int $actionId)
    {
        if ($model = Action::findOne($actionId)) {
            $actionForm = new ActionForm($this->subscription->getSubscriptionsLimitationsSummary());
            $actionForm->setModel($model);
            $actionForm->moveToActive();
            \Yii::$app->session->setFlash("success", "Акция успешно активирована");
            return $this->redirect(Url::to(['index']));
        }

        throw new NotFoundHttpException();
    }

    public function actionMoveToArchive(int $actionId)
    {
        if ($model = Action::findOne($actionId)) {
            $actionForm = new ActionForm($this->subscription->getSubscriptionsLimitationsSummary());
            $actionForm->setModel($model);
            $actionForm->moveToArchive();
            \Yii::$app->session->setFlash("success", "Акция успешно архивирована");
            return $this->redirect(Url::to(['index']));
        }

        throw new NotFoundHttpException();
    }
}
