<?php

namespace app\controllers\organization;

use app\classes\SalePointStatistic;
use app\controllers\BaseAdminController;
use app\models\forms\SalePointCreateUpdateForm;
use app\models\Organization;
use app\models\SalePoint;
use app\models\SearchModel\SalePointSearch;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

class SalepointsController extends BaseAdminController
{
    public $layout = "admin";
    protected  $organization;

    public function beforeAction($action)
    {
        //TODO: Убрать мок организации
        $this->organization = Organization::findOne(1);
        return parent::beforeAction($action); // TODO: Change the autogenerated stub
    }

    public function actionIndex()
    {
        $searchModel = new SalePointSearch();
		$searchModel->setCompanyId($this->organization->id);
        $salePointCreateForm = (new SalePointCreateUpdateForm($this->subscription->getSubscriptionsLimitationsSummary()))->setOrganization($this->organization);
        $dataProvider = $searchModel->getSearchProvider();
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'salePointCreateForm' => $salePointCreateForm,
        ]);
    }

    public function actionSalePointUpdate($id)
    {
        if ($model = SalePoint::findOne(['is_archived' => 0, 'organization_id' => $this->organization->id, 'id' => $id])) {
            $salePointCreateForm = (new SalePointCreateUpdateForm($this->subscription->getSubscriptionsLimitationsSummary()))->setOrganization($this->organization)->setSalePoint($model);
            $statistic = new SalePointStatistic($model);
            if (\Yii::$app->request->isAjax && $salePointCreateForm->load(\Yii::$app->request->post())) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($salePointCreateForm);
            }

            if ($salePointCreateForm->load(\Yii::$app->request->post()) && $salePointCreateForm->validate() && $salePointCreateForm->update()) {
                \Yii::$app->session->setFlash("success", "Информация о торговой точке успешно обновлена");
            }


            return $this->render("update", ['statistic' => $statistic, 'salePointCreateForm' => $salePointCreateForm]);
        }
        throw new NotFoundHttpException("Не удалось найти торговую точку");
    }

    public function actionDelete(int $id)
    {
        if ($model = SalePoint::findOne($id)) {
            $model->markAsArchived();
            \Yii::$app->session->setFlash("success", "Торговая точка успешно удалена");
            return $this->redirect("/sale-points");
        }
        throw  new NotFoundHttpException("Торговая точка отсутствует");
    }

    public function actionCreate()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $salePointCreateForm = (new SalePointCreateUpdateForm($this->subscription->getSubscriptionsLimitationsSummary()))->setOrganization($this->organization);
        $salePointCreateForm->load(\Yii::$app->request->post());
        $errors = ActiveForm::validate($salePointCreateForm);
        if (!\Yii::$app->request->isAjax) {
            $newModel = $salePointCreateForm->create();
            return $this->redirect('/sale-points');
        }
        return $errors;
    }

}
