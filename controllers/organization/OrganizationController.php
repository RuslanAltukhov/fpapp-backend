<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 12/06/2019
 * Time: 17:19
 */

namespace app\controllers\organization;


use app\classes\paymentsystem\Robokassa;
use app\classes\paymentsystem\Tinkoff;
use app\classes\TopStatistic;
use app\controllers\BaseAdminController;
use app\models\Cashier;
use app\models\forms\AddBalanceForm;
use app\models\forms\CashierCreateUpdateForm;
use app\models\forms\OrganizationUpdateForm;
use app\models\forms\SalePointCreateUpdateForm;
use app\models\SalePoint;
use yii\web\Controller;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;

class OrganizationController extends BaseAdminController
{
    public $layout = "admin";

    public function getViewPath()
    {
        return "@app/views/organization";
    }

    public function actionIndex()
    {
        $statistic = new TopStatistic($this->organization);
        $orgUpdateForm = (new OrganizationUpdateForm())->setOrganization($this->organization);

        $orgUpdateForm->uploadedImage = UploadedFile::getInstance($orgUpdateForm, "logo");
        if ($orgUpdateForm->load(\Yii::$app->request->post()) && $orgUpdateForm->validate() && $orgUpdateForm->save()) {
            \Yii::$app->session->setFlash("success", "Данные успешно обновлены");
        }

        $balanceFormModel = new AddBalanceForm(\Yii::$app->user->identity, new Tinkoff());
        if ($balanceFormModel->load(\Yii::$app->request->post()) && ($link = $balanceFormModel->getPaymentLink())) {
            return $this->redirect($link);
        }

        return $this->render("index", [
            'balanceFormModel' => $balanceFormModel,
            'orgUpdateForm' => $orgUpdateForm,
            'owner' => $this->organization->owner,
            'success' => \Yii::$app->session->getFlash("success"),
            'topActions' => $statistic->getTopActions(),
            'topCashiers' => $statistic->getTopCashiers(),
            'topClients' => $statistic->getTopClients(),
            'topReferals' => $statistic->getTopReferals()
        ]);
    }

    public function actionStart($currentStep = null)
    {
        $step = $currentStep ?? 1;
        $orgUpdateForm = (new OrganizationUpdateForm())->setOrganization($this->organization);
        $salePointCreateForm = (new SalePointCreateUpdateForm($this->subscription->getSubscriptionsLimitationsSummary()))->setOrganization($this->organization);
        $cashierCreateForm = (new CashierCreateUpdateForm($this->subscription->getSubscriptionsLimitationsSummary()))->setOrganization($this->organization);


        $orgUpdateForm->uploadedImage = UploadedFile::getInstance($orgUpdateForm, "logo");

        if ($orgUpdateForm->load(\Yii::$app->request->post()) && $orgUpdateForm->validate() && $orgUpdateForm->save()) {
            $step = 2;
        }


        if ($cashierCreateForm->load(\Yii::$app->request->post()) && $cashierCreateForm->validate() && $cashierCreateForm->save()) {
            $step = 3;
        }

        if (\Yii::$app->request->isAjax && $cashierCreateForm->load(\Yii::$app->request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($cashierCreateForm);
        }

        if (\Yii::$app->request->isAjax && $salePointCreateForm->load(\Yii::$app->request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($salePointCreateForm);
        }

        if ($salePointCreateForm->load(\Yii::$app->request->post()) && $salePointCreateForm->validate() && $salePointCreateForm->create()) {
            $step = 2;
        }
        $salePoints = SalePoint::find()->onlyActive()->andWhere(['organization_id' => $this->organization->id])->all();
        $cashiers = Cashier::find()->where(['organization_id' => $this->organization->id])->all();
        return $this->render("start", [
            'orgUpdateModel' => $orgUpdateForm,
            'step' => $step,
            'salePointCreateModel' => $salePointCreateForm,
            'cashierCreateModel' => $cashierCreateForm,
            'salePoints' => $salePoints,
            'cashiers' => $cashiers
        ]);
    }

    public function actionHistory()
    {
        $statistic = new TopStatistic($this->organization);
        return $this->render("history", [
            'history' => $statistic->getSalesHistory()
        ]);
    }

}