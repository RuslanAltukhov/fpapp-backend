<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 13/10/2019
 * Time: 21:33
 */

namespace app\controllers\organization;


use app\classes\Exception\SubscriptionException;
use app\controllers\BaseAdminController;
use app\models\forms\BuySubscriptionForm;

class FinancesController extends BaseAdminController
{
    public $layout = "admin";

    public function getViewPath()
    {
        return "@app/views/organization";
    }

    public function actionPlan()
    {
        $subscriptionForm = new BuySubscriptionForm($this->organization);
        try {
            if ($subscriptionForm->load(\Yii::$app->request->post())) {
                $subscriptionForm->buy();
                \Yii::$app->session->setFlash("success", "Подписка успешно приобретена");
                return $this->redirect("/organization/profile");
            }
        } catch (SubscriptionException $e) {
            \Yii::$app->session->setFlash("error", $e->getMessage());
        }

        return $this->render("plan", [
            'model' => $subscriptionForm,
            'error' => \Yii::$app->session->getFlash("error")
        ]);
    }

}