<?php

namespace app\controllers\users;

use app\controllers\BaseAdminController;
use app\models\Cashier;
use app\models\forms\CashierCreateUpdateForm;
use app\models\Organization;
use app\models\SalePoint;
use app\models\SearchModel\CashierSearchModel;
use yii\web\NotFoundHttpException;

class CashiersController extends BaseAdminController
{
    public $layout = "admin";



    public function actionIndex()
    {
        $searchModel = new CashierSearchModel();
		$searchModel->setCompanyId($this->organization->id);
        $datProvider = $searchModel->getSearchProvider();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $datProvider
        ]);
    }

    public function actionCreate()
    {
        $cashierCreateForm = (new CashierCreateUpdateForm($this->subscription->getSubscriptionsLimitationsSummary()))->setOrganization($this->organization);
        $salePoints = SalePoint::find()->onlyActive()->andWhere(['organization_id' => $this->organization->id])->all();
        if ($cashierCreateForm->load(\Yii::$app->request->post()) && $cashierCreateForm->validate() && $cashierCreateForm->save()) {
            return $this->redirect("/cashiers ");
        }
        return $this->render("create", ['model' => $cashierCreateForm, 'salePoints' => $salePoints]);
    }

    public function actionUpdate($id)
    {
        if ($model = Cashier::findOne(['id' => $id, 'organization_id' => $this->organization->id])) {
            $cashierCreateForm = (new CashierCreateUpdateForm($this->subscription->getSubscriptionsLimitationsSummary()))->setOrganization($this->organization)->setCashier($model);
            $salePoints = SalePoint::find()->onlyActive()->andWhere(['organization_id' => $this->organization->id])->all();
            if ($cashierCreateForm->load(\Yii::$app->request->post()) && $cashierCreateForm->validate() && $cashierCreateForm->save()) {
                return $this->redirect("/cashiers ");
            }
            return $this->render("update", ['model' => $cashierCreateForm, 'salePoints' => $salePoints]);
        }
        throw new NotFoundHttpException("Кассир не найден");
    }
}
