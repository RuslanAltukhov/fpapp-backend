<?php

namespace app\controllers\users;

use app\classes\UserBuyStatistic;
use app\controllers\BaseAdminController;
use app\models\Organization;
use app\models\SearchModel\UsersSearchModel;
use app\models\User;
use yii\web\NotFoundHttpException;

class UsersController extends BaseAdminController
{
    public $layout = "admin";


    public function actionIndex()
    {
        $searchModel = (new UsersSearchModel())->setOrganization($this->organization);
        $dataProvider = $searchModel->getSearchProvider();
        return $this->render('index', [
            'organization' => $this->organization,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    public function actionView($id)
    {

        if ($model = User::findOne($id)) {
            return $this->render("view", ['user' => $model, 'organization' => $this->organization,
                'userStatistic' => new UserBuyStatistic($model, $this->organization)
            ]);
        }
        throw new NotFoundHttpException("Пользователь не найден");
    }

}
