<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 05/10/2019
 * Time: 17:19
 */

namespace app\controllers\backoffice;


use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class BaseController extends Controller
{
    public $layout = "backoffice";

    public function checkDomain()
    {
        if (preg_match("/(http|https)\:\/\/app\./i", Url::base(true))) {
            throw new NotFoundHttpException("Раздел не найден");
        }
    }

    public function getViewPath()
    {
        return "@app/views/backoffice";
    }

    public function isAuthorized()
    {
        return !\Yii::$app->user->isGuest;
    }

    public function beforeAction($action)
    {
        $this->checkDomain();
        if (!$this->isAuthorized()) {
            \Yii::$app->response->redirect("/site/login");
            return false;
        }
        return parent::beforeAction($action); // TODO: Change the autogenerated stub
    }
}