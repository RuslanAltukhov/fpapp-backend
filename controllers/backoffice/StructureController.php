<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 27/09/2019
 * Time: 20:57
 */

namespace app\controllers\backoffice;


use app\classes\adapters\JsTreeResponseAdapter;
use app\classes\adapters\PartnerLimitationsAdapter;
use app\classes\adapters\UserStructureResponseAdapter;
use app\classes\Exception\UserChangeStatusException;
use app\classes\PartnerStructure;
use app\classes\TransactionsStatReader;
use app\models\forms\AgentPacketActivatorForm;
use app\models\forms\CreateUserForm;
use app\models\InvitationTree;
use yii\web\Controller;

class StructureController extends BaseController
{
    public $layout = "backoffice";

    /**
     * @return string
     */
    public function getViewPath()
    {
        return "@app/views/backoffice";
    }

    public function actionTree()
    {
        $user = \Yii::$app->user->identity;
        $tree = (new InvitationTree($user))->getTree();
        $dataAdapter = new JsTreeResponseAdapter($tree);

        return $this->render("tree", ['treeJSON' => $dataAdapter->getJSON()]);
    }

    public function actionStructure()
    {
        $userCreateForm = new CreateUserForm(\Yii::$app->user->identity);
        $limitationsAdapter = new PartnerLimitationsAdapter(\Yii::$app->user->identity);
        $agentPackageActivatorForm = new AgentPacketActivatorForm($limitationsAdapter);
        if ($userCreateForm->load(\Yii::$app->request->post()) && $userCreateForm->validate() && $userCreateForm->createUser()) {
            \Yii::$app->session->setFlash("success", "Компания успешно создана");
        }
        try {
            if ($agentPackageActivatorForm->load(\Yii::$app->request->post()) && $agentPackageActivatorForm->validate() && $agentPackageActivatorForm->activate()) {
                \Yii::$app->session->setFlash("success", "Компания успешно создана");
            }
        } catch (UserChangeStatusException $e) {
            \Yii::$app->session->setFlash("error", $e->getMessage());
        }
        $transactionReader = new TransactionsStatReader(\Yii::$app->user->identity);
        $feeTransactions = $transactionReader->getIncomeFeeTransactions(\Yii::$app->user->identity);
        $structureItems = (new PartnerStructure())->getStructureUsers(\Yii::$app->user->identity);

        return $this->render("structure", [
            'createUserFormModel' => $userCreateForm,
            'limitationsAdapter' => $limitationsAdapter,
            'agentPackageActivatorForm' => $agentPackageActivatorForm,
            'provider' => (new UserStructureResponseAdapter($structureItems, $feeTransactions))->getDataAsProvider()
        ]);
    }

}