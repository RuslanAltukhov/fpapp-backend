<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 02/10/2019
 * Time: 22:06
 */

namespace app\controllers\backoffice;


use app\models\backoffice\UserProfile;
use yii\web\Controller;
use yii\web\UploadedFile;

class ProfileController extends BaseController
{
    public $layout = "backoffice";

    public function getViewPath()
    {
        return "@app/views/backoffice";
    }

    public function actionEdit()
    {
        $user = \Yii::$app->user->identity;
        $model = new UserProfile($user);
        if ($model->profilePhoto = UploadedFile::getInstance($model, "profilePhoto")) {
            $model->upload();
        }
        if ($model->load(\Yii::$app->request->post()) && $model->validate() && $model->save()) {
            \Yii::$app->session->setFlash("success", "Данные профиля успешно обновлены");
        }

        return $this->render("edit-profile", [
            'model' => $model,
            'success' => \Yii::$app->session->getFlash("success")
        ]);
    }
}