<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 29/09/2019
 * Time: 21:29
 */

namespace app\controllers\backoffice;


use app\classes\paymentsystem\Robokassa;
use app\models\forms\AddBalanceForm;
use app\models\PanelModel;
use yii\web\Controller;

class PanelController extends BaseController
{
    public $layout = "backoffice";

    public function getViewPath()
    {
        return "@app/views/backoffice";
    }

    public function actionIndex()
    {


        $model = new PanelModel(\Yii::$app->user->identity);
        return $this->render("panel", [
            'model' => $model
        ]);
    }

}