<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 06/10/2019
 * Time: 11:37
 */

namespace app\controllers\backoffice;


use app\classes\Exception\UserChangeStatusException;
use app\classes\paymentsystem\Robokassa;
use app\classes\paymentsystem\Tinkoff;
use app\classes\TransactionsStatReader;
use app\classes\UserTypeChanger;
use app\models\backoffice\FinancialDashboardModel;
use app\models\forms\AddBalanceForm;
use app\models\forms\MoneyOutForm;

class FinancesController extends BaseController
{

    public function actionIndex()
    {
        $transactionsStat = new TransactionsStatReader(\Yii::$app->user->identity);
        $model = new FinancialDashboardModel(\Yii::$app->user->identity);
        $moneyOutForm = new MoneyOutForm(\Yii::$app->user->identity);
        $moneyOutForm->load(\Yii::$app->request->post()) && $moneyOutForm->validate();
        $balanceFormModel = new AddBalanceForm(\Yii::$app->user->identity, new Tinkoff());
        if ($balanceFormModel->load(\Yii::$app->request->post()) && ($link = $balanceFormModel->getPaymentLink())) {
            return $this->redirect($link);
        }
        return $this->render("finances", ['model' => $model,
            'balanceFormModel' => $balanceFormModel,
            'transactionsList' => $transactionsStat->getTransactionsListForUserAsProvider(),
            'moneyOutForm' => $moneyOutForm
        ]);
    }

    public function actionPlan()
    {
        $user = \Yii::$app->user->identity;
        return $this->render("plan", [
            'user' => $user,
            'success' => \Yii::$app->session->getFlash("success"),
            'error' => \Yii::$app->session->getFlash("error"),
        ]);
    }

    public function actionBuyAgent()
    {
        try {
            $userTypeChange = new UserTypeChanger(\Yii::$app->user->identity);
            $userTypeChange->changeStatusToAgent();
            \Yii::$app->session->setFlash("success", "Пакет успешно приобретен");
        } catch (UserChangeStatusException $e) {
            \Yii::$app->session->setFlash("error", $e->getMessage());
        } catch (\Exception $e) {
            \Yii::$app->session->setFlash("error", $e->getMessage());
        } finally {
            return $this->redirect("/backoffice/finances/plan");
        }
    }

    public function actionBuyPartner()
    {
        try {
            $userTypeChange = new UserTypeChanger(\Yii::$app->user->identity);
            $userTypeChange->changeStatusToPartner();
            \Yii::$app->session->setFlash("success", "Пакет успешно приобретен");
        } catch (UserChangeStatusException $e) {
            \Yii::$app->session->setFlash("error", $e->getMessage());
        } finally {
            return $this->redirect("/backoffice/finances/plan");
        }
    }
}