<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 17/10/2019
 * Time: 23:04
 */

namespace app\controllers;

use app\models\tinkoff\PaymentAcceptor;
use yii\web\Controller;
use yii\web\Response;

class PaymentsController extends Controller
{
    public $enableCsrfValidation = false;

    public function actionSuccess()
    {
        try {
            \Yii::$app->response->format = Response::FORMAT_RAW;
            $paymentAcceptor = new PaymentAcceptor(\Yii::$app->params['tinkoff']['terminal']);
            $paymentAcceptor->load(\Yii::$app->request->post(), '') && $paymentAcceptor->validate() && $paymentAcceptor->accept();
            return "OK";
        } catch (\Exception $e) {
            return "OK";
        }
        // \Yii::$app->response->format = Response::FORMAT_JSON;
        // return $paymentAcceptor->getErrorSummary(true);
    }

    public function actionResult()
    {
        \Yii::$app->response->format = Response::FORMAT_RAW;
        return "Платеж обработан";
    }
}