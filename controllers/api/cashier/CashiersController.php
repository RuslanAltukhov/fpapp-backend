<?php

namespace app\controllers\api\cashier;

use app\classes\adapters\SalepointUserActionsAdapter;
use app\classes\CashierSalesHistory;
use app\classes\PhoneNumberFormatter;
use app\classes\SalepointActions;
use app\classes\SmscSender;
use app\classes\UserActions;
use app\models\forms\api\CashierAddPlusForm;
use app\models\forms\api\CashierChooseSalePointForm;
use app\models\forms\api\CashierLoginForm;
use app\models\forms\api\CashierSalepointsForm;
use app\models\forms\api\CashierSupportMessageForm;
use app\models\forms\api\RegistrationForm;
use app\models\User;
use Symfony\Component\CssSelector\Exception\InternalErrorException;
use Yii;
use yii\filters\auth\QueryParamAuth;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\ServerErrorHttpException;

/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 27/05/2019
 * Time: 18:08
 */
class CashiersController extends BaseCashierController
{


    public function actionLogin()
    {
        $model = new CashierLoginForm();
        $model->load(Yii::$app->request->post(), '');
        if ($model->validate() && $model->login()) {
            return ['cashier' => $model->getCashier(), 'token' => $model->getAuthToken()];
        }
        Yii::$app->response->setStatusCode(401);
        return $model->getErrorsFormatted();
    }

    public function actionSalesHistory()
    {
        $historyFetcher = new CashierSalesHistory($this->cashier, $this->salePoint);
        $historyFetcher->load(Yii::$app->request->get(), '');
        if ($historyFetcher->validate()) {
            return $historyFetcher->getSalesFor5Days();
        }
        throw new BadRequestHttpException($historyFetcher->getErrorsFormatted());
    }

    public function actionCheckToken()
    {
        if ($this->authToken) {
            return ['code' => 0, 'text' => 'Token is active'];
        } else {
            return ['code' => 1, 'text' => 'Token is unavaliable'];
        }
    }

    public function actionSetSalepoint()
    {

        $model = new CashierChooseSalePointForm();
        $model->setCashier($this->cashier);
        $model->setAuthToken($this->authToken);
        $model->load(Yii::$app->request->post(), '');
        if ($model->validate() && $model->saveSelection()) {
            return ['text' => 'Торговая точка успешно изменена',
                'salePoint' => $model->salepointId];
        }
        throw new ServerErrorHttpException($model->getErrorsFormatted());
    }

    public function actionSalepoints()
    {
        $model = new CashierSalepointsForm();
        $model->setCashier($this->cashier);
        return $model->getSalePoints();
    }

    public function actionUserCards($userId)
    {
        $model = User::findByPhoneOrId($userId);
        $phoneFormatter = new PhoneNumberFormatter();


        if (!$model && $phoneFormatter->isPhoneNumber($userId)) {
            $registrationModel = new RegistrationForm();
            $registrationModel->phone = $userId;
            $model = $registrationModel->validate() ? $registrationModel->createUser() : false;
            if ($model) {
                $smsSender = SmscSender::getInstance();
                $smsSender->sendMessage("Ссылка на скачивание приложения:" . (Yii::$app->params['app_download_link']), $model->phone);
            }
        }

        if (!$model) {
            throw new NotFoundHttpException("Пользователь не найден");
        }

        $salepointActions = new SalepointUserActionsAdapter(new SalepointActions($this->salePoint), new UserActions($model));
        return ['user' => $model->toArray(), 'items' => $salepointActions->getData()];
    }

    public function actionCheckItems()
    {
        $userIdOrPhone = Yii::$app->request->post('userId', null);
        $user = User::findByPhoneOrId($userIdOrPhone);
        $phoneFormatter = new PhoneNumberFormatter();

        if (!$user && $phoneFormatter->isPhoneNumber($userIdOrPhone)) {
            $registrationModel = new RegistrationForm();
            $registrationModel->phone = $userIdOrPhone;
            $user = $registrationModel->validate() ? $registrationModel->createUser() : false;
        }

        if (!$user) {
            throw new NotFoundHttpException("Пользователь не найден");
        }

        $model = new CashierAddPlusForm($user, $this->cashier, $this->salePoint, new UserActions($user));
        $model->items = Yii::$app->request->post('items');
        if ($model->validate() && $model->save()) {
            return ['text' => 'Данные сохранены'];
        }
        return $model->getErrors();
    }

    public function actionSendMessage()
    {
        $model = new CashierSupportMessageForm($this->cashier);
        if ($model->load(Yii::$app->request->post(), '') && $model->validate() && $model->send()) {
            return ['text' => 'Сообщение отправлено'];
        }

        return ['text' => 'Ошибка отправки'];
    }
}