<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 24/07/2019
 * Time: 23:33
 */

namespace app\controllers\api\cashier;


use app\classes\CashierSessionContext;
use app\models\AuthToken;
use app\models\Cashier;
use yii\filters\auth\QueryParamAuth;
use yii\rest\Controller;
use yii\web\Response;
use yii\web\UnauthorizedHttpException;

class BaseCashierController extends Controller
{
    protected $cashier;
    protected $user;
    protected $authToken;
    protected $salePoint;
    const ACTION_LOGIN = "login";
    const ACTION_CHECK_TOKEN = 'check-token';
    const ACTION_CREATE_USER = 'create-user';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats'] = [
            'application/json' => Response::FORMAT_JSON
        ];
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::class,
            'cors' => [
                // restrict access to
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['POST', 'GET', 'OPTIONS', 'PUT', 'PATCH'],
                'Access-Control-Allow-Credentials' => false,
                'Access-Control-Request-Headers' => ['Accept', 'Origin', 'Content-Type', ''],
                'Access-Control-Expose-Headers' => ['Accept', 'Origin', 'Content-Type', '']
            ],

        ];
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::class,
            'tokenParam' => 'sessid',
            'except' => [self::ACTION_CREATE_USER, self::ACTION_CHECK_TOKEN, self::ACTION_LOGIN]
        ];
        return $behaviors;
    }

    public function beforeAction($action)
    {
        $parent = parent::beforeAction($action);
        $this->authToken = AuthToken::getByHash(\Yii::$app->request->get("sessid", ''));

        if (in_array($action->id, [self::ACTION_LOGIN, self::ACTION_CHECK_TOKEN])) {
            return $parent;
        }


        $this->user = \Yii::$app->user->identity;

        if (!$this->user) {
            throw new UnauthorizedHttpException("Необходимо авторизоваться");
        }


        $this->cashier = Cashier::findOne(['user_id' => $this->user->id]);

        if (!$this->cashier) {
            throw new UnauthorizedHttpException("Кассир не найден");
        }
        $sessionContext = new CashierSessionContext($this->cashier, $this->authToken);
        $this->salePoint = $sessionContext->geCurrentSalepoint();
        return $parent;
    }
}