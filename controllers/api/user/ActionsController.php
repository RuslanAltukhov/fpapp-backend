<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 27/05/2019
 * Time: 17:02
 */

namespace app\controllers\api\user;


use app\classes\adapters\AllActionsResponseAdapter;
use app\classes\adapters\CategoriesAdapter;
use app\classes\adapters\UserActionsResponseAdapter;
use app\classes\UserActions;
use app\models\SearchModel\ActionSearch;
use app\models\SearchModel\CategorySearch;
use app\models\SearchModel\SalePointSearch;
use yii\filters\auth\QueryParamAuth;
use yii\rest\Controller;
use yii\web\Response;

class ActionsController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats'] = [
            'application/json' => Response::FORMAT_JSON
        ];
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::class,
            'cors' => [
                // restrict access to
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['POST', 'GET', 'OPTIONS', 'PUT', 'PATCH'],
                'Access-Control-Allow-Credentials' => false,
                'Access-Control-Request-Headers' => ['Accept', 'Origin', 'Content-Type', ''],
                'Access-Control-Expose-Headers' => ['Accept', 'Origin', 'Content-Type', '']
            ],

        ];
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::class,
            'tokenParam' => 'sessid',
            'except' => ['get-actions', 'categories']
        ];
        return $behaviors;
    }

    public function actionMyActions()
    {
        $userActions = new UserActions(\Yii::$app->user->identity);
        $actionsAdapter = new UserActionsResponseAdapter($userActions);
        return $actionsAdapter->getActionsWithPurchasedItems();
    }

    public function actionSalePoints()
    {
        $searchModel = new SalePointSearch();
        $searchModel->load(\Yii::$app->request->get(), '');
        return $searchModel->getSearchProvider()->getModels();
    }

    public function actionCategories()
    {
        $searchModel = new CategorySearch();
        $searchModel->load(\Yii::$app->request->get(), '');
        $adapter = new CategoriesAdapter($searchModel->search());
        return $adapter->get();
    }

    public function actionGetActions()
    {
        $searchModel = new ActionSearch();
        // $searchModel->currentTime = mktime();
        $searchModel->load(\Yii::$app->request->get(), '');
        $responseFormatter = new AllActionsResponseAdapter($searchModel->getSearchQuery()->onlyActive()->onlyWithSalePoints(), mktime());
        return $responseFormatter->getFormatted();
    }

}