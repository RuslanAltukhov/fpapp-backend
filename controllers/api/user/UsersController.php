<?php

namespace app\controllers\api\user;

use app\classes\BarcodeGenerator;
use app\models\AuthToken;
use app\models\data\UserProfile;
use app\models\forms\api\LoginForm;
use app\models\forms\api\PushTokenSaveForm;
use app\models\forms\api\RegistrationForm;
use app\models\forms\api\UpdateUserForm;
use app\models\forms\api\UserRestorePasswordForm;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\QueryParamAuth;
use yii\filters\PageCache;
use yii\web\Response;

class UsersController extends \yii\rest\Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats'] = [
            'application/json' => Response::FORMAT_JSON
        ];
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::class,
            'cors' => [
                // restrict access to
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['POST', 'GET', 'OPTIONS', 'PUT', 'PATCH'],
                'Access-Control-Allow-Credentials' => false,
                'Access-Control-Request-Headers' => ['Accept', 'Origin', 'Content-Type', ''],
                'Access-Control-Expose-Headers' => ['Accept', 'Origin', 'Content-Type', '']
            ],

        ];
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::class,
            'tokenParam' => 'sessid',
            'except' => ['create-user', 'login', 'check-token', 'restore']
        ];
        return $behaviors;
    }

    public function actionCreateUser()
    {
        $model = new RegistrationForm();
        if ($model->load(\Yii::$app->request->post(), '') && $model->validate() && ($newUser = $model->createUser())) {
            return ['success' => 'Вы успешно зарегистрировались'];
        }
        \Yii::$app->response->setStatusCode(400);

        return ['error' => implode(",", $model->getErrorSummary(true))];

    }

    public function actionPushToken()
    {
        $form = new PushTokenSaveForm(\Yii::$app->user->identity);
        if ($form->load(\Yii::$app->request->post(), '') && $form->validate() && $form->save()) {
            return ['text' => 'Токе сохранен'];
        }
        return $form->getErrors();
    }

    public function actionCheckToken()
    {
        if ($token = AuthToken::getByToken(\Yii::$app->request->post('sessid'))) {
            return ['code' => 0, 'text' => 'Token is active'];
        } else {
            return ['code' => 1, 'text' => 'Token is unavaliable'];
        }
    }

    public function actionLogin()
    {
        $model = new LoginForm();
        if ($model->load(\Yii::$app->request->post() + \Yii::$app->request->get(), '') && $model->login() && ($token = $model->getAuthToken())) {
            return ['code' => 0, 'data' => ['user' => $model->getUser(), 'token' => $token]];
        }
        return ['code' => 1, 'text' => 'Не верный логин и пароль', 'data' => $model->getErrors()];

    }

    public function actionRestore()
    {
        $model = new UserRestorePasswordForm();
        if ($model->load(\Yii::$app->request->post(), '') && $model->validate() && $model->restorePassword()) {
            return ['text' => 'Новый пароль выслан вам по смс'];
        }
        \Yii::$app->response->setStatusCode(401);
        return ['text' => $model->getErrors()];
    }

    public function actionProfile()
    {
        $user = \Yii::$app->user->identity;
        $updateModel = (new UpdateUserForm())->setUserModel($user);
        if (\Yii::$app->request->isGet) {
            return new UserProfile($user);
        }

        if ($updateModel->load(\Yii::$app->request->post(), '') && $updateModel->validate() && $updateModel->save()) {
            return new UserProfile(\Yii::$app->user->identity);
        }
        \Yii::$app->response->setStatusCode(403);
        return implode(",", $updateModel->getErrorSummary(true));

    }

}
