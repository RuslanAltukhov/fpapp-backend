<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'language' => 'ru-RU',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'defaultRoute' => '/site/login',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '1DMtCZv7M0GbsKJoMvYFOxqePc0slQhW',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/api/users/create' => 'api/user/users/create-user',
                '/api/users/login' => 'api/user/users/login',
                '/api/actions/my-actions' => 'api/user/actions/my-actions',
                '/api/users/check-token' => 'api/user/users/check-token',
                '/api/user/profile' => 'api/user/users/profile',
                'actions' => 'actions/actions/index',
                'users' => 'users/users/index',
                'users/view/<id:\d+>' => 'users/users/view',
                'cashiers' => 'users/cashiers/index',
                'cashiers/update/<id:\d+>' => 'users/cashiers/update',
                'cashiers/create' => 'users/cashiers/create',
                'actions/view/<id:\d+>' => 'actions/actions/view',
                'actions/<actionId:\d+>/sales' => 'actions/sales',
                'sale-points' => 'organization/salepoints',
                'organization/profile' => 'organization/organization/index',
                'organization/history' => 'organization/organization/history',
                'actions/<actionId:\d+>/update' => 'actions/actions/update',
                'sale-points/create' => 'organization/salepoints/create',
                'sale-points/update/<id:\d+>' => 'organization/salepoints/sale-point-update',
                'sale-points/delete/<id:\d+>' => 'organization/salepoints/delete',
                'actions/create' => 'actions/actions/create',
                'actions/update' => 'actions/actions/update',
                'actions/<actionId:\d+>/move-to-archive' => 'actions/actions/move-to-archive',
                'actions/<actionId:\d+>/update' => 'actions/actions/update',
            ],
        ],

    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
