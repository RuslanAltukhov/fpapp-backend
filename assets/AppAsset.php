<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        "/css/style.css",
        "/css/jquery.mCustomScrollbar.min.css",
        "/css/dataTables.bootstrap.css",
        "/css/select2.min.css",
        "/css/affiliate.css",
        "/css/tree.css",
        "/css/constructor.css",
    ];
    public $js = [
        "/js/scripts/modernizr.min.js",
        'https://cdn.jsdelivr.net/npm/clipboard@2/dist/clipboard.min.js',
        "/js/plugin/bootstrap/js/bootstrap.min.js",
        "/js/plugin/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js",
        "/js/plugin/nprogress/nprogress.js",
        "/js/plugin/datatables/media/js/jquery.dataTables.js",
        "/js/plugin/datatables/media/js/dataTables.bootstrap.min.js",
        "/js/scripts/datatables.demo.js",

        "/js/plugin/select2/js/select2.min.js",
        "/js/plugin/multiselect/multiselect.min.js",
        "/js/scripts/form.demo.min.js",
        '/js/plugin/jstree.min.js',
        "/js/scripts/main.min.js"

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset'
    ];
}
