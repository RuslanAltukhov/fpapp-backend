<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 22/09/2019
 * Time: 15:47
 */

namespace app\assets;


use yii\web\AssetBundle;

class ClientRegistrartionAssets extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',

    ];
}